<html class="js jsDomReady">
<head>
    <link rel="shortcut icon" href="{{url('/design/img')}}/F_logo.png" type="image/x-icon">
    <title>xACE</title>
    <meta charset="utf-8">

    <!-- Styles -->
    <link rel="stylesheet" crossorigin="anonymous" href="{{url('/')}}/css/docs.css">
    <link rel="stylesheet" crossorigin="anonymous" href="{{url('/')}}/css/responsive.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- /Styles -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    {{--button list--}}
    <script>
        var coll = document.getElementsByClassName("collapsible");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.maxHeight){
                    content.style.maxHeight = null;
                    content.style.maxWidth = null;
                } else {
                    content.style.maxHeight = content.scrollHeight + "0px";
                    content.style.maxWidth = content.scrollWidth + "0px";
                }
            });
        }
    </script>
    <style>
        .collapsible {
            cursor: pointer;
        }

        .content {
            padding: 0 18px;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.2s ease-out;
            width: 100%;
        }

        .align-items-center {
            -webkit-box-align: center !important;
            align-items: center !important;
            margin-top: 8px;
        }
        .media-body {
            -webkit-box-flex: 1;
            flex: 1;
            margin-left: 10px;
        }

        .shadow{
            width: 17rem;
            padding-left: 1.5rem !important;
        }

        .navbar-nav .dropdown-menu {
            position: static;
            float: none;
        }
        .dropdown-menu.show {
            display: block;
        }
        .d-flex {
            display: -webkit-box !important;
            display: flex !important;
        }
        .shadow-sm {
            box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, 0.075) !important;
        }
        ::-webkit-scrollbar {width: 8px;}
        ::-webkit-scrollbar-track {background: #fff;}
        ::-webkit-scrollbar-thumb {background: #888;}
        ::-webkit-scrollbar-thumb:hover {background: #555;}
        .rounded-circle {
            border-radius: 50% !important;
        }
        b, strong {
            font-weight: bolder;
        }
        .dropdown {
            position: relative;
            display: inline-block;
        }

        .text-dark {
            color: #343a40 !important;
        }

        .nav-link {
            display: block;
            padding: 0.5rem 1rem;
        }
        .dropdown-toggle::after {
            display: inline-block;
            margin-left: 0.255em;
            vertical-align: 0.255em;
            content: "";
            border-top: 0.3em solid;
            border-right: 0.3em solid transparent;
            border-bottom: 0;
            border-left: 0.3em solid transparent;
        }
    </style>
    {{--/button list--}}
</head>

<body id="application" class="app API_DOCUMENTATION extraLargeWindow docsPageView embedded" data-peoplemanagerready="1" >

<div id="applicationHeader" class="applicationHeader">


        <div style="box-shadow: 0 0px 0px rgba(0,0,0,.25),0 2px 10px rgba(-89,0,0,.22)!important; width: 250px" >
                <div class="media d-flex align-items-center" style="width: 17rem; padding-left: 30px">

{{--                    <img src="https://flexiroam-public.s3-ap-southeast-1.amazonaws.com/flexiroam2019/image/app-icon.png " width="55" alt="logo">--}}
{{--                    <img src="https://flexiroam-public.s3-ap-southeast-1.amazonaws.com/flexiroam2019/image/logo.png" width="125" style=" padding-left: 16px;">--}}

                    <img src="{{url('/design/img')}}/xACE-Logo.jpg" width="55" alt="logo">
                    <img src="{{url('/design/img')}}/xace.png" width="125" style=" padding-left: 16px;">

                </div>
        </div>


        <div id="headerResponsiveContainer" class="headerResponsiveContainer" >

            <div class="headerLinks">
                <ul class="headerLinksList">

                    <!-- Link -->
                    <li class="headerLinkContainer">
                        <a href="/home" class="headerLink  inspector">
                            <span class="headerLinkText" style="border-bottom: none; font-weight: 400 !important; margin: -10px;margin-right: 27px;"> Dashboard </span>
                        </a>
                    </li>

                    <li class="headerLinkContainer">
                        <a href="/docs" class="headerLink  inspector">
                            <span class="headerLinkText" style="margin: -10px;">API </span>
                        </a>
                    </li>
                    <!-- /Link -->

                </ul>
            </div>

            <div class="headerUser js-ready">
               <div class="headerUserProfile noSelect">

                <div class=" d-flex align-items-center" style="padding-top: 8px;">
                    <li class="nav-item dropdown text-dark">
                        <a class=" nav-link dropdown-toggle" style="font-size: 24px"></a>
                    </li>

                    @if(!empty(auth()->user()->image_url))
                        <img src="{{url('/')}}/{{auth()->user()->image_url}}" alt="..." width="40" class=" rounded-circle shadow-sm">
                    @else
                        <img src="https://www.gravatar.com/avatar/4deca3014a7c60e87e0b5e8829388e95.png?default=https%3A%2F%2Fstatic.apiary.io%2Fassets%2F5Ew8Hq0N.png" alt="..." width="40" class=" rounded-circle shadow-sm">
                    @endif
                    <div class="media-body ">
                        <b>{{auth()->user()->username}} </b>
                    </div>


                </div>
            </div>
            </div>
        </div>

</div>
<!-- Content -->



@yield('content')


<!-- /Content -->


{{--button close--}}
<script>
    $(document).ready(function() {
        $(".backward").click(function() {
            $(".div1").hide();
        });
    });
</script>
{{--/button close--}}



</body>
</html>


