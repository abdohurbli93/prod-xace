<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://flexiroam-public.s3-ap-southeast-1.amazonaws.com/flexiroam2019/image/app-icon.png" type="image/x-icon">
    <title>FlexIot</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- font-awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Almarai&display=swap" rel="stylesheet">
    <!-- style  css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        ::-webkit-scrollbar {width: 8px;}
        ::-webkit-scrollbar-track {background: #f1f1f1;}
        ::-webkit-scrollbar-thumb {background: #888;}
        ::-webkit-scrollbar-thumb:hover {background: #555;}
        .container1 {display: block;position: relative;padding-right: 35px;margin-bottom: 12px;cursor: pointer;font-size: 18px;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}
        .container1 input {position: absolute;opacity: 0;cursor: pointer;height: 0;width: 0;}
        .checkmark {position: absolute;top: 0; /*right: 0;*/height: 25px;width: 25px;background-color: #bbb;border-radius: 5px;}
        .container1:hover input ~ .checkmark {background-color: #ccc;}
        .container1 input:checked ~ .checkmark {background-color: #2e3192;}
        .checkmark:after {content: "";position: absolute;display: none;}
        .container1 input:checked ~ .checkmark:after {display: block;}
        .container1 .checkmark:after {right: 9px;top: 4px;width: 8px;height: 15px;border: solid white;border-width: 0 3px 3px 0;-webkit-transform: rotate(45deg);-ms-transform: rotate(45deg);transform: rotate(45deg);}
        .sidenav {height: 100%;width: 0;position: fixed;z-index: 1;top: 0;right: 0;background-color: #111;overflow-x: hidden;transition: 0.5s;padding-top: 60px;}
        .sidenav a {padding: 8px 8px 8px 32px;text-decoration: none;font-size: 25px;color: #818181;display: block;transition: 0.3s;}
        .sidenav a:hover {color: #f1f1f1;}
        .sidenav .closebtn {position: absolute;top: 0;left: 25px;font-size: 36px;margin-right: 50px;}
        #main {transition: margin-Right .5s;padding: 16px;}
        @media screen and (max-height: 450px) {
            .sidenav {padding-top: 15px;}
            .sidenav a {font-size: 18px;}
        }
        .dropdown {position: relative;display: inline-block;}
        .dropdown-content {display: none;position: absolute;background-color: #f1f1f1;min-width: 160px;overflow: auto;box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);z-index: 1;}
        .dropdown-content a {color: black;padding: 12px 16px;text-decoration: none;display: block;}
        .dropdown a:hover {backgrouncontainer1d-color: #eee;}
        .show {display: block;}
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

</head>
<body>
    <div id="app">
        @yield('header')
        <main class="">
            @yield('content')
        </main>
    </div>



    <!-- sidebar js -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script>
        $(function() {
            // Sidebar toggle behavior
            $('#sidebarCollapse').on('click', function() {
                $('#sidebar, #content').toggleClass('active');
            });
        });
    </script>

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- Chart-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js"></script>

    <script src="{{asset('js/chart js/charts.js')}}"></script>



</body>
</html>
