@if(session('success'))
<div class="alert alert-success alert-dismissible fade show text-right" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
    <strong>نجاح!</strong> {{ session('success') }}.
</div>
@endif
