<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: #2b2f3e;">
    <!-- Brand Logo -->
    <a href="/" class="brand-link navbar-white">
        <div class="media d-flex align-items-center">
            <img class="ml-2 mr-1" src="{{url('/design/img')}}/xACE-Logo.jpg" width="45" alt="logo">
            <img class="ml-2" src="{{url('/design/img')}}/xace.png" width="120">
        </div>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        @can('isClient',auth()->user())
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image ml-2">
                    <a href="/home">
                        <i class="mt-2 nav-icon fas fa-tachometer-alt"></i>
                    </a>
                </div>
                <div class="info">
                    <a href="/home" class="d-block">Dashboard</a>
                </div>
            </div>
        @endcan

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                 @can('isClient',auth()->user())
                    <li class="nav-header">Main</li>
                    <li class="nav-item">
                    <a href="/home" class="nav-link">
                        <i class="nav-icon fa fa-home"></i>
                        <p>
                            Home
                        </p>
                    </a>
                </li>
                    <li class="nav-item">
                        <a href="/updateUser" class="nav-link">
                            <i class="nav-icon fas fa-user-edit"></i>
                            <p>
                                Update Info
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('getDeviceList')}}" class="nav-link">
                            <i class="nav-icon fa fa-address-card"></i>
                            <p>
                                Device List
                            </p>
                        </a>
                    </li>

                 @can('hasClientBalance',auth()->user())
                    {{--  <li class="nav-item">
                        <a href="{{route('getDetailSim')}}" class="nav-link">
                            <i class="nav-icon fas fa-sim-card"></i>
                            <p>
                                 SIM Detail
                            </p>
                        </a>
                    </li>  --}}
                @endcan
               @endcan

                @can('isAdmin',auth()->user())
                    <li class="nav-header" style="padding: 0.7rem 1rem .5rem;">Vendor</li>
                    <li class="nav-item">
                        <a href="{{route('addVendor')}}" class="nav-link">
                            <i class="nav-icon fa fa-address-book"></i>
                            <p>
                                Add Vendor
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('showVendor')}}" class="nav-link">
                            <i class="nav-icon fa fa-building"></i>
                            <p>
                                Show Vendor
                            </p>
                        </a>
                    </li>

                    <li class="nav-header">Hardware</li>
                    <li class="nav-item">
                        <a href="{{route('addHardware')}}" class="nav-link">
                            <i class="nav-icon far fa-plus-square"></i>
                            <p>
                                Add Hardware
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                    <a href="{{route('showHardware')}}" class="nav-link">
                        <i class="nav-icon fa fa-wrench"></i>
                        <p>
                            Show Hardware
                        </p>
                    </a>
                </li>

                    <li class="nav-header">Client</li>
                    <li class="nav-item">
                        <a href="{{route('addClient')}}" class="nav-link">
                            <i class="nav-icon fa fa-user-plus"></i>
                            <p>Add Client</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('showClient')}}" class="nav-link">
                            <i class="nav-icon fa fa-users"></i>
                            <p>Show Client</p>
                        </a>
                    </li>
                        <li class="nav-header">Range</li>
                        <li class="nav-item">
                        <a href="{{route('showRangDevice')}}" class="nav-link">
                            <i class="nav-icon ion ion-stats-bars"></i>
                            <p>Show Hardware Range </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('addRangeDevice')}}" class="nav-link">
                            <i class="nav-icon fas fa-wave-square"></i>
                            <p>Assign Hardware Range</p>
                        </a>
                    </li>
                @endcan
            </ul>

        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>


