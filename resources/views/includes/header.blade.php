<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>xACE</title>
    <link rel="shortcut icon" href="{{url('/design/img')}}/F_logo.png" type="image/x-icon">

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('/design')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{url('/design')}}/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{url('/design')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{url('/design')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{url('/design')}}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{url('/design')}}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{url('/design')}}/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
    <!-- Ion Slider -->
    <link rel="stylesheet" href="{{url('/design')}}/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('/design')}}/dist/css/adminlte.min.css">
    <!-- Google Font: Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        ::-webkit-scrollbar {width: 8px;}
        ::-webkit-scrollbar-track {background: #f1f1f1;}
        ::-webkit-scrollbar-thumb {background: #888;}
        ::-webkit-scrollbar-thumb:hover {background: #555;}
        .nav-sidebar .nav-item>.nav-link {margin-bottom: 0.5rem;}
         .border-top{border-top: 1.5px solid #e8ebee;}
        .border-left{border-left: 1.5px solid #e8ebee;}
        .border-bottom{border-bottom: 1.5px solid #e8ebee;}
        .border-right{border-right: 1.5px solid #e8ebee;}
        .w-td { width: 50px}
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
