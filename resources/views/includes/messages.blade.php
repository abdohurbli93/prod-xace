@if(count($errors)>0)
    @foreach($errors->all() as $error)

        <div class="alert alert-default-primary alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <b><i class="icon fa fa-ban"></i> Error!</b>
            {{$error}}
        </div>
    @endforeach
@endif


@if(session('success'))

    <div class="alert alert-default-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b><i class="icon fa fa-check"></i> Success!</b>
        {{session('success')}}.
    </div>
@endif


@if(session('error'))

    <div class="alert alert-default-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <b><i class="icon fa fa-warning"></i> Error!</b>
        {{session('error')}}.
    </div>

@endif
