<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light" style="height: 72px">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars text-primary"></i></a>
        </li>
        @can('isClient',auth()->user())
            <li class="nav-item d-none d-sm-inline-block">
            <a href="/home" class="nav-link font-weight-bold" style="border-bottom: #007bff solid">Dashboard</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/docs" class="nav-link">API</a>
        </li>
        @endcan
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto mr-5">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <div class="media">
                    <i class="fas fa-angle-down " style="padding: 10px"></i>
                    @if(!empty(auth()->user()->image_url))
                       <img src="{{url('/')}}/{{auth()->user()->image_url}}" alt="..." width="40" class=" rounded-circle shadow-sm">
                   @else
                        <img src="https://www.gravatar.com/avatar/4deca3014a7c60e87e0b5e8829388e95.png?default=https%3A%2F%2Fstatic.apiary.io%2Fassets%2F5Ew8Hq0N.png" alt="..." width="40" class=" rounded-circle shadow-sm">
                    @endif
                    <div class="media-body p-2">
                        <h3 class="dropdown-item-title">
                            <b>{{auth()->user()->username}} </b>
                        </h3>
                    </div>
                </div>
            </a>

            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right ">
                <span class="dropdown-item dropdown-header">Menu</span>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out text-danger fa-fw mr-2"></i>Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>


        </li>
    </ul>
</nav>
<!-- /.navbar -->
