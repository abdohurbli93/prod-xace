<!-- <footer class="main-footer">
    <p>Copyright &copy; 2019 <strong><a href="https://404developers.com">404 Developers</a></strong>. All rights reserved.</p>
</footer> -->

<footer class="main-footer">
    <p>Copyright &copy; 2020; <strong><a href="https://www.flexiroam.com/" target="_blank">Powered by Flexiroam</a></strong>. All rights reserved.</p>
</footer>


<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{url('/design')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{url('/design')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="{{url('/design')}}/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{url('/design')}}/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="{{url('/design')}}/plugins/moment/moment.min.js"></script>
<script src="{{url('/design')}}/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="{{url('/design')}}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{url('/design')}}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="{{url('/design')}}/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="{{url('/design')}}/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('/design')}}/dist/js/demo.js"></script>
<!-- Page script -->
<!-- SweetAlert2 -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 1,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })
        $('#reservationSimTime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 1,
            locale: {
                format: 'MM/DD/YYYY hh:mm A'
            }
        })

    })
</script>

@yield('contentRangeSlider')



<!-- Toastr -->
<script src="{{url('/design')}}/plugins/toastr/toastr.min.js"></script>


</body>
</html>
