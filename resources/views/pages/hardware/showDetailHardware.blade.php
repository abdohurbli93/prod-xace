@extends('index')
@section('content')
    <style>
        .input-group-text {background-color: #fff;border: 1px solid #ced4da;}
        .form-control {background-color: #fff;border: 1px solid #ced4da;}
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Show Detail Hardware </h1>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>

        <section class="content">
            <div class="col-md-12 row">

                <div class="col-md-6">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline ">
                        <div class="card-body box-profile">

                            <h2 class="profile-username font-weight-bold text-center">{{$showDetailHardware->device_name}}</h2>

                            <p class="text-muted text-center">{{$showDetailHardware->hardware_type}} </p>

                            <hr>

                            @if($showDetailHardware->hardware_type == 'sim')
                                <strong><i class="fas fa-fa-sim-card mr-1"></i>IMSI :</strong>

                                <b class="text-muted ml-2">{{$showDetailHardware['imsi']}}</b>

                                <hr>

                                <strong><i class="fas fa-sim-card mr-1"></i> MSISDN:</strong>

                                <b class="text-muted ml-2">  {{$showDetailHardware['msisdn']}}</b>

                                <hr>

                                <strong><i class="fas fa-sim-card mr-1"></i> ICCID:</strong>

                                <b class="text-muted ml-2">  {{$showDetailHardware['iccid']}}</b>

                                <hr>

                                <strong><i class="fas fa-sim-card mr-1"></i> PIN:</strong>

                                <b class="text-muted ml-2">  {{$showDetailHardware['pin']}}</b>

                                <hr>

                                <strong><i class="fas fa-sim-card mr-1"></i> PUK:</strong>

                                <b class="text-muted ml-2">  {{$showDetailHardware['puk']}}</b>

                                <hr>
                            @elseif($showDetailHardware->hardware_type == 'sensor')

                                <strong><i class="fas fa-sim-card mr-1"></i> Hostname:</strong>

                                <p class="text-muted">
                                    <span class="tag tag-primary">{{$showDetailHardware->hostname}}</span>
                                </p>

                                <hr>
                                <strong><i class="fas fa-lock-open mr-1"></i>Temperature Address:</strong>
                                <b class="text-muted ml-2">  {{$showDetailHardware['temperature_address']}}</b>
                                <hr>


                                <strong><i class="fas fa-lock-open mr-1"></i>Humidity Address:</strong>
                                <b class="text-muted ml-2">  {{$showDetailHardware['humidity_address']}}</b>
                                <hr>


                                <strong><i class="fas fa-lock-open mr-1"></i>Battery Address:</strong>
                                <b class="text-muted ml-2">  {{$showDetailHardware['battery_address']}}</b>
                                <hr>

                            @endif

                            <a href="/editHardwareShow/{{$showDetailHardware->id}}" class="btn btn-primary btn-block"><i class="fa fa-edit mr-2"></i><b>Hardware Edit</b></a>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

                <div class="col-md-6">
                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">About Hardware</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <strong><i class="fas fa-sim-card mr-1"></i>SKU :</strong>

                            <p class="text-muted">
                                {{$showDetailHardware->sku}}
                            </p>

                            <hr>

                            <strong><i class="fas fa-sim-card mr-1"></i> SN:</strong>

                            <p class="text-muted">{{$showDetailHardware->sn}}</p>

                            <hr>

                            <strong><i class="fas fa-sort mr-1"></i>Status:</strong>

                            <b class="text-muted ml-2"> {{$showDetailHardware->status}}</b>
                            <hr>
                            <a href="/showHardware" class="btn btn-outline-primary btn-block"><i class="fa fa-eye mr-2"></i><b> Show All Hardware</b></a>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>

    </div>
@endsection
