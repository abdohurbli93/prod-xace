@extends('includes.footerDataTable')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> View Hardware </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-6">
                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Available Stock </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <div class="row">

                                <div class="col-6 col-md-4 text-center">
                                    <input type="text" class="knob" value="{{$countSIM}}" data-skin="tron" data-thickness="0.2" data-width="90"
                                           data-height="90" data-fgColor="#3c8dbc" data-readonly="true">
                                    <div class="knob-label">SIM Number= "<b>{{$countSIM}}</b>"</div>
                                </div>

                                <div class="col-6 col-md-4 text-center">
                                    <input type="text" class="knob" value="{{$countSensor}}" data-skin="tron" data-thickness="0.2" data-width="90"
                                           data-height="90" data-fgColor="#dc3545" data-readonly="true">
                                    <div class="knob-label">Sensor Number = "<b>{{$countSensor}}</b>"</div>
                                </div>

                                <div class="col-6 col-md-4 text-center">
                                    <input type="text" class="knob" value="{{$countTotal}}" data-skin="tron" data-thickness="0.2" data-width="90"
                                           data-height="90" data-fgColor="#4caf50" data-readonly="true">
                                    <div class="knob-label">Number Total = "<b>{{$countTotal}}</b>"</div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title"> Sold Out</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <div class="row">

                                <div class="col-6 col-md-4 text-center">
                                    <input type="text" class="knob" value="{{$countSIMSold}}" data-skin="tron" data-thickness="0.2" data-width="90"
                                           data-height="90" data-fgColor="#3c8dbc" data-readonly="true">
                                    <div class="knob-label">SIM Number Sold= "<b>{{$countSIMSold}}</b>"</div>
                                </div>

                                <div class="col-6 col-md-4 text-center">
                                    <input type="text" class="knob" value="{{$countSensorSold}}" data-skin="tron" data-thickness="0.2" data-width="90"
                                           data-height="90" data-fgColor="#dc3545" data-readonly="true">
                                    <div class="knob-label">Sensor Number Sold= "<b>{{$countSensorSold}}</b>"</div>
                                </div>

                                <div class="col-6 col-md-4 text-center">
                                    <input type="text" class="knob" value="{{$countTotalSold}}" data-skin="tron" data-thickness="0.2" data-width="90"
                                           data-height="90" data-fgColor="#4caf50" data-readonly="true">
                                    <div class="knob-label">Number Total Sold= "<b>{{$countTotalSold}}</b>"</div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">View The List Of Hardware</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="overflow-x: auto">
                            <table id="example1" class="table table-bordered table-striped text-center" >
                                <thead>
                                <tr>
                                    <th>Device Name</th>
                                    <th>Sku</th>
                                    <th>Sn</th>
                                    <th>Status</th>
                                    <th>Hardware Type</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody >
                                @foreach($hardwares as $hardware)
                                    <tr>
                                        <input type="hidden" name="id" value="{{$hardware->id}}"  >
                                        <td>{{$hardware->device_name}}</td>
                                        <td>{{$hardware->sku}}</td>
                                        <td>{{$hardware->sn}}</td>
                                        <td>{{$hardware->status}}</td>
                                        <td>{{$hardware->hardware_type}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a type="button" title="Delete" onclick="deleteHardware({{$hardware->id}})" href="#" class="btn btn-primary"><i class="fas fa-trash"></i></a>
                                                <a type="button" title="View" href="/showDetailHardware/{{$hardware->id}}" class="btn bg-dark"><i class="fa fa-folder"></i></a>
                                                <a type="button" title="Edit" href="/editHardwareShow/{{$hardware->id}}" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Device Name</th>
                                    <th>Sku</th>
                                    <th>Sn</th>
                                    <th>Status</th>
                                    <th>Hardware Type</th>
                                    <th>#</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>

                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="{{ asset('js/include/delete_user.js') }}"></script>


@endsection
