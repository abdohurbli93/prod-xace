@extends('index')
@section('content')
    <style>
        .input-group-text {background-color: #fff;border: 1px solid #ced4da;}
        .form-control {background-color: #fff;border: 1px solid #ced4da;}
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Add Hardware </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Enter Information:</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('addHardwareModel')}}">
                                    @csrf
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Device Name:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" placeholder="Device Name" id="device_name"  name="device_name"  class="form-control @error('device_name') is-invalid @enderror" value="{{ old('device_name') }}" required autocomplete="device_name" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Sku:</label>
                                                <div class="input-group">
                                                    <input type="number" min="000" placeholder="Sku" id="sku"  name="sku" class="form-control @error('sku') is-invalid @enderror" value="{{ old('sku') }}" required autocomplete="sku" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>SN:</label>
                                                <div class="input-group">
                                                    <input type="text" placeholder="Sn" id="sn"  name="sn" class="form-control @error('sn') is-invalid @enderror" value="{{ old('sn') }}" required autocomplete="sn" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Choose Vendor:</label>
                                                <select  class="form-control @error('vendor_id') is-invalid @enderror" value="{{ old('vendor_id') }}" required autocomplete="vendor_id" autofocus  name="vendor_id">
                                                    <option value="" selected disabled>Choose..</option>
                                                    @foreach($vendors as $vendor)
                                                        <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Status:</label>
                                                <select  class="form-control @error('status') is-invalid @enderror" value="{{ old('status') }}" required autocomplete="status" autofocus name="status">
                                                    <option value="" disabled selected>Choose..</option>
                                                    <option value="live">live</option>
                                                    <option value="down">down</option>
                                                </select>
                                            </div>

                                        </div>


                                        <div class="col-md-6">
                                            <div class=" col-12">
                                                <div class="form-group">
                                                    <label>Hardware Type:</label>
                                                    <select  id="selector" class="form-control @error('hardware_type') is-invalid @enderror" value="{{ old('hardware_type') }}" required autocomplete="hardware_type" autofocus  name="hardware_type">
                                                        <option value="" disabled selected>Choose..</option>
                                                        <option value="sensor">sensor</option>
                                                        <option value="sim">sim</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="chooseDiv col-12" id="sensor"  style="display: none">
                                                <div class="form-group">
                                                    <label>Hostname:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Hostname" id="hostname"  name="hostname" class="form-control @error('hostname') is-invalid @enderror" value="{{ old('hostname') }}" autocomplete="hostname" autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Temperature Address:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Temperature Address" id="temperature_address"  name="temperature_address" class="form-control @error('temperature_address') is-invalid @enderror" value="{{ old('temperature_address') }}" autocomplete="temperature_address" autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Humidity Address:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Humidity Address" id="humidity_address" class="form-control @error('humidity_address') is-invalid @enderror" value="{{ old('humidity_address') }}"  autocomplete="humidity_address" autofocus name="humidity_address" >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Battery Address:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="Battery Address" id="battery_address" class="form-control @error('battery_address') is-invalid @enderror" value="{{ old('battery_address') }}" autocomplete="battery_address" autofocus  name="battery_address">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="chooseDiv col-12" id="sim"  style="display: none">

                                                <div class="form-group">
                                                    <label>IMSI:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="IMSI" id="imsi"  name="imsi" class="form-control @error('imsi') is-invalid @enderror" value="{{ old('imsi') }}" autocomplete="imsi" autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>MSISDN:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="MSISDN" id="msisdn"  name="msisdn" class="form-control @error('msisdn') is-invalid @enderror" value="{{ old('msisdn') }}" autocomplete="msisdn" autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>ICCID:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="ICCID" id="iccid"  name="iccid" class="form-control @error('iccid') is-invalid @enderror" value="{{ old('iccid') }}" autocomplete="iccid" autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>PIN:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="PIN" id="pin"  name="pin" class="form-control @error('pin') is-invalid @enderror" value="{{ old('pin') }}" autocomplete="pin" autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>PUK:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="PUK" id="puk"  name="puk" class="form-control @error('puk') is-invalid @enderror" value="{{ old('puk') }}" autocomplete="puk" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary text-white"><i class="fa fa-check"></i>&nbsp;ADD</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(function() {
            $('#selector').change(function(){
                $('.chooseDiv').hide();
                $('#' + $(this).val()).show();
                $('#' + $(this).val()).show();
            });
        });
    </script>

@endsection
