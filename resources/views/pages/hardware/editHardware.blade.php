@extends('index')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Hardware</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Enter New Information:</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('updateHardwareModel')}}">
                                    @csrf
                                    <input value="{{$editHardwareShow->id}}" name="id" hidden>
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Device Name:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" value="{{$editHardwareShow->device_name}}" id="device_name" class="form-control" name="device_name" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Sku:</label>
                                                <div class="input-group">
                                                    <input type="number" min="0" value="{{$editHardwareShow->sku}}"  id="sku" class="form-control" name="sku" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Sn:</label>
                                                <div class="input-group">
                                                    <input type="text" value="{{$editHardwareShow->sn}}" id="sn" class="form-control" name="sn" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Choose Vendor:</label>
                                                <select class="form-control" required  name="vendor_id">
                                                    <option value="{{$editHardwareShow->vendor_id}}">{{$editHardwareShow->vendor_name}}</option>
                                                    @foreach($vendors as $vendor)
                                                        <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Status:</label>
                                                <select class="form-control" name="status">
                                                    <option value="live" {{$editHardwareShow->status=='live' ? 'selected':''}}>live</option>
                                                    <option value="down" {{$editHardwareShow->status=='down' ? 'selected':''}}>down</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class=" col-12">
                                                <div class="form-group">
                                                    <label>Hardware Type:</label>
                                                    <select class="form-control" id="selector" name="hardware_type">
                                                        <option value="sim" {{$editHardwareShow->hardware_type=='sim' ? 'selected':''}}>sim</option>
                                                        <option value="sensor" {{$editHardwareShow->hardware_type=='sensor' ? 'selected':''}}>sensor</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="chooseDiv col-12" id="sensor"  style=" {{$editHardwareShow->hardware_type=='sensor' ? '':'display:none'}} ">
                                                <div class="form-group">
                                                    <label>Hostname:</label>
                                                    <div class="input-group">
                                                        <input type="number" value="{{$editHardwareShow->hostname}}" id="hostname" class="form-control" name="hostname">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Temperature Address:</label>
                                                    <div class="input-group">
                                                        <input type="text" value="{{$editHardwareShow->temperature_address}}" id="temperature_address" class="form-control" name="temperature_address" >
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Humidity Address:</label>
                                                    <div class="input-group">
                                                        <input type="text" value="{{$editHardwareShow->humidity_address}}" id="humidity_address" class="form-control" name="humidity_address">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Battery Address:</label>
                                                    <div class="input-group">
                                                        <input type="text" value="{{$editHardwareShow->battery_address}}" id="battery_address" class="form-control" name="battery_address">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="chooseDiv col-12" id="sim"  style=" {{$editHardwareShow->hardware_type=='sim' ? '':'display:none'}} ">
                                                <div class="form-group">
                                                    <label>IMSI:</label>
                                                    <div class="input-group">
                                                        <input type="text" value="{{$editHardwareShow->imsi}}" id="imsi" class="form-control" name="imsi">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label>MSISDN:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="MSISDN" id="msisdn"  name="msisdn" class="form-control " value="{{$editHardwareShow->msisdn}}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>ICCID:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="ICCID" id="iccid"  name="iccid" class="form-control" value="{{$editHardwareShow->iccid}}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>PIN:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="PIN" id="pin"  name="pin" class="form-control" value="{{$editHardwareShow->pin}}">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>PUK:</label>
                                                    <div class="input-group">
                                                        <input type="text" placeholder="PUK" id="puk"  name="puk" class="form-control" value="{{$editHardwareShow->puk}}">
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                        <div class="form-group">
                                            <label><br></label>
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary font-weight-bold"><i class=" fa fa-check "></i>&nbsp;Save</button>
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>
        </section>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(function() {
            $('#selector').change(function(){
                $('.chooseDiv').hide();
                $('#' + $(this).val()).show();
            });
        });
    </script>
@endsection
