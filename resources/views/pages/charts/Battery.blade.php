<style>
    #chartBattery {
        width: 100%;
        height: 500px;
    }
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
{{--<script>--}}
{{--    var data1 = {!! json_encode($data1->toArray()) !!};--}}
{{--    console.log(data1);--}}
{{--    am4core.ready(function() {--}}

{{--// Themes begin--}}
{{--        am4core.useTheme(am4themes_dataviz);--}}
{{--        am4core.useTheme(am4themes_animated);--}}
{{--// Themes end--}}

{{--// Create chart instance--}}
{{--        var chart = am4core.create("chartBattery", am4charts.XYChart);--}}

{{--// Add data--}}
{{--        chart.data=[];--}}
{{--        for (let i = 0; i <data1.length ; i++) {--}}
{{--            chart.data.push({--}}
{{--                "date": data1[i].date_time,--}}
{{--                "value": [data1[i].value]--}}
{{--            });--}}
{{--        }--}}

{{--// Set input format for the dates--}}
{{--        chart.dateFormatter.inputDateFormat = "yyyy-MM-dd hh:mm:ss";--}}

{{--// Create axes--}}

{{--        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());--}}
{{--        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());--}}

{{--// Create series--}}
{{--        var series = chart.series.push(new am4charts.LineSeries());--}}
{{--        series.dataFields.valueY = "value";--}}
{{--        series.dataFields.dateX = "date";--}}
{{--        series.tooltipText = "{value}";--}}
{{--        series.strokeWidth = 2;--}}
{{--        series.minBulletDistance = 15;--}}

{{--// Drop-shaped tooltips--}}
{{--        series.tooltip.background.cornerRadius = 20;--}}
{{--        series.tooltip.background.strokeOpacity = 0;--}}
{{--        series.tooltip.pointerOrientation = "vertical";--}}
{{--        series.tooltip.label.minWidth = 40;--}}
{{--        series.tooltip.label.minHeight = 40;--}}
{{--        series.tooltip.label.textAlign = "middle";--}}
{{--        series.tooltip.label.textValign = "middle";--}}

{{--// Make bullets grow on hover--}}
{{--        var bullet = series.bullets.push(new am4charts.CircleBullet());--}}
{{--        bullet.circle.strokeWidth = 2;--}}
{{--        bullet.circle.radius = 4;--}}
{{--        bullet.circle.fill = am4core.color("#f00");--}}

{{--        var bullethover = bullet.states.create("hover");--}}
{{--        bullethover.properties.scale = 1.3;--}}

{{--// Make a panning cursor--}}
{{--        chart.cursor = new am4charts.XYCursor();--}}
{{--        chart.cursor.behavior = "panXY";--}}
{{--        chart.cursor.xAxis = dateAxis;--}}
{{--        chart.cursor.snapToSeries = series;--}}

{{--// Create vertical scrollbar and place it before the value axis--}}
{{--        chart.scrollbarY = new am4core.Scrollbar();--}}
{{--        chart.scrollbarY.parent = chart.leftAxesContainer;--}}
{{--        chart.scrollbarY.toBack();--}}

{{--// Create a horizontal scrollbar with previe and place it underneath the date axis--}}
{{--        chart.scrollbarX = new am4charts.XYChartScrollbar();--}}
{{--        chart.scrollbarX.series.push(series);--}}
{{--        chart.scrollbarX.parent = chart.bottomAxesContainer;--}}

{{--        dateAxis.start = 0.79;--}}
{{--        dateAxis.keepSelection = true;--}}

{{--        // var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());--}}
{{--        // valueAxis.min = 0;--}}
{{--        // valueAxis.max = 15;--}}
{{--        // valueAxis.strictMinMax = true;--}}
{{--        //--}}
{{--        // var DateAxis = chart.xAxes.push(new am4charts.DateAxis());--}}
{{--        // DateAxis.min = date.setHours(0, 0, 0, 0);--}}
{{--        // DateAxis.max = 1500;--}}
{{--        // DateAxis.strictMinMax = true;--}}

{{--    }); // end am4core.ready()--}}
{{--</script>--}}



<!-- HTML -->
<div id="chartBattery"></div>

