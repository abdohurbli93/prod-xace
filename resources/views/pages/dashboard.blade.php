@extends('index')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            
                            <div class="card-tools">

                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        @for( $i=0;$i<count($data);$i++)
                            <div class="card-body p-0">
                                <div class="mailbox-controls">

                                    <div class="btn-group ml-1 pt-2">
                                         <b> Zone 1</b>
                                    </div>

                                    <div class="float-right mr-5">
                                        <div class="text-center">
                                           Last Updated on <br>
                                            <div class="btn-group">
                                                {{gmdate('F j, Y, g:i a',strtotime($data[0]['temperature']['date_time'])). " GMT"}}
                                               </div>
                                        </div>
                                    </div>
                                   </div>
                                <!-- /.float-right -->
                                <div class="table-responsive mailbox-messages">
                                    <table class="" border="0" style="width: 100%">
                                        <tbody>
                                            <tr class="border-top">
                                                <td class="w-td"></td>
                                                <td class="w-td pl-2 border-left border-bottom"> <b>Gateway</b></td>
                                                <td class="w-td"></td>
                                                <td class="w-td "><b>SN:</b> 121601211383</td>
                                                <td class="w-td "> <a href="#"><img src="" width="35"></a> </td>

                                                @if($data[0]['humidity']['status'] =='Active' )
                                            <td class="w-td"><span class="badge badge-success">Active</span> </td>
                                       @elseif($data[0]['humidity']['status'] =='Inactive')
                                       <td class="w-td"><span class="badge badge-danger">Inactive</span> </td>
                                        @endif
                                                <td class="w-td"></td>
                                                <td class="w-td"></td>
                                                <td class="w-td border-top border-bottom">
                                                    <ul class="navbar-nav ml-auto mr-5">
                                                        <li class="nav-item dropdown">
                                                            <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                                <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                                <div class="dropdown-divider"></div>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <input hidden value="1" id="sku{{$data[0]['sku']}}" >
                                                <input hidden value="humidity" id="type{{$i}}">
                                                <td class="w-td"></td>
                                                <td class="w-td"></td>
                                                <td class="w-td p-2 border-top border-left" >Humidity</td>
                                                <td class="w-td border-top"><b>SN:</b> 422000031</td>
                                                <td class="w-td border-top">
                                                    <a href="http://xace.tech/api/testing13?sku={{$data[0]['sku']}}&type=humidity">
                                                        <img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                               @if($data[0]['humidity']['status'] =='Active' )
                                                    <td class="w-td border-top"><span class="badge badge-success">Active</span> </td>
                                               @elseif($data[0]['humidity']['status'] =='Inactive')
                                                    <td class="w-td border-top"><span class="badge badge-danger">Inactive</span> </td>
                                                @endif
                                                <td class="w-td border-top pt-2 pr-3">{{$data[0]['humidity']['data']}} %</td>
                                                @if($data[0]['humidity']['status'] =='Inactive' )
                                                <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-empty fa-2x text-center" style="border-radius: 20px; color:red; "></p></td>
                                            @elseif($data[0]['humidity']['status'] =='Active')
                                            <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-full fa-2x text-center" style="border-radius: 20px; color:green; "></p></td>
                                            @endif

                                                <td class="w-td border-top border-bottom">
                                                    <ul class="navbar-nav ml-auto mr-5">
                                                        <li class="nav-item dropdown">
                                                            <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                                <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                                <div class="dropdown-divider"></div>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <input hidden value="1" id="sku{{$data[0]['sku']}}" >
                                                <input hidden value="temperature" id="type{{$i+10}}">
                                                <td class="w-td"></td>
                                                <td class="w-td"></td>
                                                <td class="w-td p-2 border-left border-top border-bottom">Temperature</td>
                                                <td class="w-td border-top border-bottom"><b>SN:</b> 422000026</td>
                                                <td class="w-td  border-top border-bottom">
                                                    <a href="http://xace.tech/api/testing13?sku={{$data[0]['sku']}}&type=temperature">
                                                        <img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                                @if($data[0]['temperature']['status'] =='Inactive' )
                                                    <td class="w-td border-top border-bottom"><span class="badge badge-danger">Inactive</span> </td>
                                                @elseif($data[0]['temperature']['status'] =='Active')
                                                    <td class="w-td border-top border-bottom"><span class="badge badge-success">Active</span> </td>
                                                @endif
                                                <td class="w-td border-top border-bottom">{{$data[0]['temperature']['data']}} C</td>
                                                @if($data[0]['temperature']['status'] =='Inactive' )
                                                <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-empty fa-2x text-center" style="border-radius: 20px; color:red; "></p></td>
                                            @elseif($data[0]['temperature']['status'] =='Active')
                                            <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-full fa-2x text-center" style="border-radius: 20px; color:green; "></p></td>
                                            @endif

                                                <td class="w-td border-top border-bottom">
                                                    <ul class="navbar-nav ml-auto mr-5">
                                                        <li class="nav-item dropdown">
                                                            <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                                <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                                <div class="dropdown-divider"></div>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                                <!-- /.mail-box-messages -->
                            </div>

                            <div class="card-body pt-5 pl-0 pr-0">
                                <div class="mailbox-controls border-top border-left border-right">

                                        <div class="btn-group ml-1 pt-2">
                                            <b> Zone 2</b>
                                        </div>

                                        <div class="float-right mr-5">
                                            <div class="text-center">
                                                Last Updated on <br>
                                                <div class="btn-group">
                                                    {{gmdate('F j, Y, g:i a',strtotime($data[1]['temperature']['date_time'])). " GMT"}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.float-right -->
                                <div class="table-responsive mailbox-messages">
                                    <table class="" border="0" style="width: 100%">
                                        <tbody>
                                        <tr class="border-top">
                                            <td class="w-td"></td>
                                            <td class="w-td pl-2 border-left border-bottom"> <b>Gateway</b></td>
                                            <td class="w-td"></td>
                                            <td class="w-td"><b>SN:</b> 121601211408</td>
                                            <td class="w-td "> <a href="#"><img src="" width="35"></a> </td>
                                            @if($data[1]['humidity']['status'] =='Active' )
                                            <td class="w-td"><span class="badge badge-success">Active</span> </td>
                                       @elseif($data[1]['humidity']['status'] =='Inactive')
                                       <td class="w-td"><span class="badge badge-danger">Inactive</span> </td>
                                        @endif

                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <input hidden value="2" id="sku{{$data[1]['sku']}}" >
                                            <input hidden value="humidity" id="type{{$i+1}}">
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-top border-left" >Humidity</td>
                                            <td class="w-td border-top"  ><b>SN:</b> 422000028</td>
                                            <td class="w-td border-top">
                                                <a href="http://xace.tech/api/testing13?sku={{$data[1]['sku']}}&type=humidity">
                                                    <img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                            @if($data[1]['humidity']['status'] =='Inactive' )
                                                <td class="w-td border-top"><span class="badge badge-danger">Inactive</span> </td>
                                            @elseif($data[1]['humidity']['status'] =='Active')
                                                <td class="w-td border-top"><span class="badge badge-success">Active</span> </td>
                                            @endif
                                            <td class="w-td border-top pt-2 pr-3">{{$data[1]['humidity']['data']}} %</td>
                                            @if($data[1]['humidity']['status'] =='Inactive' )
                                            <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-empty fa-2x text-center" style="border-radius: 20px; color:red; "></p></td>
                                        @elseif($data[1]['humidity']['status'] =='Active')
                                        <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-full fa-2x text-center" style="border-radius: 20px; color:green; "></p></td>
                                        @endif
                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <input hidden value="2" id="sku{{$data[1]['sku']}}" >
                                            <input hidden value="temperature" id="type{{$i+11}}">
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-left border-top border-bottom">Temperature</td>
                                            <td class="w-td border-top border-bottom"><b>SN:</b> 422000027</td>
                                            <td class="w-td border-top border-bottom">
                                                <a href="http://xace.tech/api/testing13?sku={{$data[1]['sku']}}&type=temperature">
                                                    <img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                            @if($data[1]['temperature']['status'] =='Inactive' )
                                                <td class="w-td border-top border-bottom"><span class="badge badge-danger">Inactive</span> </td>
                                            @elseif($data[1]['temperature']['status'] =='Active')
                                                <td class="w-td border-top border-bottom"><span class="badge badge-success">Active</span> </td>
                                            @endif
                                            <td class="w-td border-top border-bottom pt-2 pr-3">{{$data[1]['temperature']['data']}} C</td>
                                            @if($data[1]['temperature']['status'] =='Inactive' )
                                            <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-empty fa-2x text-center" style="border-radius: 20px; color:red; "></p></td>
                                        @elseif($data[1]['temperature']['status'] =='Active')
                                        <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-full fa-2x text-center" style="border-radius: 20px; color:green; "></p></td>
                                        @endif
                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                            </div>

                            <div class="card-body pt-5 pl-0 pr-0 ">
                                <div class="mailbox-controls border-top border-left border-right">

                                        <div class="btn-group ml-1 pt-2">
                                            <b> Zone 3</b>
                                        </div>

                                        <div class="float-right mr-5">
                                            <div class="text-center">
                                                Last Updated on <br>
                                                <div class="btn-group">
                                                    {{gmdate('F j, Y, g:i a',strtotime($data[2]['temperature']['date_time'])). " GMT"}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.float-right -->
                                <div class="table-responsive mailbox-messages">
                                    <table class="" border="0" style="width: 100%">
                                        <tbody>
                                        <tr class="border-top">
                                            <td class="w-td"></td>
                                            <td class="w-td pl-2 border-left border-bottom"> <b>Gateway</b></td>
                                            <td class="w-td"></td>
                                            <td class="w-td"><b>SN:</b> 121601211426</td>
                                            <td class="w-td "> <a href="#" ><img src="" width="35"></a> </td>
                                            @if($data[2]['humidity']['status'] =='Active' )
                                            <td class="w-td"><span class="badge badge-success">Active</span> </td>
                                       @elseif($data[2]['humidity']['status'] =='Inactive')
                                       <td class="w-td"><span class="badge badge-danger">Inactive</span> </td>
                                        @endif
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <input hidden value="3" id="sku{{$data[2]['sku']}}" >
                                            <input hidden value="humidity" id="type{{$i+2}}">
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-top border-left" >Humidity</td>
                                            <td class="w-td border-top"><b>SN:</b> 422000029</td>
                                            <td class="w-td border-top">
                                                <a href="http://xace.tech/api/testing13?sku={{$data[2]['sku']}}&type=humidity">
                                                    <img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                            @if($data[2]['humidity']['status'] =='Inactive' )
                                                <td class="w-td border-top"><span class="badge badge-danger">Inactive</span> </td>
                                            @elseif($data[2]['humidity']['status'] =='Active')
                                                <td class="w-td border-top"><span class="badge badge-success">Active</span> </td>
                                            @endif
                                            <td class="w-td border-top pt-2 pr-3">{{$data[2]['humidity']['data']}} %</td>
                                            @if($data[2]['humidity']['status'] =='Inactive' )
                                                <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-empty fa-2x text-center" style="border-radius: 20px; color:red; "></p></td>
                                            @elseif($data[2]['humidity']['status'] =='Active')
                                            <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-full fa-2x text-center" style="border-radius: 20px; color:green; "></p></td>
                                            @endif

                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <input hidden value="3" id="sku{{$data[2]['sku']}}" >
                                            <input hidden value="temperature" id="type{{$i+22}}">
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-left border-top border-bottom">Temperature</td>
                                            <td class="w-td border-top border-bottom"><b>SN:</b> 422000030</td>
                                            <td class="w-td border-top border-bottom">
                                                <a href="http://xace.tech/api/testing13?sku={{$data[2]['sku']}}&type=temperature">
                                                    <img src="{{url('/design/img/')}}/123.png" width="35">
                                                </a> </td>
                                            @if($data[2]['temperature']['status'] =='Inactive' )
                                                <td class="w-td border-top border-bottom"><span class="badge badge-danger">Inactive</span> </td>
                                            @elseif($data[2]['temperature']['status'] =='Active')
                                                <td class="w-td border-top border-bottom"><span class="badge badge-success">Active</span> </td>
                                            @endif
                                            <td class="w-td border-top border-bottom pt-2 pr-3">{{$data[2]['temperature']['data']}} C</td>
                                            @if($data[2]['temperature']['status'] =='Inactive' )
                                            <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-empty fa-2x text-center" style="border-radius: 20px; color:red; "></p></td>
                                        @elseif($data[2]['temperature']['status'] =='Active')
                                        <td class="w-td border-top border-bottom pt-2 pr-3 text-center" style="width: 95px; "><p type="" class="fas fa-battery-full fa-2x text-center" style="border-radius: 20px; color:green; "></p></td>
                                        @endif

                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>


                                    <!-- /.mail-box-messages -->
                                </div>

                            <div class="card-body pt-5 pl-0 pr-0 ">
                                   <div class="mailbox-controls border-top border-left border-right">

                                        <div class="btn-group ml-1 pt-2">
                                            <b> Zone 4</b>
                                        </div>

                                        <div class="float-right mr-5">
                                            <div class="text-center">
                                                Last Updated on <br>
                                                <div class="btn-group">
                                                    {{gmdate('F j, Y, g:i a',strtotime($data[3]['temperature']['date_time'])). " GMT"}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.float-right -->
                                <div class="table-responsive mailbox-messages">
                                    <table class="" border="0" style="width: 100%">
                                        <tbody>
                                        <tr class="border-top">
                                            <td class="w-td"></td>
                                            <td class="w-td pl-2 border-left border-bottom"> <b>Gateway</b></td>
                                            <td class="w-td"></td>
                                            <td class="w-td"><b>SN:</b> 121601211539</td>
                                            <td class="w-td "> <a href="#"><img src="" width="35"></a> </td>
                                            @if($data[3]['humidity']['status'] =='Active' )
                                            <td class="w-td"><span class="badge badge-success">Active</span> </td>
                                       @elseif($data[3]['humidity']['status'] =='Inactive')
                                       <td class="w-td"><span class="badge badge-danger">Inactive</span> </td>
                                        @endif
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>

                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>

                                        <tr>
                                            <input hidden value="5" id="sku{{$data[3]['sku']}}}" >
                                            <input hidden value="temperature" id="type{{$i+33}}">
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-left border-top border-bottom">Temperature</td>
                                            <td class="w-td border-top border-bottom"><b>SN:</b> 422000032</td>
                                            <td class="w-td border-top border-bottom">
                                                <a href="http://xace.tech/api/testing13?sku={{$data[3]['sku']}}&type=temperature">
                                                    <img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                              @if($data[3]['temperature']['status'] =='Inactive' )
                                                 <td class="w-td border-top border-bottom"><span class="badge badge-danger">Inactive</span> </td>
                                            @elseif($data[3]['temperature']['status'] =='Active')
                                                <td class="w-td border-top border-bottom"><span class="badge badge-success">Active</span> </td>
                                            @endif
                                            <td class="w-td border-top border-bottom pt-2 pr-3">{{$data[3]['temperature']['data']}} C</td>
                                            <td class="w-td border-top border-bottom pt-2 pr-3" style="width: 95px;text-align: center;">
                                                @if($data[3]['temperature']['status'] =='Inactive' )
                                                <p type="" class="fas fa-battery-empty fa-2x text-center" style="border-radius: 10px; color:red; padding-left:19px; text-align: center;"></p></td>
                                            @elseif($data[3]['temperature']['status'] =='Active')
                                            <p type="" class="fas fa-battery-full fa-2x text-center" style="border-radius: 10px; color:green; padding-left:19px;"></p></td>
                                            @endif

                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                            </div>
                        @break
                        @endfor

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>

    <script>
        function showChart($i) {
            var sku=$('#sku' + $i).val();
            var type=$('#type' + $i).val();
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            console.log(sku);
            console.log(type);
            jQuery.ajax({
                url: '/show',
                type: "POST",
                data: {_token: CSRF_TOKEN ,type:type,sku:sku},
                success: function (result) {
                    console.log('done');
                }
            });

        }
    </script>


@endsection


