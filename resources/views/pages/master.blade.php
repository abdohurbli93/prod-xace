@include('includes.header')
<link rel="stylesheet" href="{{url('/design')}}/dist/css/master.css">
<header class="fixed-top bg-white">
    <nav class="navbar navbar-expand-lg ">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img class="img-fluid" width="50px" src="{{url('/')}}/design/img/F_logo.png" alt="logo">
                <span class="brand-text text-dark"><b>xACE</b></span>
            </a>
            <button class="navbar-toggler fas fa-bars border-0" type="button" data-toggle="collapse" data-target="#navigation">
                <i class="ti-align-right"></i>
            </button>
            <div class="collapse navbar-collapse text-center" id="navigation" style="direction: rtl">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="btn text-white nav-link" href="/login">Sign in</a></li>
                    <li class="nav-item"><a class="nav-link" href="#Case">Case Studies</a></li>
                    <li class="nav-item"><a class="nav-link" href="#Partners">Partners</a></li>
                    <li class="nav-item"><a class="nav-link" href="#Features">Features</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
    <!-- Brand Logo -->


    <section id="">
        <!-- End Section Icons -->
        <div class="hero">
            <div class="container">
                <div class="row">
                    <!-- Content Column -->
                    <div class="col-lg-5 col-md-6">
                            <p class="small">WELCOME TO XACE</p>
                            <h1 class="mb-4">Inspiring Change Through Connectivity</h1>
                            <p class="info-1">
                                xACE offers convenient Global Connectivity that accelerates deployment of IoT. Easily connect with over 550 networks worldwide.
                            </p>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <img src="{{url('/')}}/design/img/sim-stack-mobile-hero.png">
                    </div>

                    <!-- Image Column -->

                </div>
            </div>
        </div>
    </section>

    <section id="Features" class="py-5">
        <!-- End Section Icons -->
        <div class="container pb-5">
            <div class="row text-blue text-center">
                <!-- Content Column -->
                <div class="col-lg-3 col-md-6 my-auto py-3">
                    <img src="{{url('/')}}/design/img/section2/sim.png" alt="Image" class="icon-img">
                    <br><h4 class="font-weight-bolder">Single SIM<br> Deployment<br></h4>
                    <div class="info-2">Deploy new projects<br> worldwide with same SIM</div>
                </div>

                <div class="col-lg-3 col-md-6 my-auto py-3">
                    <img src="{{url('/')}}/design/img/section2/global.png" alt="Image" class="icon-img">
                    <br><h4 class="font-weight-bolder">Global<br> Connectivity<br></h4>
                    <div class="info-2">Easy to connect with Global<br> Connectivity of over 550<br> networks 2G / 3G / 4G.</div>
                </div>

                <div class="col-lg-3 col-md-6 my-auto py-3">
                    <img src="{{url('/')}}/design/img/section2/control.png" alt="Image" class="icon-img">
                    <br><h4 class="font-weight-bolder">Full<br> Control<br></h4>
                    <div class="info-2">Quick integration with <br> customized control, alerts & <br> data insights </div>
                </div>

                <div class="col-lg-3 col-md-6 my-auto py-3">
                    <img src="{{url('/')}}/design/img/section2/device.png" alt="Image" class="icon-img">
                    <br><h4 class="font-weight-bolder">Device<br> Agnostic<br></h4>
                    <div class="info-2">Flexible platform that <br> supports all IoT devices</div>
                </div>
                <!-- Image Column -->

            </div>
        </div>
    </section>

    <section class="partners py-5" id="Partners">
        <div class="container py-5">

                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-6 my-auto py-3">
                        <img class="img-fluid" src="{{url('/')}}/design/img/section3/amazon.png" width="160" alt="">
                    </div>
                    <div class="col-lg-3 col-md-6 my-auto py-3">
                        <img class="img-fluid" src="{{url('/')}}/design/img/section3/dhl.png" width="210" alt="">
                    </div>
                    <div class="col-lg-3 col-md-6 my-auto py-3">
                        <img class="img-fluid" src="{{url('/')}}/design/img/section3/panasonic.png" width="210" alt="">
                    </div>
                    <div class="col-lg-3 col-md-6 my-auto py-3">
                        <img class="img-fluid" src="{{url('/')}}/design/img/section3/idemia.png" width="160" alt="">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-7 col-md-7">
                        <div class=" text-center pt-2">
                            <p>XACE WORKS WITH PARTNERS THAT CHALLENGE US TO THINK OF <br> NEW WAYS TO HELP THEM IMPROVE THROUGH CONNECTIVITY</p>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-6 my-auto py-3">
                        <img class="img-fluid" src="{{url('/')}}/design/img/section3/arm.png" width="130" alt="">
                    </div>
                    <div class="col-lg-3 col-md-6 my-auto py-3">
                        <img class="img-fluid" src="{{url('/')}}/design/img/section3/texas-instruments.png" width="150" alt="">
                    </div>
                    <div class="col-lg-3 col-md-6 my-auto py-3">
                        <img class="img-fluid" src="{{url('/')}}/design/img/section3/XH Smart.png" width="90" alt="">
                    </div>
                </div>
        </div>
    </section>

    <section id="Case" class="studies py-5">
        <div class="container py-5">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-md-9">
                    <div class="section-title text-center">
                        <h2 class="mt-3 font-weight-bolder mb-4 position-relative content-title">Take control of the connectivity cycle</h2>
                        <p class="mb-5 info-2">Easily deploy single SIM solution to the best network across our extensive coverage.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class=" m-4 col-md-5">
                    <div class="card h-100">
                        <div class="row no-gutters">
                            <img src="{{url('/')}}/design/img/section4/froxen-food.jpg" class="img-fluid w-100" style="height: 300px"alt="">
                            <div class="card-body ml-2">
                                <h3 class="mb-3">Quick On-site Temperature Data Collection</h3>
                                <p class="card-text info-2 mb-4">Frozen food business owner finds that energy <br> cost is off the roof in recent months, and they are looking for the root cause of the spike.</p>
                                <!-- <a href="/" class="text-p">Learn More</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class=" m-4 col-md-5">
                    <div class="card h-100">
                        <div class="row no-gutters">
                            <img src="{{url('/')}}/design/img/section4/street-light.jpg" class="img-fluid w-100" style="height: 300px"alt="">
                            <div class="card-body ml-2">
                                <h3 class="mb-3">Quick Deployment for Street Lighting Control</h3>
                                <p class="card-text info-2 mb-4">Street lighting operator is finding it hard to secure connectivity along stretches of roads thus slowing down the deployment of street lighting control.</p>
                                <!-- <a href="/" class="text-p">Learn More</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class=" m-4 col-md-5">
                    <div class="card h-100">
                        <div class="row no-gutters">
                            <img src="{{url('/')}}/design/img/section4/fleet.jpg" class="img-fluid w-100" style="height: 300px"alt="">
                            <div class="card-body">
                                <h3 class="mb-3">Better And Wider Connectivity for Fleet Management</h3>
                                <p class="card-text info-2 mb-4">Logistics company and rental cars find tough to secure stable connectivity and monitor assets when the assests travel across a few countries.</p>
                                <!-- <a href="/" class="text-p">Learn More</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class=" m-4 col-md-5">
                    <div class="card h-100">
                        <div class="row no-gutters">
                            <img src="{{url('/')}}/design/img/section4/energy.jpg" class="img-fluid w-100" style="height: 300px"alt="">
                            <div class="card-body">
                                <h3 class="mb-3">Quick On-site Alternating Current (AC) Data Collection</h3>
                                <p class="card-text info-2 mb-4">Energy company needs to have quicker monitoring to assist them with effective load matching.</p>
                               <!-- <br> <a href="/" class="text-p">Learn More</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class=" m-4 col-md-5">
                    <div class="card h-100">
                        <div class="row no-gutters">
                            <img src="{{url('/')}}/design/img/section4/construction.jpg" class="img-fluid w-100" style="height: 300px"alt="">
                            <div class="card-body">
                                <h3 class="mb-3">Quick On-site Cement Strength Analysis</h3>
                                <p class="card-text info-2 mb-4"> Construction workers are finding ways to speed up construction projects' schedules.</p>
                                <!-- <a href="/" class="text-p">Learn More</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class=" m-4 col-md-5">
                    <div class="card h-100">
                        <div class="row no-gutters">
                            <img src="{{url('/')}}/design/img/section4/elderly.jpg" class="img-fluid w-100" alt="" style="height: 300px">
                            <div class="card-body">
                                <h3 class="mb-3">Stable Connectivity for Elderly SOS Alert</h3>
                                <p class="card-text info-2 mb-4"> SOS alert device marker find it tough to get stable data connection and find it slow to be deployed globally.</p>
                                <!-- <a href="/" class="text-p">Learn More</a> -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>




@include('includes.footer')
