@extends('includes.footerDataTable')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Show Detail </h1>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>

        <section class="content">
            <div class="col-md-12 row">

                <div class="col-md-12">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline ">
                        <div class="card-body box-profile">
                            <table id="example1" class="table table-bordered table-striped text-center">
                                <thead>
                                <tr>
                                    <td colspan="3">Flexiroam level 1-room 1</td>
                                    <td colspan="3">SN: 12345678xxx</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>SN</td>
                                    <td>Value</td>
                                    <td>SKU</td>
                                    <td>Chart</td>
                                    <td>Choose</td>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Temperature</td>
                                        <td>555</td>
                                        <td>66</td>
                                        <td>77</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                    <tr>
                                        <td>Humidity</td>
                                        <td>44</td>
                                        <td>99</td>
                                        <td>25</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <td colspan="3">Flexiroam level 2-room 2</td>
                                        <td colspan="3">SN: 0234756xx</td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>SN</td>
                                        <td>Value</td>
                                        <td>SKU</td>
                                        <td>Chart</td>
                                        <td>Choose</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Humidity</td>
                                        <td>414</td>
                                        <td>499</td>
                                        <td>215</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                    <tr>
                                        <td>dff</td>
                                        <td>344</td>
                                        <td>65</td>
                                        <td>89</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <td colspan="3">Flexiroam level 2-room 2</td>
                                        <td colspan="3">SN: 0234756xx</td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>SN</td>
                                        <td>Value</td>
                                        <td>SKU</td>
                                        <td>Chart</td>
                                        <td>Choose</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Humidity</td>
                                        <td>414</td>
                                        <td>499</td>
                                        <td>215</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                    <tr>
                                        <td>dff</td>
                                        <td>344</td>
                                        <td>65</td>
                                        <td>89</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <td colspan="3">Flexiroam level 2-room 2</td>
                                        <td colspan="3">SN: 0234756xx</td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>SN</td>
                                        <td>Value</td>
                                        <td>SKU</td>
                                        <td>Chart</td>
                                        <td>Choose</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Humidity</td>
                                        <td>414</td>
                                        <td>499</td>
                                        <td>215</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                    <tr>
                                        <td>dff</td>
                                        <td>344</td>
                                        <td>65</td>
                                        <td>89</td>
                                        <td><span class="nav-icon fas fa-wave-square"></span></td>
                                        <td><span class="fa fa-bars "></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

            </div>
        </section>














{{--        <section class="content">--}}
{{--            <div class="col-md-12 row">--}}

{{--                <div class="col-md-12">--}}
{{--                    <!-- Profile Image -->--}}
{{--                    <div class="card card-primary card-outline ">--}}
{{--                        <div class="card-body box-profile">--}}

{{--                            <h2 class="profile-username font-weight-bold text-center">{{$showDetail->device_name}}</h2>--}}

{{--                            <p class="text-muted text-center">{{$showDetail->hardware_type}} </p>--}}

{{--                            <hr>--}}
{{--                            <div class="col-12 row">--}}
{{--                                <div class="col-md-6">--}}
{{--                                    @if($showDetail->hardware_type == 'sensor')--}}

{{--                                        <strong><i class="fas fa-sim-card mr-1"></i> Hostname:</strong>--}}

{{--                                        <b class="text-muted">--}}
{{--                                            <span class="tag tag-primary">{{$showDetail->hostname}}</span>--}}
{{--                                        </b>--}}

{{--                                        <hr>--}}
{{--                                        <strong><i class="nav-icon ion ion-stats-bars mr-1"></i>Temperature Address:</strong>--}}
{{--                                        <b class="text-muted ml-2">  {{$showDetail['temperature_address']}}</b>--}}
{{--                                        <hr>--}}

{{--                                        <strong><i class="nav-icon fas fa-wave-square mr-1"></i>Humidity Address:</strong>--}}
{{--                                        <b class="text-muted ml-2">  {{$showDetail['humidity_address']}}</b>--}}
{{--                                        <hr>--}}

{{--                                    @endif--}}

{{--                                </div>--}}

{{--                                <div class="col-md-6">--}}

{{--                                    <strong><i class="fas fa-sim-card mr-1"></i>SKU:</strong>--}}

{{--                                    <b class="text-muted ml-2"> {{$showDetail->sku}}</b>--}}
{{--                                    <hr>--}}

{{--                                    <strong><i class="fas fa-sim-card mr-1"></i>SN:</strong>--}}

{{--                                    <b class="text-muted ml-2"> {{$showDetail->sn}}</b>--}}
{{--                                    <hr>--}}

{{--                                    <strong><i class="fas fa-sort mr-1"></i>Status:</strong>--}}

{{--                                    <b class="text-muted ml-2"> {{$showDetail->status}}</b>--}}
{{--                                    <hr>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-md-5 row">--}}
{{--                                <a href="/" class="btn btn-outline-primary"><b> Temperature</b></a>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-5 row mt-4">--}}
{{--                            <a href="/" class="btn btn-outline-primary"><b>Humidity </b></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <!-- /.card-body -->--}}
{{--                    </div>--}}
{{--                    <!-- /.card -->--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </section>--}}

    </div>
@endsection
