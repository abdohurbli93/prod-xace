@extends('index')
@section('content')
    <link rel="stylesheet" href="{{url('/design')}}/dist/css/style.css">

    <div class="content-wrapper">
        <input id="data3" hidden name="data" value="{{$data3}}">
        <input id="data4" hidden name="data" value="{{$data4}}">
        <input id="sku" hidden name="data" value="{{$sku}}">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Chart SIM</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                SIM

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date and time range:</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                </div>
                                                <input type="text" name="date" value="" class="form-control float-right" id="res">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Choose Chart Show:</label>
                                            <select class="select2" id="selectorSim" multiple="multiple" data-placeholder="Select" style="width: 100%;">
                                                <option selected value="sim">SIM Details</option>
                                                <option selected value="consumption">All Consumption</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary" onclick="refreshSingleSim()">
                                    <i class="fa fa-spinner fa-spin mr-2" ></i>Refresh2
                                </button>
                            </div>
                        </div>

                        <div class="card card-primary card-outline box blue" id="sim">
                            <div class="card-header">
                                <h3 class="card-title">All SIM</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-md-12" >
                                        @include('pages.charts.Sim')
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <div class="card card-primary card-outline box blue" id="consumption">
                            <div class="card-header">
                                <h3 class="card-title"> Sim Consumption</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" >
                                <div class="row">
                                    <div class="col-md-12" >
                                        @include('pages.charts.new_sim_consumption')
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                </div>


            </div>
        </section>



        <!-- /.content -->
    </div>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#selector').select2();
            $('#selector').on('change', function() {
                var values =  $("#selector").select2("val");
                for(var i = 0; i <values.length;i++){
                    showCharts(values[i].toLowerCase());
                }
            });
            $('#selector').on("select2:unselecting", function(e){
                var values =  $("#selector").select2("val");
                for(var i = 0; i <values.length;i++){
                    hideCharts(values[i].toLowerCase());
                }
            });

            function hideCharts(param){
                $('#'+param).hide();
            }
            function showCharts(param){
                $('#'+param).show();
            }

            $('#selectorSim').select2();
            $('#selectorSim').on('change', function() {
                var values =  $("#selectorSim").select2("val");
                for(var i = 0; i <values.length;i++){
                    showCharts(values[i].toLowerCase());
                }
            });
            $('#selectorSim').on("select2:unselecting", function(e){
                var values =  $("#selectorSim").select2("val");
                for(var i = 0; i <values.length;i++){
                    hideCharts(values[i].toLowerCase());
                }
            });

            function hideCharts(param){
                $('#'+param).hide();
            }
            function showCharts(param){
                $('#'+param).show();
            }


        });
    </script>
    <script src="{{asset('js/chart js/SingleSimChart.js')}}"></script>
@endsection

