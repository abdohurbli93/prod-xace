@extends('includes.footerDataTable')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Show The Device </h1>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">View The List Of Devices</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="overflow-x: auto">
                            <table id="example1" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Device Name</th>
                                    <th>Serial Number</th>
                                    <th>hardware Type</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td>1</td>
                                    <td>Gateway</td>
                                    <td>121601211383</td>
                                    <td>Gateway</td>
                                    <td>Live</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Humidity Sensor</td>
                                    <td>422000031</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Temperature Sensor</td>
                                    <td>422000031</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
