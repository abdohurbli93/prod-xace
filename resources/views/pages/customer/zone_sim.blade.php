@extends('index')

@section('content')
    <style>
        .irs--flat .irs-bar {background-color: #4CAF50;}
        .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color: #4CAF50;}
        .irs--flat .irs-handle>i:first-child {background-color: #4CAF50;}
        .irs--flat .irs-from:before, .irs--flat .irs-to:before, .irs--flat .irs-single:before {border-top-color: #4CAF50;}
        .irs-disabled {opacity: 0.9;}

    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="col-md-12 row">
                        <div class="col-sm-5">
                            <div class="form-group col-md-auto">
                                <label>Total Balance:</label>
                                @isset($currentBalance)
                                    <input value="{{$currentBalance}}MB" class="form-control text-primary text-center font-weight-bold" disabled style="cursor: no-drop;">
                                @endisset
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label>Available Balance:</label>
                                {{--                                @isset($balance)--}}
                                <div class="row margin">
                                    <div class="col-md-12">
                                        <input id="range_1" type="text" value="range_1" disabled>
                                    </div>
                                </div>
                                {{--                                @endisset--}}
                            </div>
                        </div>
                    </div>

                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            {{--                            {{ " herlaksdjfal;skdjfal;sdkjf l;aks ".$data[0]['humidity']['date_time']}}--}}
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                            </div>
                            <!-- /.card-tools -->
                        </div>

                        <!-- /.card-header -->
                        @for( $i=0;$i<count($data);$i++)
                            <div class="card-body pb-3 pl-0 pr-0 ">
                                <div class="mailbox-controls">
                                    <div class="btn-group ml-1 pt-2">
                                        <b> SIM 1</b>
                                    </div>
                                </div>
                                <!-- /.float-right -->
                                <div class="table-responsive mailbox-messages">
                                    <table class="" border="0" style="width: 100%">
                                        <tbody>
                                        <tr class="border-top">
                                            <td class="w-td"></td>
                                            <td class="w-td pl-2 border-left border-bottom"> <b>{{$data[0]['device_name']}}</b></td>
                                            <td class="w-td"></td>
                                            <td class="w-td  border-top border-bottom"><a href="/simStatistic?{{$data[0]['sku']}}"><img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                            <td class="w-td"></td>
                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img class="faa-shake animated" src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#modal-lg"><i class="fa fa-plus faa-pulse animated text-primary fa-fw mr-2"></i>Add Data</a>
                                                            <div class="dropdown-divider"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-top border-left" >Data Usage</td>
                                            <td class="w-td border-top border-bottom"></td>
                                            <td class="w-td border-top pt-2 pr-3">{{$data[0]['data_usage']}}</td>
                                            <td class="w-td border-bottom"></td>
                                        </tr>
                                        <tr>
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-left border-top border-bottom">Balance</td>
                                            <td class="w-td border-top border-bottom"></td>
                                            <td class="w-td border-top border-bottom">{{$data[0]['balance']}}</td>
                                            <td class="w-td border-bottom"></td>
                                        </tr>
                                        <tr>
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-left border-top border-bottom">IMSI</td>
                                            <td class="w-td border-top border-bottom"></td>
                                            <td class="w-td border-top pt-2 border-bottom pr-3">{{$data[0]['imsi']}}</td>
                                            <td class="w-td border-bottom"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                                <!-- /.mail-box-messages -->
                            </div>

                            <div class="card-body pb-3 pl-0 pr-0 ">
                                <div class="mailbox-controls">
                                    <div class="btn-group ml-1 pt-2">
                                        <b> SIM 2</b>
                                    </div>
                                </div>
                                <!-- /.float-right -->
                                <div class="table-responsive mailbox-messages">
                                    <table class="" border="0" style="width: 100%">
                                        <tbody>
                                        <tr class="border-top">
                                            <td class="w-td"></td>
                                            <td class="w-td pl-2 border-left border-bottom"> <b>{{$data[1]['device_name']}}</b></td>
                                            <td class="w-td"></td>
                                            <td class="w-td  border-top border-bottom"><a href="/simStatistic?{{$data[1]['sku']}}"><img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>
                                            <td class="w-td"></td>
                                            <td class="w-td border-top border-bottom">
                                                <ul class="navbar-nav ml-auto mr-5">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img class="faa-shake animated" src="{{url('/design/img/')}}/oo.png" width="25"></a>
                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                            <a class="dropdown-item" href="/home"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#modal-lg1"><i class="fa fa-plus faa-pulse animated text-primary fa-fw mr-2"></i>Add Data</a>
                                                            <div class="dropdown-divider"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-top border-left" >Data Usage</td>
                                            <td class="w-td border-top border-bottom"></td>
                                            <td class="w-td border-top pt-2 pr-3">{{$data[1]['data_usage']}}</td>
                                            <td class="w-td border-bottom"></td>
                                        </tr>
                                        <tr>
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-left border-top border-bottom">Balance</td>
                                            <td class="w-td border-top border-bottom"></td>
                                            <td class="w-td border-top border-bottom">{{$data[1]['balance']}}</td>
                                            <td class="w-td border-bottom"></td>
                                        </tr>
                                        <tr>
                                            <td class="w-td"></td>
                                            <td class="w-td"></td>
                                            <td class="w-td p-2 border-left border-top border-bottom">IMSI</td>
                                            <td class="w-td border-top border-bottom"></td>
                                            <td class="w-td border-top pt-2 border-bottom pr-3">{{$data[1]['imsi']}}</td>
                                            <td class="w-td border-bottom"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                                <!-- /.mail-box-messages -->
                            </div>

                            <!-- .modal -->
                            <div class="modal fade" id="modal-lg">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add Balance To The SIM </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('addClientBalance')}}">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Enter The Balance To Be Added:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text text-primary">MB</span>
                                                        </div>
                                                        <input hidden value="{{$data[0]['imsi']}}" name="imsi" >
                                                        <input type="number" name="client_balance"  min="0" placeholder="The Required Balance" class="form-control @error('client_balance') is-invalid @enderror" value="{{ old('client_balance') }}" required autocomplete="client_balance" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary"> <i class="fa fa-check mr-1"></i>Add Balance</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <!-- .modal -->
                            <div class="modal fade" id="modal-lg1">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add Balance To The SIM </h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('addClientBalance')}}">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Enter The Balance To Be Added:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text text-primary">MB</span>
                                                        </div>
                                                        <input hidden value="{{$data[1]['imsi']}}" name="imsi" >
                                                        <input type="number" name="client_balance"  min="0" placeholder="The Required Balance" class="form-control @error('client_balance') is-invalid @enderror" value="{{ old('client_balance') }}" required autocomplete="client_balance" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary"> <i class="fa fa-check mr-1"></i>Add Balance</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
{{--                            <div class="card-body pt-5 pl-0 pr-0 ">--}}
{{--                                <div class="mailbox-controls">--}}

{{--                                    <div class="btn-group ml-1 pt-2">--}}
{{--                                        <b> SIM 2</b>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                                <!-- /.float-right -->--}}
{{--                                <div class="table-responsive mailbox-messages">--}}
{{--                                    <table class="" border="0" style="width: 100%">--}}
{{--                                        <tbody>--}}
{{--                                        <tr class="border-top">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td pl-2 border-left border-bottom"> <b>{{$data[1]['device_name']}}</b></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td  border-top border-bottom"><a href=""/simStatistic?{{$data[0]['sku']}}"><img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td border-top border-bottom">--}}
{{--                                                <ul class="navbar-nav ml-auto mr-5">--}}
{{--                                                    <li class="nav-item dropdown">--}}
{{--                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img class="faa-shake animated" src="{{url('/design/img/')}}/oo.png" width="25"></a>--}}
{{--                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
{{--                                                            <a class="dropdown-item" href="/sim"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>--}}
{{--                                                            <div class="dropdown-divider"></div>--}}
{{--                                                            <a class="dropdown-item" href="/sim"><i class="fa fa-plus faa-pulse animated text-primary fa-fw mr-2"></i>Add Data</a>--}}
{{--                                                            <div class="dropdown-divider"></div>--}}
{{--                                                        </div>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{--                                            <input hidden value="1" id="sku{{$data[1]['sku']}}" >--}}
{{--                                            --}}{{--                                            <input hidden value="humidity" id="type{{$i}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-top border-left" >Data Usage</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top pt-2 pr-3">{{$data[1]['data_usage']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{--<input hidden value="1" id="sku{{$data[1]['sku']}}" >--}}
{{--                                            --}}{{-- <input hidden value="temperature" id="type{{$i+10}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-left border-top border-bottom">Balance</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top border-bottom">{{$data[1]['balance']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{-- <input hidden value="1" id="sku{{$data[1]['sku']}}" >--}}
{{--                                            --}}{{--  <input hidden value="temperature" id="type{{$i+10}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-left border-top border-bottom">IMSI</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top pt-2 border-bottom pr-3">{{$data[1]['imsi']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                    <!-- /.table -->--}}
{{--                                </div>--}}
{{--                                <!-- /.mail-box-messages -->--}}
{{--                            </div>--}}
{{--                            <div class="card-body pt-5 pl-0 pr-0 ">--}}
{{--                                <div class="mailbox-controls">--}}

{{--                                    <div class="btn-group ml-1 pt-2">--}}
{{--                                        <b> SIM 1</b>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                                <!-- /.float-right -->--}}
{{--                                <div class="table-responsive mailbox-messages">--}}
{{--                                    <table class="" border="0" style="width: 100%">--}}
{{--                                        <tbody>--}}
{{--                                        <tr class="border-top">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td pl-2 border-left border-bottom"> <b>{{$data[2]['device_name']}}</b></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td  border-top border-bottom"><a href="/e"><img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td border-top border-bottom">--}}
{{--                                                <ul class="navbar-nav ml-auto mr-5">--}}
{{--                                                    <li class="nav-item dropdown">--}}
{{--                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img class="faa-shake animated" src="{{url('/design/img/')}}/oo.png" width="25"></a>--}}
{{--                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
{{--                                                            <a class="dropdown-item" href="/sim"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>--}}
{{--                                                            <div class="dropdown-divider"></div>--}}
{{--                                                            <a class="dropdown-item" href="/sim"><i class="fa fa-plus faa-pulse animated text-primary fa-fw mr-2"></i>Add Data</a>--}}
{{--                                                            <div class="dropdown-divider"></div>--}}
{{--                                                        </div>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{--                                            <input hidden value="1" id="sku{{$data[2]['sku']}}" >--}}
{{--                                            --}}{{--                                            <input hidden value="humidity" id="type{{$i}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-top border-left" >Data Usage</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top pt-2 pr-3">{{$data[2]['data_usage']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{--<input hidden value="1" id="sku{{$data[2]['sku']}}" >--}}
{{--                                            --}}{{-- <input hidden value="temperature" id="type{{$i+10}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-left border-top border-bottom">Balance</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top border-bottom">{{$data[2]['balance']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{-- <input hidden value="1" id="sku{{$data[2]['sku']}}" >--}}
{{--                                            --}}{{--  <input hidden value="temperature" id="type{{$i+10}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-left border-top border-bottom">IMSI</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top pt-2 border-bottom pr-3">{{$data[2]['imsi']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                    <!-- /.table -->--}}
{{--                                </div>--}}
{{--                                <!-- /.mail-box-messages -->--}}
{{--                            </div>--}}
{{--                            <div class="card-body pt-5 pl-0 pr-0 ">--}}
{{--                                <div class="mailbox-controls">--}}

{{--                                    <div class="btn-group ml-1 pt-2">--}}
{{--                                        <b> SIM 1</b>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                                <!-- /.float-right -->--}}
{{--                                <div class="table-responsive mailbox-messages">--}}
{{--                                    <table class="" border="0" style="width: 100%">--}}
{{--                                        <tbody>--}}
{{--                                        <tr class="border-top">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td pl-2 border-left border-bottom"> <b>{{$data[3]['device_name']}}</b></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td  border-top border-bottom"><a href="/e"><img src="{{url('/design/img/')}}/123.png" width="35"></a> </td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td border-top border-bottom">--}}
{{--                                                <ul class="navbar-nav ml-auto mr-5">--}}
{{--                                                    <li class="nav-item dropdown">--}}
{{--                                                        <a class="nav-link" data-toggle="dropdown" href="#"><img class="faa-shake animated" src="{{url('/design/img/')}}/oo.png" width="25"></a>--}}
{{--                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">--}}
{{--                                                            <a class="dropdown-item" href="/sim"><i class="fa fa-spinner fa-spin text-primary fa-fw mr-2"></i>Refresh Status</a>--}}
{{--                                                            <div class="dropdown-divider"></div>--}}
{{--                                                            <a class="dropdown-item" href="/sim"><i class="fa fa-plus faa-pulse animated text-primary fa-fw mr-2"></i>Add Data</a>--}}
{{--                                                            <div class="dropdown-divider"></div>--}}
{{--                                                        </div>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{--                                            <input hidden value="1" id="sku{{$data[3]['sku']}}" >--}}
{{--                                            --}}{{--                                            <input hidden value="humidity" id="type{{$i}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-top border-left" >Data Usage</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top pt-2 pr-3">{{$data[3]['data_usage']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{--<input hidden value="1" id="sku{{$data[3]['sku']}}" >--}}
{{--                                            --}}{{-- <input hidden value="temperature" id="type{{$i+10}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-left border-top border-bottom">Balance</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top border-bottom">{{$data[3]['balance']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            --}}{{-- <input hidden value="1" id="sku{{$data[3]['sku']}}" >--}}
{{--                                            --}}{{--  <input hidden value="temperature" id="type{{$i+10}}">--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td"></td>--}}
{{--                                            <td class="w-td p-2 border-left border-top border-bottom">IMSI</td>--}}
{{--                                            <td class="w-td border-top border-bottom"></td>--}}
{{--                                            <td class="w-td border-top pt-2 border-bottom pr-3">{{$data[3]['imsi']}}</td>--}}
{{--                                            <td class="w-td border-bottom"></td>--}}
{{--                                        </tr>--}}
{{--                                        </tbody>--}}
{{--                                    </table>--}}
{{--                                    <!-- /.table -->--}}
{{--                                </div>--}}
{{--                                <!-- /.mail-box-messages -->--}}
{{--                            </div>--}}
                            @break
                        @endfor
                    <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>



@endsection


@section('contentRangeSlider')
    <!-- Ion Slider -->
    <script src="{{url('/design')}}/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap slider -->
    <script>
        $(function () {
            var data = {!! json_encode($data) !!};
            var currentBalance = {!! json_encode($currentBalance) !!};
            console.log(data.client_balance);
            console.log(currentBalance);
            $('#range_1').ionRangeSlider({
                min     : 0,
                max     : currentBalance,
                from    : 0,
                to      : 155,
                type    : 'double',
                prefix  : 'MB',
                prettify: false,
                hasGrid : true,
                block : true
            })
        })
        $('#range_2').ionRangeSlider()
    </script>
@endsection
