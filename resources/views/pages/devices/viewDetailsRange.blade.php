@extends('includes.footerDataTable')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> View Details Rang Devices </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Show Range SIM For Client <b>{{$client['client_name']}}</b></h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                    @isset($getUserSkuRange)
                        <!-- /.card-header -->
                        <div class="card-body" style="overflow-x: auto">
                            <table id="example1" class="table table-bordered table-striped text-center" >
                                <thead>
                                <tr>
                                    <th>Range SIM</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody >
                                @php($i=0)
{{--                                @foreach($getUserSkuRange['sim'] as $data)--}}
                                    @for (;$i<count($getUserSkuRange['sim']);$i++)
                                        <tr>
                                            <td>
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar bg-primary" role="progressbar" aria-volumenow="{{$getUserSkuRange['sim'][$i]['from']}}" aria-volumemin="0" aria-volumemax="100" style="width: {{$getUserSkuRange['sim'][$i]['from']}}%">
                                                    </div>
                                                </div>
                                                <small>
                                                    <b>SKU</b>(#0{{$getUserSkuRange['sim'][$i]['from']}} -> #0{{$getUserSkuRange['sim'][$i]['to']}})
                                                </small>
                                            </td>

                                            <input hidden value="{{$getUserSkuRange['sim'][$i]['from']}}" name="skuFrom" id="skuFrom{{$i}}" >
                                            <input hidden value="{{$getUserSkuRange['sim'][$i]['to']}}" name="skuTo" id="skuTo{{$i}}" >
                                            <input hidden value="{{$client['user_id']}}" name="user_id" id="user_id" >

                                            <td>
                                                <a onclick="deleteRange({{$i}})" class="btn btn-primary btn-sm" href="#">
                                                    <i class="fas fa-trash"></i>Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endfor
{{--                                @endforeach--}}
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Range SIM</th>
                                    <th>Delete</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    @endisset
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Show Range Sensor For Client <b>{{$client['client_name']}}</b> </h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        @isset($getUserSkuRange)
                            <!-- /.card-header -->
                            <div class="card-body" style="overflow-y: auto">
                                <table id="example2" class="table table-bordered table-striped text-center" >
                                    <thead>
                                    <tr>
                                        <th>Range Sensor</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody >
{{--                                    @foreach($getUserSkuRange['sensor'] as $data)--}}
                                        @for ($j=0;$j<count($getUserSkuRange['sensor']);$j++)
                                            <input hidden value="{{$getUserSkuRange['sensor'][$j]['from']}}" name="skuFrom" id="skuFrom{{$i+$j}}" >
                                            <input hidden value="{{$getUserSkuRange['sensor'][$j]['to']}}" name="skuTo" id="skuTo{{$i+$j}}" >
                                            <input hidden value="{{$client['user_id']}}" name="userId" id="userId" >
                                            <tr>
                                                <td>
                                                    <div class="progress progress-sm">
                                                        <div class="progress-bar bg-primary" role="progressbar" aria-volumenow="{{$getUserSkuRange['sensor'][$j]['from']}}" aria-volumemin="0" aria-volumemax="100" style="width: {{$getUserSkuRange['sensor'][$j]['from']}}%">
                                                        </div>
                                                    </div>
                                                    <small>
                                                        <b>SKU</b>(#0{{$getUserSkuRange['sensor'][$j]['from']}} -> #0{{$getUserSkuRange['sensor'][$j]['to']}})
                                                    </small>
                                                </td>
                                                <td>
                                                    <a onclick="deleteRange({{$i+$j}})" class="btn btn-primary btn-sm" href="#">
                                                        <i class="fas fa-trash">
                                                        </i>
                                                        Delete
                                                    </a>
                                                </td>
                                            </tr>
                                        @endfor
{{--                                    @endforeach--}}
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Range Sensor</th>
                                        <th>Delete</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        @endisset
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script>
        function deleteRange($i) {
            var userId=$('#userId').val();
            var skuTo=$('#skuTo' + $i).val();
            var skuFrom=$('#skuFrom'+ $i).val();
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            console.log(userId);
            console.log(skuTo);
            console.log(skuFrom);
            jQuery.ajax({
                url: '/deleteRangeDevice',
                type: "POST",
                data: {_token: CSRF_TOKEN ,userId:userId,skuTo:skuTo,skuFrom:skuFrom},
                success: function (result) {
                    console.log('here .....' + result);
                    if (result != 0) {
                        swal({
                            position: "center",
                            type: "success",
                            title: "Yaaay, Deleted Successfully ",
                            showConfirmButton: false,
                            timer: 1500
                        });
                        window.location = "/showRangDevice";
                    } else if (result == 0) {
                        swal({
                            position: "center",
                            type: "error",
                            title: "Server side Error, please try again later ",
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });

        }

    </script>
@endsection
