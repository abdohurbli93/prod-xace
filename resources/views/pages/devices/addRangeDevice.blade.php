@extends('index')
@section('content')
    <style>
        .irs--flat .irs-bar {background-color: #4CAF50;}
        .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color: #4CAF50;}
        .irs--flat .irs-handle>i:first-child {background-color: #4CAF50;}
        .irs--flat .irs-from:before, .irs--flat .irs-to:before, .irs--flat .irs-single:before {border-top-color: #4CAF50;}
    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Assign Hardware Range</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title"> View SIM Available Domains</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool mt-1" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" style="overflow-y: auto; height: 180px;">
                                @isset($dataRang['sim'])
                                    @foreach($dataRang['sim'] as $dat)
                                        <div class="row margin">
                                            <div class="col-md-12">
                                                <input id="{{$dat['range_number']}}" type="text" name="range_1" value="" disabled>
                                            </div>
                                        </div>
                                    @endforeach
                                @endisset
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-md-6">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title"> View Sensor Available Domains</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool mt-1" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" style="overflow-y: auto; height: 180px;">
                                @isset($dataRang['sensor'])
                                    @foreach($dataRang['sensor'] as $dat)
                                        <div class="row margin">
                                            <div class="col-md-12">
                                                <input id="{{$dat['range_number']}}" type="text" name="range_1" value="" disabled>
                                            </div>
                                        </div>
                                    @endforeach
                                @endisset
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- /.col -->
                </div>
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Add New Rage:</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool mt-1" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">

                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('addRangeDeviceModel')}}">
                                    @csrf
                                    <div class="col-md-12 row">

                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Client</label>
                                                <select class="form-control select2 select2-primary" required name="user_id" data-dropdown-css-class="select2-primary" data-placement="Choose Client.." style="width: 100%;">
                                                    <option value="" disabled selected>Choose Client</option>
                                                    @foreach($clients as $client)
                                                        <option value="{{$client->user_id}}">{{$client->client_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                        @isset($dataRang['max'])
                                            <div class="col-md-3 ">
                                            <div class="form-group text-center">
                                                <label>From Sku </label>
                                                <input type="number" min="0" max="{{$dataRang['max']}}"  step="1" name="from_sku" required class="form-control text-primary text-center font-weight-bold" placeholder="Add First SKU...">
                                            </div>
                                        </div>
                                            <div class="col-md-4 ">
                                            <div class="form-group text-center">
                                                <label>To Sku</label>
                                                <input type="number" min="0" max="{{$dataRang['max']}}" step="1" name="to_sku" required class="form-control text-primary text-center font-weight-bold" placeholder="Add End SKU..." >
                                            </div>
                                        </div>
                                            <div class="col-md-3">
                                            <div class="form-group text-center">
                                                <label>End Sku</label>
                                                <input type="text" class="form-control text-primary text-center font-weight-bold" value="{{$dataRang['max']}}" disabled style="cursor: not-allowed;">
                                            </div>
                                        </div>
                                        @endisset
                                    </div>
                                    <div class="form-group">
                                        <label></label>
                                        <div class="input-group">
                                            <button type="submit" class="btn btn-primary toastsDefaultprimary font-weight-bold">
                                                <i class="fa fa-check mr-1"></i> ADD</button>
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                </form>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>
@endsection

@section('contentRangeSlider')
    <!-- Ion Slider -->
    <script src="{{url('/design')}}/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap slider -->
    <script>
        $(function () {
            var data = {!! json_encode($dataRang) !!};
            console.log(data);
            console.log(data.sim);
            for (let i = 0; i <data.sensor.length ; i++) {
                $('#'+data.sensor[i].range_number).ionRangeSlider({
                    min: 0,
                    max: data.max,
                    from: data.sensor[i].from,
                    to: data.sensor[i].to,
                    type: 'double',
                    prefix: '#0',
                    prettify: false,
                    hasGrid: true,
                    block : true
                })
            }
            for (let i = 0; i <data.sim.length ; i++) {
                $('#' +data.sim[i].range_number).ionRangeSlider({
                    min: 0,
                    max: data.max,
                    from: data.sim[i].from,
                    to: data.sim[i].to,
                    type: 'double',
                    // step: 1,
                    prefix: '#00',
                    prettify: false,
                    hasGrid: true,
                    block : true
                })
            }
        })
    </script>
    <style>
        .irs-disabled {
            opacity: 1;
        }
    </style>
@endsection


