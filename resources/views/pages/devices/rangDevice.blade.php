@extends('includes.footerDataTable')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1> Show Hardware Range </h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="col-md-7">
        @include('includes.messages')
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card  card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">View The List Of Client</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="overflow-x: auto">
                        <table id="example1" class="table table-bordered table-striped text-center" >
                            <thead>
                            <tr>
                                <th>Client Name</th>
                                <th>Range SIM</th>
                                <th>Range Sensor</th>
                                <th>View Details</th>
                            </tr>
                            </thead>
                            <tbody >
                            @foreach($showRangDevice as $rangDevice)
                                <tr>
                                    <td>
                                        {{$rangDevice->client_name}}
                                    </td>
                                    <td>
                                        @if(!empty($rangDevice['data']) )
                                            @foreach($rangDevice['data']['sim'] as $data)
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar bg-primary" role="progressbar" aria-volumenow="{{$data['from']}}" aria-volumemin="0" aria-volumemax="100" style="width: {{$data['from']}}%">
                                                    </div>
                                                </div>
                                                <small>
                                                    <b>SKU</b>(#0{{$data['from']}} -> #0{{$data['to']}})
                                                </small>
                                            @endforeach
                                        @else
                                            <div class="progress progress-sm">
                                                <div class="progress-bar bg-primary" role="progressbar" aria-volumenow="0" aria-volumemin="0" aria-volumemax="100" style="width: 0%">
                                                </div>
                                            </div>
                                            <small>
                                                <b>SKU</b>   (#00)
                                            </small>
                                        @endif
                                    </td>
                                    <td>
                                        @if(!empty($rangDevice['data']) )
                                            @foreach($rangDevice['data']['sensor'] as $data)
                                                <div class="progress progress-sm">
                                                    <div class="progress-bar bg-primary" role="progressbar" aria-volumenow="{{$data['from']}}" aria-volumemin="0" aria-volumemax="100" style="width: {{$data['from']}}%"></div>
                                                </div>
                                                <small>
                                                    <b>SKU</b>(#0{{$data['from']}} -> #0{{$data['to']}})
                                                </small>
                                            @endforeach
                                        @elseif(empty($rangDevice['data']) )
                                            <div class="progress progress-sm">
                                                <div class="progress-bar bg-primary" role="progressbar" aria-volumenow="0" aria-volumemin="0" aria-volumemax="100" style="width: 0%">
                                                </div>
                                            </div>
                                            <small>
                                                <b>SKU</b> (#00)
                                            </small>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-dark btn-sm" href="/viewDetailsRange/{{$rangDevice->user_id}}">
                                            <i class="fas fa-folder mr-1"></i>View
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Client Name</th>
                                <th>Range</th>
                                <th>Range Sensor</th>
                                <th>View Details</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection
