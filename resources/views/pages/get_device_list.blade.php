@extends('includes.footerDataTable')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Show The Device </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                {{--  <div class="card  card-primary card-outline">  --}}
                {{--  <div class="card-header">
                    <h3 class="card-title">View Device Summary</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>  --}}
                <!-- /.card-header -->
                {{--  <div class="card-body">

                    <div class="row">

                         <div class="col-md-4 text-center">
                            <input type="text" class="knob" value="{{$countSIM}}" data-width="120" data-height="120" data-fgColor="#3c8dbc">
                            <div class="knob-label">SIM Number= "<b>{{$countSIM}}</b>"</div>
                        </div>

                        <div class="col-md-4 text-center">
                            <input type="text" class="knob" value="{{$countSensor}}" data-width="120" data-height="120" data-fgColor="#dc3545">
                            <div class="knob-label">Sensor Number = "<b>{{$countSensor}}</b>"</div>
                        </div>

                        <div class="col-md-4 text-center">
                            <input type="text" class="knob" value="{{$countTotal}}" data-width="120" data-height="120" data-fgColor="#4caf50">
                            <div class="knob-label">Number Total = "<b>{{$countTotal}}</b>"</div>
                        </div>

                    </div>
                </div>  --}}
                <!-- /.card-body -->
                    {{--  </div>  --}}

                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">View The List Of Devices</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="overflow-x: auto">
                            <table id="example1" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Device Name</th>
                                    <th>Serial Number</th>
                                    <th>hardware Type</th>
                                    <th>Status</th>
                                    {{--  <th>SIM Balance MB</th>  --}}
                                </tr>
                                </thead>
                                <tbody >

                                <tr>
                                    <td>1</td>
                                    <td>Gateway</td>
                                    <td>121601211383</td>
                                    <td>Gateway</td>
                                    <td>Live</td>
                                    {{--  @if(!empty($hardware-> balance))
                                         <td>{{$hardware-> balance}}</td>
                                    @else
                                        <td class="text-danger"><i class="fa fa-battery-empty" style="font-size: 35px"></i></td>
                                    @endif  --}}
                                </tr>

                                <tr>
                                    <td>2</td>
                                    <td>Humidity Sensor</td>
                                    <td>422000031</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>

                                <tr>
                                    <td>3</td>
                                    <td>Temperature Sensor</td>
                                    <td>422000026</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>

                                <tr>
                                    <td>4</td>
                                    <td>Gateway</td>
                                    <td>121601211408</td>
                                    <td>Gateway</td>
                                    <td>Live</td>
                                </tr>

                                <tr>
                                    <td>5</td>
                                    <td>Humidity Sensor</td>
                                    <td>422000028</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>


                                <tr>
                                    <td>6</td>
                                    <td>Temperature Sensor</td>
                                    <td>422000027</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>


                                <tr>
                                    <td>7</td>
                                    <td>Gateway</td>
                                    <td>121601211426</td>
                                    <td>Gateway</td>
                                    <td>Live</td>
                                </tr>

                                <tr>
                                    <td>8</td>
                                    <td>Humidity Sensor</td>
                                    <td>422000029</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>


                                <tr>
                                    <td>9</td>
                                    <td>Temperature Sensor</td>
                                    <td>422000030</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>

                                <tr>
                                    <td>10</td>
                                    <td>Gateway</td>
                                    <td>121601211539</td>
                                    <td>Gateway</td>
                                    <td>Live</td>
                                </tr>


                                <tr>
                                    <td>11</td>
                                    <td>Water Temperature Sensor</td>
                                    <td>422000032</td>
                                    <td>Sensor</td>
                                    <td>Live</td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection
