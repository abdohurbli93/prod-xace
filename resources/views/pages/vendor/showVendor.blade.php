@extends('includes.footerDataTable')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> View Vendors </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">View The List Of Vendor</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="overflow-x: auto">
                            <table id="example1" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>Vendor Name</th>
                                    <th>E-mail</th>
                                    <th>Phone</th>
                                    <th>Base Url</th>
                                    <th>Type</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody >
                                @foreach($vendors as $vendor)
                                    <tr>
                                        <input type="hidden" name="id" value="{{$vendor->vendor_id}}"  >
                                        <td>{{$vendor->vendor_name}}</td>
                                        <td>{{$vendor->email}}</td>
                                        <td>{{$vendor->phone_number}}</td>
                                        <td>{{$vendor->base_url}}</td>
                                        <td>{{$vendor->type}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a type="button" title="Delete" onclick="deleteUser({{$vendor->vendor_id}})" href="#" class="btn btn-primary"><i class="fas fa-trash"></i></a>
                                                <a type="button" title="View" href="/showDetailVendor/{{$vendor->vendor_id}}" class="btn bg-dark"><i class="fa fa-folder"></i></a>
                                                <a type="button" title="Edit" href="/editVendorShow/{{$vendor->vendor_id}}" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Vendor Name</th>
                                    <th>E-mail</th>
                                    <th>Phone</th>
                                    <th>Base Url</th>
                                    <th>Type</th>
                                    <th>#</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="{{ asset('js/include/delete_user.js') }}"></script>
@endsection
