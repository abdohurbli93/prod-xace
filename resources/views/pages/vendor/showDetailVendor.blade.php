@extends('index')
@section('content')
    <style>
        .input-group-text {background-color: #fff;border: 1px solid #ced4da;}
        .form-control {background-color: #fff;border: 1px solid #ced4da;}
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Show Detail Vendor </h1>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <section class="content">
            <div class="col-md-12 row">

                <div class="col-md-6">
                    <!-- Profile Image -->
                    <div class="card card-primary card-outline ">
                        <div class="card-body box-profile">

                            <h2 class="profile-username font-weight-bold text-center">{{$showDetailVendor->vendor_name}}</h2>

                            <p class="text-muted text-center">{{$showDetailVendor->type}} </p>

                            <hr>

                            @if($showDetailVendor->type == 'basic_auth')
                                <strong><i class="fas fa-user-alt mr-1"></i>Username </strong>

                                <p class="text-muted">
                                    {{$vendorType['username']}}
                                </p>

                                <hr>

                                <strong><i class="fas fa-user-lock mr-1"></i> Password</strong>

                                <p class="text-muted">  {{$vendorType['password']}}</p>

                                <hr>
                            @elseif($showDetailVendor->type == 'token')

                                <strong><i class="fas fa-lock-open mr-1"></i>Token</strong>

                                <p class="text-muted">  {{$vendorType['token']}}</p>
                                <hr>
                            @endif

                            <a href="/editVendorShow/{{$showDetailVendor->vendor_id}}" class="btn btn-primary btn-block"><i class="fa fa-edit mr-2"></i><b>Vendor Edit</b></a>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

                <div class="col-md-6">
                    <!-- About Me Box -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">About Vendor</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <strong><i class="fas fa-book mr-1"></i>E-mail </strong>

                            <p class="text-muted">
                                {{$showDetailVendor->email}}
                            </p>

                            <hr>

                            <strong><i class="fas fa-map-marker-alt mr-1"></i> Address</strong>

                            <p class="text-muted">{{$showDetailVendor->address}}</p>

                            <hr>

                            <strong><i class="fas fa-phone-alt mr-1"></i> Phone</strong>

                            <p class="text-muted">
                                <span class="tag tag-primary">{{$showDetailVendor->phone_number}}</span>
                            </p>

                            <hr>

                            <strong><i class="far fa-file-alt mr-1"></i> Base Url</strong>

                            <p class="text-muted"> {{$showDetailVendor->base_url}}</p>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>

    </div>
@endsection
