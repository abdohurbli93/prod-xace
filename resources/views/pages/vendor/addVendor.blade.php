@extends('index')
@section('content')
<style>
    .input-group-text {background-color: #fff;border: 1px solid #ced4da;}
    .form-control {background-color: #fff;border: 1px solid #ced4da;}
</style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> Add Vendor </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Enter Information:</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('addVendorModel')}}">
                                    @csrf
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Vendor Name:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" placeholder="Vendor Name" id="vendor_name" name="vendor_name" class="form-control @error('vendor_name') is-invalid @enderror" value="{{ old('vendor_name') }}" required autocomplete="vendor_name" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Email:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-envelope text-primary"></i></span>
                                                    </div>
                                                    <input type="email" placeholder="Email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Phone:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-phone text-primary"></i></span>
                                                    </div>
                                                    <input type="tel" placeholder="phone" name="phone_number"  class="form-control @error('phone_number') is-invalid @enderror" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Address:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-address-card text-primary"></i></span>
                                                    </div>
                                                    <textarea rows="2" id="address" placeholder="Enter..." name="address"  class="form-control @error('address') is-invalid @enderror" value="{{ old('address') }}" required autocomplete="address" autofocus></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Base Url:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text text-primary"><i class="fa fa-link text-primary"></i></span>
                                                        </div>
                                                        <input type="url" name="base_url"  placeholder="Base Url" class="form-control @error('base_url') is-invalid @enderror"value="{{ old('base_url') }}" required autocomplete="base_url" autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Type:</label>
                                                    <select  class="form-control @error('header_json') is-invalid @enderror" value="{{ old('header_json') }}" required autocomplete="header_json" autofocus id="selector" name="header_json">
                                                        <option value="" selected disabled>Choose..</option>
                                                        <option value="authShow">basic auth</option>
                                                        <option value="tokenShow">token</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="chooseDiv col-12" id="tokenShow" style="display: none">
                                                <div class="form-group">
                                                    <label>Token:</label>
                                                    <div class="input-group">
                                                        <textarea rows="5" placeholder="Enter..." name="token" class="form-control @error('token') is-invalid @enderror" value="{{ old('token') }}"  autocomplete="token" autofocus></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="chooseDiv" id="authShow" style="display: none">
                                                    <div class="form-group">
                                                        <label>Username:</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                            </div>
                                                            <input type="text" placeholder="Username" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}"  autocomplete="token" autofocus>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Password:</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i id="pass-status" class="fa fa-fw fa-eye-slash field-icon text-primary eyePassword" onclick="showPassword()"></i></span>
                                                            </div>
                                                            <input  type="password" id="password" name="password" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>

                                        <div class="form-group">
                                            <label><br></label>
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary text-white"><i class="fa fa-check"></i>&nbsp;ADD</button>
                                            </div>
                                        </div>

                                    </div>

                                </form>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(function() {
        $('#selector').change(function(){
            $('.chooseDiv').hide();
            $('#' + $(this).val()).show();
        });
    });
    function showPassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
            passStatus.className='fa fa-eye-slash eyePassword text-primary';
        } else {
            x.type = "password";
            passStatus.className='fa fa-eye-slash eyePassword text-primary';

        }
    }
</script>
@endsection
