@extends('index')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Vendor</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    @include('layouts._message')
                    <div class="card-header">
                        <h3 class="card-title">Enter New Information:</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('updateVendorModel')}}">
                                    @csrf
                                    <input value="{{$editVendorShow->vendor_id}}" name="vendor_id" hidden>
                                    <input value="{{$editVendorShow->id}}" name="vendorCredential_id" hidden>
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Vendor Name:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" value="{{$editVendorShow->vendor_name}}" id="vendor_name" class="form-control" name="vendor_name" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Email:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-envelope text-primary"></i></span>
                                                    </div>
                                                    <input type="email" value="{{$editVendorShow->email}}" name="email" class="form-control" autocomplete required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Phone:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-phone text-primary"></i></span>
                                                    </div>
                                                    <input value="{{$editVendorShow->phone_number}}" name="phone_number" class="form-control" data-mask="" im-insert="true" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Address:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-address-card text-primary"></i></span>
                                                    </div>
                                                    <textarea rows="2" id="address" class="form-control" name="address" required>{{$editVendorShow->address}}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Base Url:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text text-primary"><i class="fa fa-link text-primary"></i></span>
                                                        </div>
                                                        <input type="url" value="{{$editVendorShow->base_url}}" name="base_url" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Type:</label>
                                                    <select class="form-control" id="selector" name="header_json">
                                                        <option value="authShow" {{$editVendorShow->type=='basic_auth'?'selected':''}}>basic auth</option>
                                                        <option value="tokenShow" {{$editVendorShow->type=='token'?'selected':''}} >token</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="chooseDiv col-md-12" id="tokenShow"  style=" {{$editVendorShow->type=='token'?'':'display:none'}} ">
                                                <div class="form-group">
                                                    <label>Token:</label>
                                                    <div class="input-group">
                                                        <textarea rows="6" class="form-control" name="token">{{$vendorType['token']}}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="chooseDiv" id="authShow" style=" {{$editVendorShow->type=='basic_auth'?'':'display:none'}} ">
                                                <div class="form-group">
                                                    <label>Username:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                        </div>
                                                        <input type="text" value="{{$vendorType['username']}}" class="form-control" name="username">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label>Password:</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i id="pass-status" class="fa fa-fw fa-eye-slash field-icon text-primary eyePassword" onclick="showPassword()"></i></span>
                                                        </div>
                                                        <input  type="password" value="{{$vendorType['password']}}" id="password" class="form-control" name="password" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label></label>
                                            <div class="input-group">
                                                <button type="submit"  class="btn btn-primary font-weight-bold">
                                                    <i class="fa fa-check"></i>&nbsp;Save</button>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script>

        $(function() {
            $('#selector').change(function(){
                $('.chooseDiv').hide();
                $('#' + $(this).val()).show();
            });
        });

        function showPassword() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
                passStatus.className='fa fa-eye eyePassword text-primary';
            } else {
                x.type = "password";
                passStatus.className='fa fa-eye-slash eyePassword text-primary';
            }
        }

    </script>
    <!-- End demo content -->


@endsection
