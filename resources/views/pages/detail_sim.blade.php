@extends('index')
@section('content')
    <style>
        .irs--flat .irs-bar {background-color: #4CAF50;}
        .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color: #4CAF50;}
        .irs--flat .irs-handle>i:first-child {background-color: #4CAF50;}
        .irs--flat .irs-from:before, .irs--flat .irs-to:before, .irs--flat .irs-single:before {border-top-color: #4CAF50;}
        .irs-disabled {opacity: 0.9;}

    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Balance To The SIM</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Add Credit To The SIM</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool mt-1" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">

                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('addClientBalance')}}">
                                    @csrf
                                    <div class="col-12 row">
                                        <div class="form-group col-md-auto">
                                            <label>Total Balance:</label>
                                            @isset($currentBalance)
                                                <input value="{{$currentBalance}}MB" class="form-control text-primary text-center font-weight-bold" disabled style="cursor: no-drop;">
                                            @endisset
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Available Balance:</label>
                                                @isset($clientBalance)
                                                    <div class="row margin">
                                                        <div class="col-md-12">
                                                            <input id="range_1" type="text" value="range_1" disabled>
                                                        </div>
                                                    </div>
                                                @endisset
                                            </div>
                                        </div>

                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Enter The Balance To Be Added:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text text-primary">MB</span>
                                                    </div>
                                                    <input type="number" name="client_balance" max="{{$clientBalance['client_balance']}}"  min="0" placeholder="The Required Balance" class="form-control @error('client_balance') is-invalid @enderror" value="{{ old('client_balance') }}" required autocomplete="client_balance" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Choose SIM SKU </label>
                                                <select class="form-control select2 select2-primary" required name="hardware_id" data-dropdown-css-class="select2-primary" data-placement="Choose Client.." style="width: 100%;">
                                                    <option value="" disabled selected>Choose SIM SKU..</option>
                                                        @foreach($sims as $sim)
                                                            <option value="{{$sim->id}}">{{$sim->sku}}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <br>
                                    <div class="form-group col-12">
                                        <div class="input-group">
                                            <button type="submit" class="btn btn-primary toastsDefaultprimary font-weight-bold">
                                                <i class="fa fa-check mr-1"></i> ADD</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>
@endsection

@section('contentRangeSlider')
    <!-- Ion Slider -->
    <script src="{{url('/design')}}/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap slider -->
    <script>
        $(function () {
            var data = {!! json_encode($clientBalance) !!};
            var currentBalance = {!! json_encode($currentBalance) !!};
            console.log(data.client_balance);
            console.log(currentBalance);
            $('#range_1').ionRangeSlider({
                min     : 0,
                max     : data.client_balance,
                from    : 0,
                to      : currentBalance,
                type    : 'double',
                prefix  : 'MB',
                prettify: false,
                hasGrid : true,
                block : true
            })
        })
    </script>
@endsection


