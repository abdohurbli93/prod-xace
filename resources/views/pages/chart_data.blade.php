@extends('index')
@section('content')
    <link rel="stylesheet" href="{{url('/design')}}/dist/css/style.css">

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Data Results</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <input id="data" hidden name="data" value="{{$data}}">
        <input id="sku" hidden name="sku" value="{{$sku}}">
        <input id="prefix" hidden name="prefix" value="{{$prefix}}">
        <input id="type" hidden name="prefix" value="{{$type}}">
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                            <div class="card-body">
                                        <div class="card card-primary card-outline">
                                            <div class="card-header">
                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                                                </div>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <!-- Date and time range -->
                                                        <div class="form-group">
                                                            <label>Date and time range:</label>

                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                                </div>
                                                                <input type="text" name="date" value="{{$date}}" class="form-control float-right" id="reservationtime">
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                    <!-- /.col -->
                                                </div>
                                                <div id="demoButtonDatetimeStart"></div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="button" class="btn btn-primary" onclick="refresh()">
                                                    <i class="fa fa-spinner fa-spin mr-2" ></i>Refresh
                                                </button>
                                            </div>
                                        </div>





                                        @if(!empty($data))
                                            <div class="card card-primary card-outline box blue" id="temperature">
                                                <div class="card-header">
                                                    <h3 class="card-title">Data Chart</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body" >
                                                    <div class="row">
                                                        <div class="col-md-12" >
                                                            @include('pages.charts.Temperature')
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                        @endif

                                    </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
        </section>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="{{asset('js/chart js/singleChart.js')}}"></script>

