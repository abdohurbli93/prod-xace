@extends('index')
@section('content')
    <link rel="stylesheet" href="{{url('/design')}}/dist/css/style.css">

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Dashboard Results</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card card-tabs">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Sensor</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-three-sim-tab" data-toggle="pill" href="#custom-tabs-three-sim" role="tab" aria-controls="custom-tabs-three-sim" aria-selected="false">SIM</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">

                                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                        <div class="card card-primary card-outline">
                                            <div class="card-header">
                                                <div class="btn-group">
                                                    <button onclick="myFunctionHidden()"  href="#" class="btn btn-primary"> All </button>
                                                    <button onclick="myFunctionShow()"  href="#" class="btn btn-dark"> Custom </button>
                                                </div>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                                                </div>
                                            </div>
                                            <input id="data" hidden name="data" value="{{$data}}">
                                            <input id="data1" hidden name="data" value="{{$data1}}">
                                            <input id="data2" hidden name="data" value="{{$data2}}">
                                            <input id="data3" hidden name="data" value="{{$data3}}">
                                            <input id="data4" hidden name="data" value="{{$data4}}">
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <!-- Date and time range -->
                                                        <div class="form-group">
                                                            <label>Date and time range:</label>

                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                                </div>
                                                                <input type="text" name="date" value="{{$date}}" class="form-control float-right" id="reservationtime">
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Choose:</label>
                                                            <select class="select2" id="selector" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">
                                                                <option selected value="Battery">Battery</option>
                                                                <option selected value="Humadidy">Humidity</option>
                                                                <option selected value="Temperature">Temperature</option>
                                                          </select>

                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <div id="demoButtonDatetimeStart"></div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="button" class="btn btn-primary" onclick="refresh()">
                                                    <i class="fa fa-spinner fa-spin mr-2" ></i>Refresh
                                                </button>
                                            </div>
                                        </div>

                                        @if(!empty($data1))
                                            <div class="card card-primary card-outline box red" id="battery">
                                                <div class="card-header">
                                                    <h3 class="card-title">Battery</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body" >
                                                    <div class="row">
                                                        <div class="col-md-12" >
                                                            @include('pages.charts.Battery')
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                        @endif

                                        @if(!empty($data2))
                                            <div class="card card-primary card-outline box green" id="humadidy">
                                                <div class="card-header">
                                                    <h3 class="card-title">Humidity</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body" >
                                                    <div class="row">
                                                        <div class="col-md-12" >
                                                            @include('pages.charts.Humidity')
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                        @endif

                                        @if(!empty($data))
                                            <div class="card card-primary card-outline box blue" id="temperature">
                                                <div class="card-header">
                                                    <h3 class="card-title">Temperature</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body" >
                                                    <div class="row">
                                                        <div class="col-md-12" >
                                                            @include('pages.charts.Temperature')
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                        @endif

                                    </div>

                                    <div class="tab-pane fade" id="custom-tabs-three-sim" role="tabpanel" aria-labelledby="custom-tabs-three-sim-tab">

                                        <div class="card card-primary card-outline">
                                            <div class="card-header">
                                                <div class="btn-group">
                                                    <button onclick="myFunctionHiddenSim()"  href="#" class="btn btn-primary"> All </button>
                                                    <button onclick="myFunctionShowSim()"  href="#" class="btn btn-dark"> Custom </button>
                                                </div>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                                                </div>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Date and time range:</label>

                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                                </div>
                                                                <input type="text" name="date" value="{{$date}}" class="form-control float-right" id="reservationSimTime2">
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Choose Chart Show:</label>
                                                            <select class="select2" id="selectorSim" multiple="multiple" data-placeholder="Select" style="width: 100%;">
                                                                <option selected value="multisim">SIM Details </option>
                                                                <option selected value="Sim">All SIM</option>
                                                                <option selected value="multisimconsumption">Consumption Details</option>
                                                                <option selected value="consumption">All Consumption</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="demoButtonDatetimeStartSim"></div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="button" class="btn btn-primary" onclick="refreshAllSim()">
                                                    <i class="fa fa-spinner fa-spin mr-2" ></i>Refresh2
                                                </button>
                                            </div>
                                        </div>

                                        <div class="card card-primary card-outline">
                                            <div class="card-header">
                                                <h3 class="card-title">Multi SIM Details</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
                                                </div>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <div class="row">
                                                    @isset($dataRang['max'])
                                                        <div class="col-md-6 scroll">
                                                            @isset($dataRang['sim'])
                                                                @foreach($dataRang['sim'] as $dat)
                                                                    <div class="row margin">
                                                                        <div class="col-md-12">
                                                                            <input id="{{$dat['range_number']}}" type="text" name="range_1" value="" disabled>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @endisset
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Inter SKU:</label>
                                                                <input type="text" min="0" max="{{$dataRang['max']}}" step="1" name="to_sku" required id="input_sku" class="form-control text-primary text-center font-weight-bold" placeholder="SkuNumber1,.....,SkuNumber5" >
                                                            </div>
                                                        </div>

                                                    @endisset
                                               </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="button" class="btn btn-primary" onclick="refreshSim()">
                                                    <i class="fa fa-spinner fa-spin mr-2" ></i>Show
                                                </button>
                                            </div>
                                        </div>

                                        <div class="card card-primary card-outline box blue" id="multisim">
                                            <div class="card-header">
                                                <h3 class="card-title"> SIM Details</h3>
                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                </div>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body" >
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        @include('pages.charts.multiSim')
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.card-body -->
                                        </div>

                                        @if(!empty($data3))
                                            <div class="card card-primary card-outline box blue" id="sim">
                                                <div class="card-header">
                                                    <h3 class="card-title">All SIM</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body" >
                                                    <div class="row">
                                                        <div class="col-md-12" >
                                                            @include('pages.charts.Sim')
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                        @endif

                                        <div class="card card-primary card-outline box blue" id="multisimconsumption">
                                            <div class="card-header">
                                                <h3 class="card-title">SIM Consumption Details </h3>
                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                </div>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body" >
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        @include('pages.charts.multiSimConsumption')
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.card-body -->
                                        </div>

                                    @if(!empty($data4))
                                            <div class="card card-primary card-outline box blue" id="consumption">
                                                <div class="card-header">
                                                    <h3 class="card-title">All Sim Consumption</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body" >
                                                    <div class="row">
                                                        <div class="col-md-12" >
                                                            @include('pages.charts.SimConsumption')
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>


            </div>
        </section>



        <!-- /.content -->
    </div>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script>
        function myFunctionShow() {
            document.getElementById("demoButtonDatetimeStart").innerHTML = ' <div class="row">\n' + '<div class="col-md-6">\n' + ' <div class="form-group">\n' + '<label for="inputEstimatedDuration">From SKU</label>\n' + '<input type="number" id="skuFrom" name="from_sku" class="form-control" value="001" min="0" step="001">\n' + '</div>\n' + ' </div>\n' + '<div class="col-md-6">\n' + '<div class="form-group">\n' + '<label for="inputEstimatedDuration">To SKU</label>\n' + '<input type="number" id="skuTo" name="to_sku" class="form-control" value="001" min="0" step="001">\n' + '</div>\n' + '</div>\n' + '</div>';
        }
        function myFunctionHidden() {
            document.getElementById("demoButtonDatetimeStart").innerHTML = '';
        }

        function myFunctionShowSim() {
            document.getElementById("demoButtonDatetimeStartSim").innerHTML = ' <div class="row">\n' + '<div class="col-md-6">\n' + ' <div class="form-group">\n' + '<label for="inputEstimatedDuration">From SIM SKU</label>\n' + '<input type="number" id="skuFromSim" name="from_sku_sim" class="form-control" value="001" min="0" step="001">\n' + '</div>\n' + ' </div>\n' + '<div class="col-md-6">\n' + '<div class="form-group">\n' + '<label for="inputEstimatedDuration">To SIM SKU</label>\n' + '<input type="number" id="skuToSim" name="to_sku_sim" class="form-control" value="001" min="0" step="001">\n' + '</div>\n' + '</div>\n' + '</div>';
        }
        function myFunctionHiddenSim() {
            document.getElementById("demoButtonDatetimeStartSim").innerHTML = '';
        }

        $(document).ready(function(){
            $('#selector').select2();
            $('#selector').on('change', function() {
                var values =  $("#selector").select2("val");
                for(var i = 0; i <values.length;i++){
                    showCharts(values[i].toLowerCase());
                }
            });
            $('#selector').on("select2:unselecting", function(e){
                var values =  $("#selector").select2("val");
                for(var i = 0; i <values.length;i++){
                    hideCharts(values[i].toLowerCase());
                }
            });

            function hideCharts(param){
                $('#'+param).hide();
            }
            function showCharts(param){
                $('#'+param).show();
            }

            $('#selectorSim').select2();
            $('#selectorSim').on('change', function() {
                var values =  $("#selectorSim").select2("val");
                for(var i = 0; i <values.length;i++){
                    showCharts(values[i].toLowerCase());
                }
            });
            $('#selectorSim').on("select2:unselecting", function(e){
                var values =  $("#selectorSim").select2("val");
                for(var i = 0; i <values.length;i++){
                    hideCharts(values[i].toLowerCase());
                }
            });

            function hideCharts(param){
                $('#'+param).hide();
            }
            function showCharts(param){
                $('#'+param).show();
            }


        });

        $('#input_sku').keypress (function (e) {
            var chr = String.fromCharCode(e.which);
            if ("0123456789,".indexOf(chr) < 0 )
                return false;
        })
    </script>
    <script src="{{asset('js/chart js/amChart.js')}}"></script>
    <script src="{{asset('js/chart js/simChart.js')}}"></script>
@endsection

@section('contentRangeSlider')
    <!-- Ion Slider -->
    <script src="{{url('/design')}}/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap slider -->
    <script>
        $(function () {
            var data = {!! json_encode($dataRang) !!};
            console.log(data);
            console.log(data.sim);
            for (let i = 0; i <data.sim.length ; i++) {
                $('#' +data.sim[i].range_number).ionRangeSlider({
                    min: 0,
                    max: data.max,
                    from: data.sim[i].from,
                    to: data.sim[i].to,
                    type: 'double',
                    prefix: '#00',
                    prettify: false,
                    hasGrid: true,
                    block : true
                })
            }
        })
    </script>
@endsection
