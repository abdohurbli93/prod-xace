@extends('index')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Update Info</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="col-md-7">
            @include('includes.messages')
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Enter New Information:</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('updateUserModel')}}">
                                    @csrf
                                    <div class="col-12 row">
                                        <div class="col-md-10">

                                            <div class="form-group">
                                                <label>Client Name:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" value="{{$editUserShow->client_name}}" id="client_name"  name="client_name" class="form-control @error('client_name') is-invalid @enderror" value="{{ old('client_name') }}" required autocomplete="client_name" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Username:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" value="{{$editUserShow->username}}" id="username"  name="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" required autocomplete="username" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Address:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-address-card text-primary"></i></span>
                                                    </div>
                                                    <textarea rows="3" id="address"  name="address" class="form-control @error('address') is-invalid @enderror" value="{{ old('address') }}" required autocomplete="address" autofocus>{{$editUserShow->address}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Phone:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-phone text-primary"></i></span>
                                                    </div>
                                                    <input type="numbers" value="{{$editUserShow->phone_number}}" name="phone_number" class="form-control @error('phone_number') is-invalid @enderror" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>
                                                </div>
                                            </div>

                                            <input value="{{$editUserShow->id}}" name="id" hidden>
                                            <input value="{{$editUserShow->client_id}}" name="client_id" hidden>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <button  class="btn btn-primary font-weight-bold" >Save</button>
                                                </div>
                                                <!-- /.input group -->
                                            </div>

                                        </div>
                                    </div>



                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>

    </div>
    <script>
        function img_pathUrl(input){
            $('#img_url')[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
        }
        function showPassword() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
                passStatus.className='fa fa-eye eyePassword text-primary';
            } else {
                x.type = "password";
                passStatus.className='fa fa-eye-slash eyePassword text-primary';

            }
        }

    </script>
    <!-- End demo content -->


@endsection
