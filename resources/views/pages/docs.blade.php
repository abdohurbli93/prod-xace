@extends('include.header')
@section('content')


    <div id="applicationContent">

        <div id="documentation" class="js-scrolling-done">

            <!-- TocColumn -->
            <div id="documentationTocColumn " class="tocColumn customScrollbar customScrollbarLight" id="myScrollspy"  style="width: 250px; margin-top:50px;">
                <div class="tocColumnContainer">

                    <!-- Content -->
                    <div class="tocColumnContent">

                        <div class="row tocColumnTools">

                            <!-- API Blueprint -->
                            <div class="row tocColumnTool tocColumnTool--download">
                                <a href="/docs" class=" tocColumnToolLink"><h3>API
                                  <span class=" tocColumnToolSubtext">Xace tech</span></h3>
                                </a>
                            </div>
                            <!-- /API Blueprint -->


                        </div>

                        <div class="tocColumnSection">

                            <!-- Title -->
                            <a href="#introduction">
                                <div class="tocColumnTitle noSelect">
                                    <h3 class="tocColumnTitleText">Introduction</h3>
                                </div>
                            </a>
                            <!-- /Title -->

                            <!-- Items -->
                            <div class="tocColumnSectionItems">

                                <li class="tocColumnItem hasSubitems">

                                    <!-- Container -->
                                    <div class="tocColumnItemContainer" >
                                        <a class="tocColumnItemText" href="#sectionAuthentication">
                                            Authentication        </a>
                                    </div>
                                    <!-- /Container -->

                                </li>

                                <li class="tocColumnItem noSelect hasSubitems">

                                    <!-- Container -->
                                    <div class="tocColumnItemContainer">
                                        <a class="tocColumnItemText" href="#sectionGeneral">
                                            General
                                        </a>
                                    </div>
                                    <!-- /Container -->

                                </li>

                                <li class="tocColumnItem noSelect hasSubitems">

                                    <!-- Container -->
                                    <div class="tocColumnItemContainer">
                                        <a class="tocColumnItemText" href="#sectionTerms">
                                            Common Terms
                                        </a>
                                    </div>
                                    <!-- /Container -->

                                </li>

                            </div>
                            <!-- /Items -->

                        </div>

                        <div class="tocColumnSection">

                            <!-- Title -->
                            <div class="tocColumnTitle noSelect">
                                <a href="#reference">
                                    <h3 class="tocColumnTitleText" >REFERENCE</h3>
                                </a>
                            </div>
                            <!-- /Title -->

                        @foreach($project->projectParts as $projectPart)
                            <!-- Items -->
                                <div class="tocColumnSectionItems">
                                    <li class="tocColumnItem noSelect hasSubitems">

                                        <!-- Container -->
                                        <div class="tocColumnItemContainer">
                                            <a class="tocColumnItemText" href="#section{{$projectPart['id']}}">
                                                {{$projectPart['name']}}
                                            </a>
                                        </div>
                                        <!-- /Container -->

                                        <!-- Subitems -->
                                        <ul class="tocColumnSubitems">
                                            @foreach($projectPart->partRequest as $partRequest)
                                                <li class="tocColumnSubitem noSelect">
                                                    <!-- Text -->
                                                    <a class="tocColumnSubitemText" href="#section{{$partRequest['id']}}">
                                                        {{$partRequest['name']}} </a>
                                                    <!-- /Text -->

                                                </li>
                                            @endforeach
                                        </ul>
                                        <!-- /subitems -->
                                    </li>

                                </div>
                                <!-- /Items -->
                            @endforeach
                        </div>

                    </div>
                    <!-- /Content -->
                </div>
            </div>
            <!-- /TocColumn -->

            <!-- HumanColumn -->
            <div id="documentationHumanColumn" class="humanColumn customScrollbar customScrollbarLight" tabindex="1" style=" transform: translateX(255px);">
                <div class="humanColumnContents " >
                    <!-- Name -->
                    <h1 class="humanColumnApiName" class="tablinks" onclick="openMachineColumn(event, 'id0')" id="defaultOpen">
                        {{$project->name}}
                    </h1>
                    <!-- /Name -->

                    <div class="humanColumnSection humanColumnContentsIntroduction">
                        <div class="humanColumnSection humanColumnSectionIntroduction">

                            <!-- Title -->
                            <div class="humanColumnTitle">
                                <h3 class="humanColumnTitleText" id="introduction">Introduction</h3>
                            </div>
                            <!-- /Title -->

                            <!-- Description -->
                            <div class="humanColumnApiDescription markdown formalTheme">
                                <p>IoT Interface Specification (HTTPS).</p>
                                @foreach($project['authentication'] as $pro)
                                    <h2 id="sectionAuthentication">{{$pro->class_name}}</h2>
                                    <ul>  {!!$pro->description!!} </ul>
                                @endforeach

                                @foreach($project['general'] as $pro)
                                    <h2 id="sectionGeneral">{{$pro->class_name}}</h2>

                                    <ul>
                                        <li>
                                            <p><code>URL Path</code> {{$pro->url_path}} .</p>
                                        </li>
                                        <li>
                                            <p><code>Example URL</code>{{$pro->example_url}} </p>
                                        </li>
                                        <li>
                                            <p><code>Protocol</code> {{$pro->protocol}}.</p>
                                        </li>
                                        <li>
                                            <p><code>Response Format</code> {{$pro->response_format}} </p>
                                        </li>
                                        <li>
                                            <p><code>Date Format</code> {{$pro->date_format}}.</p>
                                        </li>
                                    </ul>

                                @endforeach


                                @foreach($project['commonTerms'] as $pro)
                                    <h2 id="sectionTerms">{{$pro->class_name}}</h2>
                                    <p><code>Status</code>{!! $pro->status !!}</p>
                                    <p><code>Msg</code>{!! $pro->msg !!}</p>
                            @endforeach

                            <!-- /Description -->

                            </div>

                        </div>

                        <div class="humanColumnContentsAuth"></div>

                        <div class="humanColumnSection humanColumnContentsReference">
                            <div class="humanColumnSection humanColumnSectionReference">

                                <!-- Title -->
                                <div class="humanColumnTitle">
                                    <h3 class="humanColumnTitleText" id="reference">Reference</h3>
                                </div>
                                <!-- /Title -->

                                <div class="resourceGroups">
                                    <div class="resourceGroup">
                                    @foreach($project->projectParts as $projectPart)

                                        <!-- Name -->
                                            <h1 class="resourceGroupName" id="section{{$projectPart['id']}}">
                                                {{$projectPart['name']}}    </h1>
                                            <!-- /Name -->

                                            <!-- Description -->
                                            <div class="resourceGroupDescription markdown formalTheme">
                                                <p>{{$projectPart['description']}}.</p>
                                            </div>
                                            <!-- /Description -->

                                        @foreach($projectPart->partRequest as $partRequest)
                                            <!-- Resources -->
                                                <div class="resource">

                                                    <!-- Name -->
                                                    <h2 class="resourceName" id="section{{$partRequest['id']}}">
                                                        {{$partRequest->name}}  </h2>
                                                    <!-- /Name -->

                                                    <!-- Actions -->
                                                    <div class="resourceActions">
                                                        <div class="action get">

                                                            <!-- Invitation -->
                                                            <div class="actionInvitation get" class="tablinks" onclick="openMachineColumn(event, 'id{{$partRequest->id}}')">
                                                                <a class="actionInvitationRow">
                                                                    <div class="actionTag get"></div>

                                                                    <!-- Name -->
                                                                    <h4 class="actionName"> {{$partRequest->name}}      </h4>
                                                                    <!-- /Name -->

                                                                    <!-- Icon -->
                                                                    <div class="actionInvitationIcon"></div>
                                                                    <!-- /Icon -->

                                                                </a>
                                                            </div>
                                                            <!-- Invitation -->

                                                            <!-- Parameters -->
                                                            <div class="row actionParameters hidden"></div>
                                                            <!-- /Parameters -->


                                                            <!-- Description -->
                                                            <div class="actionDescription markdown formalTheme">
                                                                <p>{{$partRequest->description}}</p>
                                                                <p><code>HTTP Method: </code> {{$partRequest->type}}</p>
                                                                <p><code>Route: </code> {{$partRequest->url}}</p>
                                                            </div>
                                                            <!-- /Description -->

                                                        </div>

                                                        <div class="action put">


                                                            <!-- Parameters -->
                                                            <div class="row actionParameters hidden"></div>
                                                            <!-- /Parameters -->


                                                        </div>

                                                    </div>
                                                    <!-- /Actions -->

                                                </div>


                                                <!-- /Resources -->
                                            @endforeach

                                        @endforeach
                                    </div>

                                </div>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <!-- /HumanColumn -->

            <div id="id0" class="tabcontent div1 ">
                <!-- MachineColumnHeader -->
                <div id=" documentationMachineColumnHeader  " class="machineColumnHeader" >
                    <div class="row machineColumnHeaderContainer noSelect">

                        <!-- Back -->
                        <div class="columnHeaderButton columnHeaderBackButton backward">
                            <span class="icon backward"></span>
                        </div>


                        <!-- /Back -->

                    </div>
                </div>
                <!-- /MachineColumnHeader -->

                <!-- MachineColumn -->
                <div id="documentationMachineColumn" class="machineColumn">
                    <div class="row machineColumnContainer">
                        <div class="row machineColumnContents customScrollbar customScrollbarMachineColumn" style="height: 90%;">

                            <!-- BlankSlate -->
                            <div class="machineColumnBlankSlate">
                                <h3 class="blankSlateTitle">No action selected</h3>

                                <p class="blankSlateText">You can try selecting ‘<span class="blankSlateActionExample">Retrieve a Device</span>’ from the left column.</p>

                                <div class="blankSlateLink">
                                    <a href="/" class="blankSlateLinkText" target="_blank">Learn more about using the documentation.</a>
                                </div>

                            </div>
                            <!-- /BlankSlate -->

                        </div>

                    </div>
                </div>
            </div>


            @foreach($project->projectParts as $projectPart)

                @foreach($projectPart->partRequest as $partRequest)
                    <div id="id{{$partRequest['id']}}" class="tabcontent div1">
                        <!-- MachineColumnHeader -->
                        <div id=" documentationMachineColumnHeader  " class="machineColumnHeader" >
                            <div class="row machineColumnHeaderContainer noSelect">

                                <!-- Back -->
                                <div class="columnHeaderButton columnHeaderBackButton backward">
                                    <span class="icon backward"></span>
                                </div>


                                <!-- /Back -->

                            </div>
                        </div>
                        <!-- /MachineColumnHeader -->

                        <!-- MachineColumn -->
                        <div id="documentationMachineColumn " class="machineColumn" >

                            <div class="row machineColumnContainer" id="sec">

                                <div class="row machineColumnContents customScrollbar customScrollbarMachineColumn" >

                                    <div class="row machineColumnContent" >
                                        <!-- Breadcrumbs -->
                                        <div class="row machineColumnBreadcrumbsContainer" >

                                            <div class="row machineColumnBreadcrumbs">

                                                <div class="machineColumnBreadcrumbLine">

                                                    <!-- Resource Group -->
                                                    <a href="/#section{{$projectPart->id}}" class="machineColumnBreadcrumb machineColumnResourceGroupName">
                                                        {{$projectPart->name}}
                                                    </a>

                                                    <span class="machineColumnBreadcrumbsSlash">/</span>
                                                    <!-- /Resource Group -->

                                                    <!-- Action -->
                                                    <a href="/#section{{$partRequest->id}}" class="machineColumnBreadcrumb machineColumnActionName">
                                                        {{$partRequest->name}}
                                                    </a>
                                                    <!-- /Action -->

                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Breadcrumbs -->

                                        <!-- Destination -->
                                        <div class="row machineColumnDestination">

                                            <div class="destination">
                                                <!-- HTTP Method -->
                                                <span class="destinationMethod post">{{$partRequest->data->type}}</span>
                                                <!-- /HTTP Method --><!-- Protocol -->
                                                <span class="destinationProtocol">{{$partRequest->data->url}}</span>
                                                <!-- /Protocol -->

                                            </div>
                                        </div>
                                        <!-- /Destination -->
                                    @isset($partRequest->data['requestParams'])
                                        <!-- Parameters -->
                                            <div class="row machineColumnParameters">
                                                <div class="parameters lightTheme">

                                                    <!-- Title -->
                                                    <h3 class="row parametersTitle">Parameters</h3>
                                                    <!-- /Title -->
                                                @foreach($partRequest->data['requestParams'] as $dat)
                                                    <!-- List -->
                                                        <ul class="parametersList">
                                                            <li class="parameter required">
                                                                <div class="parameterRow">
                                                                    <!-- Key -->
                                                                    <div class="parameterColumn parameterKeyColumn">
                                                                        <div class="parameterKey">
                                                                            {{$dat->name}}
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Key -->

                                                                @if($dat['required'] == 1)
                                                                    <!-- Requirement -->
                                                                        <div class="parameterColumn parameterRequirementColumn">
                                                                            <div class="parameterRequirement">
                                                                                <span class="parameterIcon"></span>
                                                                                <span class="tooltip">Required</span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /Requirement -->
                                                                @endif

                                                                <!-- Description -->
                                                                    <div class="parameterColumn parameterDescriptionColumn">

                                                                        <!-- Description -->
                                                                        <div class="parameterDescription markdown lightTheme">
                                                                            <p>{{$dat->description}}</p>
                                                                        </div>
                                                                        <!-- /Description -->

                                                                    @if(!empty($dat->temp_value))
                                                                        <!-- Example -->
                                                                            <em class="parameterExampleText">Example: <code class="parameterExampleValue">
                                                                                    {{$dat->temp_value}}
                                                                                </code>.</em>
                                                                            <!-- /Example -->
                                                                        @endif


                                                                    </div>
                                                                    <!-- /Description -->

                                                                    <!-- Type -->
                                                                    <div class="parameterColumn parameterTypeColumn">
                                                                        <div class="parameterType">
                                                                            {{$dat->data_types_name}}
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Type -->

                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <!-- /List -->
                                                    @endforeach

                                                </div>
                                            </div>
                                            <!-- /Parameters -->
                                    @endisset

                                    @isset($partRequest->data['requestData'])
                                        <!-- Requestdata -->
                                            <div class="machineColumnRequest">

                                                <!-- Title -->
                                                <div class="row machineColumnTitle">
                                                    <h3 class="row machineColumnTitleText">Request</h3>
                                                </div>
                                                <!-- /Title -->

                                                <!-- Content -->
                                                <div class="machineColumnRequestContent">

                                                    <!-- Attributes -->
                                                    <div class="row machineColumnRequestAttributes">
                                                        <div class="row attributesTitle">
                                                            <h3 class="row attributesTitleText">Attributes</h3>
                                                        </div>

                                                        @foreach($partRequest->data['requestData'] as $dat)
                                                            <div class="row attributes"></div>
                                                            <div class="row attributesKit">
                                                                <div data-reactroot="" class="attributesKit">
                                                                    <style>
                                                                        .attributesKit p{margin-bottom: 4px;
                                                                            font-size: 14px;
                                                                            color: #8A93A3;
                                                                            line-height: 21px;
                                                                            font-weight: regular;}
                                                                        .attributesKit p:last-child{margin-bottom: 0px;}
                                                                        .attributesKit ul{margin-left: 20px;}
                                                                        .attributesKit a{color: #747E8E;
                                                                            text-decoration: none;
                                                                            border-bottom: 1px solid #DCE0E8;}
                                                                        .attributesKit a:hover{border-bottom: none;}
                                                                    </style>
                                                                    <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative; border-top: 1px solid rgb(133, 138, 159);">
                                                                        <div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;">
                                                                            <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                            </div>
                                                                            <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                                <div data-radium="true" style="width: 100%; height: auto;">
                                                                                    <div data-radium="true">
                                                                                        <div data-radium="true">
                                                                                            <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative; border-bottom: none; padding-top: 14px; padding-bottom: 14px;">
                                                                                                <div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;">
                                                                                                    <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: center; align-items: center; position: relative; padding-bottom: 14px;">
                                                                                                        <div data-radium="true" style="height: auto; width: 64px; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start; margin-left: 0px; margin-right: 40px; min-width: 64px; max-width: 64px;">
                                                                                                            <div data-radium="true" style="width: 100%; height: auto; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-weight: 500; font-size: 13px; line-height: 13px; color: rgb(255, 255, 255); word-break: keep-all; overflow-wrap: normal;">
                                                                                                                {{$dat->name}}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;">
                                                                                                            <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                                                                <div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;">
                                                                                                                    <div data-radium="true" style="width: 100%; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 13px; color: rgb(176, 185, 201); line-height: 13px;">
                                                                                                                        @if($dat['is_array']== 1 )
                                                                                                                            array
                                                                                                                        @endif
                                                                                                                        @if(!empty($dat['child']))
                                                                                                                            object
                                                                                                                        @endif
                                                                                                                        @if($dat['is_array']== 0 && empty($dat['child']))
                                                                                                                            {{$dat['data_types_name']}}
                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                                                                <div data-radium="true">
                                                                                                                    <div data-radium="true" style=" font-size: 13px; color: rgb(138, 147, 163); line-height: 150%;">
                                                                                                                        <p>{{$dat->description}}</p>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="collapsible" data-radium="true" style="width: 13px; height: 13px; max-width: 13px; min-width: 13px; cursor: pointer; margin-right: 7px;"><div data-radium="true" style="width: 13px; height: 13px; background-repeat: no-repeat; background-position: center center; background-image: url(&quot;data:image/svg+xml;utf8,<svg width=\&quot;13\&quot; height=\&quot;8\&quot; viewBox=\&quot;0 0 13 8\&quot; xmlns=\&quot;http://www.w3.org/2000/svg\&quot;><title>Slice 1</title><path d=\&quot;M6.51 4.695l3.98-3.98a1.128 1.128 0 0 1 1.589.003c.442.442.44 1.15.002 1.588L7.303 7.084a1.118 1.118 0 0 1-.79.326 1.104 1.104 0 0 1-.796-.326L.94 2.306A1.128 1.128 0 0 1 .942.718 1.12 1.12 0 0 1 2.53.715l3.98 3.98z\&quot; fill=\&quot;%2349B7FF\&quot; fill-rule=\&quot;evenodd\&quot;/></svg>&quot;); background-size: 13px 8px;"></div></div>
                                                                                            <div class="content">
                                                                                                <div data-radium="true" style="min-width: 100%; max-width: 100%; height: auto; padding-left: 6px;">
                                                                                                    <div data-radium="true" style="min-width: 100%; max-width: 100%; width: 100%; border-left: 1px solid transparent; position: relative;"><div data-radium="true" style="position: absolute; top: -14px; left: -1px; width: 1px; height: 14px; background-color: transparent;"></div><div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;"><div data-radium="true"><div data-radium="true" style=" font-size: 13px; color: rgb(138, 147, 163); line-height: 150%; margin-bottom: 14px; padding-left: 13px;"><p>Array of SIM numbers to activate</p>
                                                                                                                </div></div></div>
                                                                                                        <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;"><div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;"><div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;"><div style="width: 100%; height: 5px; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative; border-top: 1px solid rgb(133, 138, 159); border-right: 1px solid rgb(133, 138, 159); border-bottom: none; border-left: 1px solid rgb(133, 138, 159); border-image: initial; background-color: rgb(133, 138, 159);"></div><div data-radium="true" style="width: 100%; height: auto; border: 1px solid rgb(133, 138, 159);"><div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative; border-bottom: 1px solid rgb(133, 138, 159); padding-top: 8px; padding-bottom: 8px;"><span data-radium="true" style="position: absolute; top: 8px; right: 8px; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 13px; color: rgb(138, 147, 163); opacity: 0.56;">0</span><div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start; padding-left: 8px;"><div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;"><div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;"><div data-radium="true" style="width: auto; height: auto; margin: 0px; font-weight: 600; background-color: rgb(105, 109, 130); border-radius: 3px; padding: 4px 8px; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 13px; line-height: 13px; color: rgb(255, 255, 255); word-break: break-word;">9999999999999</div></div><div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: center; align-items: flex-start;"><div data-radium="true" style="width: 100%; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 13px; color: rgb(176, 185, 201); line-height: 13px; margin-bottom: 0px;">number</div></div></div></div></div><div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative; border-bottom: none; padding-top: 8px; padding-bottom: 8px;"><span data-radium="true" style="position: absolute; top: 8px; right: 8px; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 13px; color: rgb(138, 147, 163); opacity: 0.56;">1</span><div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start; padding-left: 8px;"><div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;"><div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;"><div data-radium="true" style="width: auto; height: auto; margin: 0px; font-weight: 600; background-color: rgb(105, 109, 130); border-radius: 3px; padding: 4px 8px; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 13px; line-height: 13px; color: rgb(255, 255, 255); word-break: break-word;">1111111111111</div></div><div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: center; align-items: flex-start;"><div data-radium="true" style="width: 100%; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 13px; color: rgb(176, 185, 201); line-height: 13px; margin-bottom: 0px;">number</div></div></div></div></div></div></div></div></div></div></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <!-- /Attributes -->

                                                </div>
                                                <!-- /Content -->

                                            </div>
                                            <!--/ RequestData -->
                                    @endisset

                                    <!-- ResponceData -->
                                        @isset($partRequest->data['requestResponses'])
                                            <div class="machineColumnResponse">

                                                <!-- Title -->
                                                <div class="row machineColumnTitle">
                                                    <h3 class="row machineColumnTitleText">Response</h3>
                                                </div>
                                                <!-- /Title -->

                                                <!-- Content -->
                                                <div class="machineColumnResponseContent">
                                                    @foreach($partRequest->data['requestResponses'] as $dat)
                                                        <div class="response">
                                                            <!-- StatusCode -->
                                                            <div class="responseStatusCode" style="margin-bottom: 0px">
                                                                {{$dat['responses_status_code']}} (  {{$dat['request_msg']}}  )
                                                            </div>
                                                            <!-- /StatusCode -->
                                                        </div>

                                                        <!-- Attributes -->
                                                        <div class="row machineColumnResponseAttributes">
                                                            <div class="row attributesTitle">
                                                                <h3 class="row attributesTitleText">Attributes</h3>
                                                            </div>
                                                            @foreach($dat['requestResponsesData'] as $requestResponseData)
                                                                <div class="row attributes"></div>
                                                                <div class="row attributesKit">
                                                                    <div data-reactroot="" class="attributesKit">
                                                                        <style>
                                                                            .attributesKit p{margin-bottom: 4px;
                                                                                font-size: 14px;
                                                                                color: #8A93A3;
                                                                                line-height: 21px;
                                                                                font-weight: regular;}
                                                                            .attributesKit p:last-child{margin-bottom: 0px;}
                                                                            .attributesKit ul{margin-left: 20px;}
                                                                            .attributesKit a{color: #747E8E;text-decoration: none;border-bottom: 1px solid #DCE0E8;}
                                                                            .attributesKit a:hover{border-bottom: none;}
                                                                        </style>
                                                                        <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative; border-top: 1px solid rgb(133, 138, 159);">
                                                                            <div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;">
                                                                                <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;"></div><div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                                    <div data-radium="true" style="width: 100%; height: auto;">
                                                                                        <div data-radium="true">
                                                                                            <div data-radium="true">
                                                                                                <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative; border-bottom: 1px solid rgb(88, 92, 110); padding-top: 14px; padding-bottom: 14px;">
                                                                                                    <div data-radium="true" style="height: auto; width: 50px; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start; margin-left: 20px; margin-right: 40px; min-width: 50px; max-width: 50px;">
                                                                                                        <div data-radium="true" style="width: 100%; height: auto; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-weight: 500; font-size: 13px; line-height: 13px; color: rgb(255, 255, 255); word-break: keep-all; overflow-wrap: normal;">
                                                                                                            {{$requestResponseData['name']}}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;">
                                                                                                        <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                                                            <div data-radium="true" style="height: auto; width: 100%; display: flex; flex-direction: column; justify-content: flex-start; align-items: flex-start;">
                                                                                                                <div data-radium="true" style="width: 100%; font-family: SFMono-Regular, Consolas, &quot;Liberation Mono&quot;, Menlo, Courier, monospace; font-size: 13px; color: rgb(176, 185, 201); line-height: 13px;">
                                                                                                                    {{$requestResponseData['data_types_name']}}
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                                                            <div data-radium="true">
                                                                                                                <div data-radium="true"
                                                                                                                     style="font-size: 13px; color: rgb(138, 147, 163); line-height: 150%;">
                                                                                                                    <p>{{$requestResponseData['description']}}</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        @if(!empty($requestResponseData['temp_value']))
                                                                                                            <div style="width: 100%; height: auto; display: flex; flex-direction: row; justify-content: flex-start; align-items: stretch; position: relative;">
                                                                                                                <div data-radium="true" style="width: auto; height: auto; margin: 6px 0px 0px;
                                                                                                 font-weight: 600; background-color: rgb(105, 109, 130); border-radius: 3px; padding: 4px 8px; font-size: 13px; line-height: 13px; color: rgb(255, 255, 255); word-break: break-word;">
                                                                                                                    {{$requestResponseData['temp_value']}}
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        @endif
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <!-- /Attributes -->

                                                        <!-- Schema -->
                                                        <div class="row machineColumnResponseSchema"></div>
                                                        <!-- /Schema -->

                                                    @endforeach
                                                </div>
                                                <!-- Content -->

                                            </div>

                                    @endisset
                                    <!--/ ResponceData -->
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /MachineColumn -->
                    </div>

                @endforeach

            @endforeach

        </div>


    </div>

    <script>
        function openMachineColumn(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>

@endsection

