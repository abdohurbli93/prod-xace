@extends('index')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit User</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Enter New Information:</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('editClientModel')}}">
                                    @csrf
                                    <input value="{{$editClientShow->id}}" name="user_id" hidden>
                                    <input value="{{$editClientShow->client_id}}" name="client_id" hidden>
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Client Name:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-compass text-primary"></i></span>
                                                    </div>
                                                    <input type="text" value="{{$editClientShow->client_name}}" id="username" class="form-control" name="username" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Username:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" value="{{$editClientShow->username}}" id="username" class="form-control" name="username" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>E-mail:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-envelope text-primary"></i></span>
                                                    </div>
                                                    <input type="email" value="{{$editClientShow->email}}"  id="email" class="form-control" name="email" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>User Type:</label>
                                                <select required class="form-control" name="user_type">
                                                    <option value="admin" {{$editClientShow->user_type=='admin'?'selected':''}}>admin</option>
                                                    <option value="client" {{$editClientShow->user_type=='client'?'selected':''}}>client</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Phone:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-phone text-primary"></i></span>
                                                    </div>
                                                    <input value="{{$editClientShow->phone_number}}" name="phone_number" class="form-control" data-mask="" im-insert="true" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Address:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-map-marker-alt text-primary"></i></span>
                                                    </div>
                                                    <textarea rows="5" id="address" class="form-control" name="address" required>{{$editClientShow->address}}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label></label>
                                                <div class="input-group">
                                                    <button  class="btn btn-primary font-weight-bold" >
                                                        <i class="fa fa-check"></i>
                                                        Save</button>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>

    <script>
        function showPassword() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
                passStatus.className='fa fa-eye eyePassword text-primary';
            } else {
                x.type = "password";
                passStatus.className='fa fa-eye-slash eyePassword text-primary';

            }
        }
    </script>
@endsection
