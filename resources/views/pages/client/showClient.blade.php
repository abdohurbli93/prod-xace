@extends('includes.footerDataTable')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> View Client </h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card  card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">View The List Of Client</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="overflow-x: scroll">
                            <table id="example1" class="table table-bordered table-striped text-center" >
                                <thead>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Username</th>
                                    <th>E-mail</th>
                                    <th>Address</th>
                                    <th>User Type</th>
                                    <th>Phone Number</th>
                                    <th>SIM</th>
                                    <th>Sensor</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody >
                                @foreach($clients as $client)
                                    <tr>
                                        <input type="hidden" name="id" value="{{$client->client_id}}">
                                        <td>{{$client->client_name}}</td>
                                        <td>{{$client->username}}</td>
                                        <td>{{$client->email}}</td>
                                        <td>{{$client->address}}</td>
                                        <td>{{$client->user_type}}</td>
                                        <td>{{$client->phone_number}}</td>
                                        <td>{{$client->countAllSim}}</td>
                                        <td>{{$client->countAllSensor}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a type="button" title="Delete"  onclick="deleteClient({{$client->client_id}})" href="#" class="btn btn-primary"><i class="fas fa-trash"></i></a>
                                                <a type="button" title="Edit" href="/editClientShow/{{$client->id}}" class="btn btn-dark"><i class="fas fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Client Name</th>
                                    <th>Username</th>
                                    <th>E-mail</th>
                                    <th>Address</th>
                                    <th>User Type</th>
                                    <th>Phone Number</th>
                                    <th>SIM</th>
                                    <th>Sensor</th>
                                    <th>#</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="{{ asset('js/include/delete_user.js') }}"></script>

@endsection
