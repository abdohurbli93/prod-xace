@extends('index')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add User</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="col-md-7">
            @include('includes.messages')
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Enter New Information:</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="settings">
                                <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{route('addClientModel')}}">
                                    @csrf
                                    <div class="col-md-12 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Client Name:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user-alt text-primary"></i></span>
                                                    </div>
{{--                                                    <input type="text" name="client_name" value="{{ old( 'client_name') }}" id="client_name" class="form-control" placeholder="client Name" required>--}}
                                                    <input id="client_name" type="text" placeholder="client Name" name="client_name" class="form-control @error('client_name') is-invalid @enderror"value="{{ old('client_name') }}" required autocomplete="client_name" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Username:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-user text-primary"></i></span>
                                                    </div>
                                                    <input type="text" id="username"  name="username" placeholder="Username" class="form-control @error('username') is-invalid @enderror"value="{{ old('usernaqme') }}" required autocomplete="username" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>E-mail:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-envelope text-primary"></i></span>
                                                    </div>
                                                    <input type="email"  id="email"  name="email" placeholder="E-mail" class="form-control @error('email') is-invalid @enderror"value="{{ old('email') }}" required autocomplete="email" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Password:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i id="pass-status" class="fa fa-fw fa-eye-slash field-icon text-primary eyePassword" onclick="showPassword()"></i></span>
                                                    </div>
                                                    <input  type="password" id="password"  name="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror"value="{{ old('password') }}" required autocomplete="password" autofocus>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Phone:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-phone text-primary"></i></span>
                                                    </div>
                                                    <input type="number" placeholder="Phone" name="phone_number" class="form-control @error('phone_number') is-invalid @enderror"value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label>Address:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-map-marker-alt text-primary"></i></span>
                                                    </div>
                                                    <textarea rows="4" id="address"  name="address" placeholder="Address" class="form-control @error('address') is-invalid @enderror"value="{{ old('address') }}" required autocomplete="address" autofocus></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-top: 30px;">
                                                <label>User Type:</label>
                                                <select required id="selector1" name="user_type" class="form-control @error('user_type') is-invalid @enderror"value="{{ old('user_type') }}" required autocomplete="user_type" autofocus >
                                                    <option value="" disabled selected>choose..</option>
                                                    <option value="admin">admin</option>
                                                    <option value="client">client</option>
                                                </select>
                                            </div>

                                            <div class="form-group chooseDiv1" id="client" style="display: none">
                                                <label>Client Type:</label>
                                                <select required id="selector"  name="client_type" class="form-control @error('client_type') is-invalid @enderror"value="{{ old('client_type') }}" required autocomplete="user_type" autofocus >
                                                    <option value="" disabled selected>choose..</option>
                                                    <option value="test">test</option>
                                                    <option value="production">production</option>
                                                </select>
                                            </div>

                                            <div class="chooseDiv form-group" id="test" style="display: none">
                                                <label>Client Balance:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-sim-card text-primary"></i></span>
                                                    </div>
                                                    <input type="number" min="0" value="0" placeholder="Client Balance" name="client_balance" class="form-control @error('client_balance') is-invalid @enderror" autocomplete="client_balance" autofocus>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label></label>
                                            <div class="input-group">
                                                <button type="submit"  class="btn btn-primary font-weight-bold" ><i class="fa fa-check"></i> Save</button>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>

        </section>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(function() {
            $('#selector').change(function(){
                $('.chooseDiv').hide();
                $('#' + $(this).val()).show();
                $('#' + $(this).val()).show();
            });

            $('#selector1').change(function(){
                $('.chooseDiv1').hide();
                $('#' + $(this).val()).show();
                $('#' + $(this).val()).show();
            });
        });

        function showPassword() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
                passStatus.className='fa fa-eye eyePassword text-primary';
            } else {
                x.type = "password";
                passStatus.className='fa fa-eye-slash eyePassword text-danger';

            }
        }
    </script>
@endsection
