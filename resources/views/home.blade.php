@extends('layouts.app')

@section('content')
    <!-- Vertical navbar -->
    <div class="vertical-nav text-dark bg-ebla text-right  mr-2 shadow" id="sidebar">

        <div class="text-center pl-4 pt-2 bg-white shadow" style="height: 70px">
            <div class="media d-flex align-items-center">

                <img src="https://flexiroam-public.s3-ap-southeast-1.amazonaws.com/flexiroam2019/image/app-icon.png " width="55" alt="logo">
                <img CLASS="ml-3"  src="https://flexiroam-public.s3-ap-southeast-1.amazonaws.com/flexiroam2019/image/logo.png"  width="110">
            </div>
        </div>

        <div class="text-left bg-ebla" style="overflow-y: auto;height:100%; padding-bottom: 100px;">
            <h5 class="font-weight-bold text-light text-center px-3 pb-3 pt-3" style="border-bottom: 1px solid #4f5962;">
                <a class="text-light" href="/home">
                    Dashboard
                </a>
            </h5>
            <ul class="nav flex-column text-gray  ml-4 mt-4">
{{--                <b class="text-white mb-1">Main </b>--}}
                <li class="nav-item ">
                    <a href="/home" class="nav-link  font-weight-bolder ">
                        <i class="fa fa-home mr-1 fa-fw text-white"></i>Home
                    </a>
                </li>
                <li class="nav-item mt-1">
                    <a href="/updateUser" class="nav-link  font-weight-bolder ">
                        <i class="fa fa-edit mr-1 fa-fw text-white"></i>Update Client Info
                    </a>
                </li>
            </ul>
        </div>


    </div>
    <!-- End vertical navbar -->

    <!-- Page content holder -->
    <div class="page-content p-5 mb-lg-5" id="content">
        <!-- Toggle button -->

        <nav class=" navbar fixed-top pr-5  navbar-expand-sm navbar-danger bg-white shadow px-4"
             style="left: unset; width: inherit; height: 70px; ">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content"
                    aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Brand -->
            <button id="sidebarCollapse" type="button" class=" btn btn-light">
                <i class="fa fa-bars text-danger"></i>
            </button>

            <div class="ml-3">
                <ul class="navbar-nav">
                    <li class="nav-item d-none ml-3 d-sm-inline-block ">
                        <a href="/home" class="nav-link font-weight-bold" style="border-bottom: #dc2726 solid" >Dashboard</a>
                    </li>
                    <li class="nav-item d-none ml-3 d-sm-inline-block">
                        <a href="/docs" class="nav-link ">Documentation</a>
                    </li>

                </ul>
            </div>


            <!-- Links -->
            <div class="collapse navbar-collapse " id="nav-content" style="direction: rtl;">
                <ul class="navbar-nav">

                    <div class="media d-flex align-items-center">
                        <div class="media-body ">
                            <b>{{auth()->user()->client_name}} </b>
                        </div>
{{--                        <img src="{{url('/design/img')}}/user.png"--}}
                        <img src="https://www.gravatar.com/avatar/4deca3014a7c60e87e0b5e8829388e95.png?default=https%3A%2F%2Fstatic.apiary.io%2Fassets%2F5Ew8Hq0N.png"
                             alt="..." width="40" class="mr-2 ml-2  rounded-circle shadow-sm">

                    </div>


                    <li class="nav-item dropdown text-dark pt-2">
                        <a class="dropbtn nav-link dropdown-toggle text-dark "
                           data-toggle="dropdown" id="Preview" href="#" role="button" aria-haspopup="true"
                           aria-expanded="false" style="font-size: 24px">

                        </a>
                        <div id="myDropdown" class="dropdown-content dropdown-menu" aria-labelledby="Preview">
                            <a class="dropdown-item text-center" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                                <i class="fa fa-sign-out text-danger mr-1 fa-fw"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="mt-5">
            @yield('content1')


        </div>
    </div>
    <!-- End Top navbar -->


@endsection
