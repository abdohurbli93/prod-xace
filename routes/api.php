<?php

use App\Curl;
use App\Documentation\Projects;
use App\Helper\HttpHelper;
use App\Http\Controllers\API\ClientApiController;
use App\Http\Controllers\API\FlexiRoam;
use App\Http\Controllers\API\Globiots;
use App\Jobs\HardwaresJob;
use App\User;
use App\Vendors\ClientHardware;
use App\Vendors\Clients;
use App\Vendors\Hardware;
use App\Vendors\Result;
use App\Vendors\VendorCredentials;
use App\Vendors\Vendors;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Vendors\HardwareData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('ApiErrorToken', function () {
    return HttpHelper::notAuthorized('access denied');
})->name('ApiErrorToken');

/*******devices Section*****/
Route::get("devices/getAllDevices", "API\Globiots@retrieveAllDevices");
Route::get('devices/getDeviceByHostName', "API\Globiots@retrieveDeviceByHostName");
Route::get('devices/getDeviceBySn', "API\Globiots@retrieveDeviceBySn");
Route::get('devices/getDeviceByStatus', "API\Globiots@retrieveDeviceByStatus");


/*******parameters Section*****/
Route::get('parameters/getAllParameterByHostName', "API\Globiots@retrieveAllParameterByHostName");
Route::get('parameters/getParametersByHostNameAndAddress', "API\Globiots@retrieveParametersByHostNameAndAddress");
Route::get('parameters/getParametersByHostNameAndLoggedStatus', "API\Globiots@retrieveParametersByHostNameAndLoggedStatus");

/*******alarms Section*****/
Route::get('alarms/getAllAlarmConfigurationByParameterId', "API\Globiots@retrieveAllAlarmConfigurationByParameterId");


/*******event Section*****/
Route::get('event/getAllEventConfigurationByHostName', "API\Globiots@retrieveAllEventConfigurationByHostName");


/*******memoryMap Section*****/
Route::get('memoryMap/getAllAddressOnMemoryMapByHostName', "API\Globiots@retrieveAllAddressOnMemoryMapByHostName");

/*******logData Section*****/
Route::get('logData/getLogDataMapByHostNameAndAddress', "API\Globiots@retrieveLogDataByHostNameAndAddress");
Route::post('logData/getLogDataMapByMultipleAddress', "API\Globiots@retrieveLogDataByMultipleAddress");
Route::get('logData/getLogDataMapByHostNameAndAddressAndDate', "API\Globiots@retrieveLogDataByHostNameAndAddressAndDate")
    ->name('globiots_data');
Route::get('logData/getLogDataMapByHostNameAndAddressAndDateJson', "API\Globiots@retrieveLogDataByHostNameAndAddressAndDateJson");
Route::post('logData/getLogDataMapByMultipleAddressAndDate', "API\Globiots@retrieveLogDataByMultipleAddressAndDate");


/*******alarm Log Data Section*****/
Route::get('alarmLogData/getAllDataOfAlarmSummery', "API\Globiots@retrieveAllDataOfAlarmSummery");
Route::get('alarmLogData/getAlarmSummeryByHostname', "API\Globiots@retrieveAlarmSummeryByHostname");
Route::get('alarmLogData/AlarmDetailByHostname', "API\Globiots@retrieveAlarmDetailByHostname");
Route::get('alarmLogData/AlarmHistoryByHostName', "API\Globiots@retrieveAlarmHistoryByHostName");



/*******Event Log Data Section*****/
Route::get('eventLogData/getEventHistoryByHostName', "API\Globiots@retrieveEventHistoryByHostName");


//Route::group(['middleware'=>['auth:api']],function ()
//{

/********vendor section********/
Route::post('/vendor/addVendor', "API\Vendors\VendorController@addVendor");
Route::post('/vendor/updateVendor', "API\Vendors\VendorController@updateVendor");
Route::post('/vendor/deleteVendor', "API\Vendors\VendorController@deleteVendor");


/********VendorCredential section********/
Route::post('/vendorCredential/addVendorCredential', "API\Vendors\VendorCredentialController@addVendorCredential");
Route::post('/vendorCredential/updateVendorCredential', "API\Vendors\VendorCredentialController@updateVendorCredential");
Route::post('/vendorCredential/deleteVendorCredential', "API\Vendors\VendorCredentialController@deleteVendorCredential");


/********Hardware section********/

Route::post('/hardware/addHardware', "API\Vendors\HardwareController@addHardware");
Route::post('/hardware/updateHardware', "API\Vendors\HardwareController@updateHardware");
Route::post('/hardware/deleteHardware', "API\Vendors\HardwareController@deleteHardware");


/********HardwareType section********/

Route::post('/hardwareType/addHardwareType', "API\Vendors\HardwareTypeController@addHardwareType");
Route::post('/hardwareType/updateHardwareType', "API\Vendors\HardwareTypeController@updateHardwareType");
Route::post('/hardwareType/deleteHardwareType', "API\Vendors\HardwareTypeController@deleteHardwareType");

/********client section********/

Route::post('/client/addClient', "API\Vendors\ClientController@addClient");
Route::post('/client/updateClient', "API\Vendors\ClientController@updateClient");
Route::post('/client/deleteClient', "API\Vendors\ClientController@deleteClient");
Route::post('/client/addHardware', 'API\Vendors\ClientController@addClientHardware');


/********client credential section********/

Route::post('/clientCredential/addClientCredential', "API\Vendors\ClientCredentialController@addClientCredential");
Route::post('/clientCredential/updateClientCredential', "API\Vendors\ClientCredentialController@updateClientCredential");
Route::post('/clientCredential/deleteClientCredential', "API\Vendors\ClientCredentialController@deleteClientCredential");

/*********client api***********/
Route::get('/client/api/getDevices', 'API\ClientApiController@getDeviceList');
Route::get('/client/api/getTemperatureByHardwareId', 'API\ClientApiController@getTemperatureByHardwareId');
Route::get('/client/api/getHumidityByHardwareId', 'API\ClientApiController@getHumidityByHardwareId');
Route::get('/client/api/getBatteryByHardwareId', 'API\ClientApiController@getBatteryByHardwareId');
Route::get('/client/api/getSimDataByHardwareId', 'API\ClientApiController@getSimDataByHardwareId');
Route::get('/client/api/getClientBalance', 'API\ClientApiController@getClientBalance');
Route::get('/client/api/getClientBalanceHistory', 'API\ClientApiController@getClientBalanceHistory');
Route::get('/client/api/getTemperatureHistoryByHardwareId', 'API\ClientApiController@getTemperatureHistoryByHardwareId');
Route::get('/client/api/getHumidityHistoryByHardwareId', 'API\ClientApiController@getHumidityHistoryByHardwareId');
Route::get('/client/api/getBatteryHistoryByHardwareId', 'API\ClientApiController@getBatteryHistoryByHardwareId');
Route::get('/client/api/getSimHistoryByHardwareId', 'API\ClientApiController@getSimHistoryByHardwareId');
Route::get('/client/api/getAllTemperatureDevicesHistory', 'API\ClientApiController@getAllDevicesTemperatureHistory');
Route::get('/client/api/getAllHumidityDevicesHistory', 'API\ClientApiController@getAllHumidityDevicesHistory');
Route::get('/client/api/getAllBatteryDevicesHistory', 'API\ClientApiController@getAllBatteryDevicesHistory');
Route::get('/client/api/getAllSimDevicesHistory', 'API\ClientApiController@getAllSimDevicesHistory');
Route::get('/client/api/getTemperatureFromSkuRang', 'API\ClientApiController@getTemperatureFromSkuRang');
Route::get('/client/api/getHumidityFromSkuRang', 'API\ClientApiController@getHumidityFromSkuRang');
Route::get('/client/api/getBatteryFromSkuRang', 'API\ClientApiController@getBatteryFromSkuRang');
Route::get('/client/api/getSimDataFromSkuRang', 'API\ClientApiController@getSimDataFromSkuRang');
Route::post('/client/api/login', 'API\ClientApiController@login');
Route::get('/getHeaderJson', "API\Globiots@getHeaderJson");
Route::get('/ttttt','API\ClientApiController@test');
Route::get('/ttttt','API\ClientApiController@test');



Route::get('/testingGets', function (Request $request) {
    $vendor = VendorCredentials::getVendorCredentialById(1);
    $headerJson = json_decode($vendor['header_json'], true);
    $url = $vendor->base_url;
    $userName = $headerJson['username'];
    $password = $headerJson['password'];
    $curl = new Curl($userName . ":" . $password, $url);
    $hostIp = $request->get('ip');
    $address = $request->get('address');
    $fromDate = $request->get('from_date');
    $toDate = $request->get('to_date');
    $fromDate = Carbon::now()->subSeconds(300)->format('YmdHi00');
    $toDate = Carbon::now()->format('YmdHi00');
    if (empty($hostIp) || empty($address) || empty($fromDate) || empty($toDate))
        return HttpHelper::errorMissingParameter('missing required param');
    $response = $curl->callCurl(
        '/rest/api/public/v2/data-log?hostname=' . $hostIp . '&address=' . $address .
            '&fromDate=' . $fromDate . '&toDate=' . $toDate,
        null,
        true,
        false
    );
    $response = json_decode($response, true);
    if ($response['responseCode'] == 200) {
        $data = $response['data'];
        foreach ($data as $res) {
            $result['value'] = $res[1];
            $result['date_time'] = Carbon::createFromTimestampMs($res[0])->toDateTimeString();
            $result['hardware_id'] = 1;
            Result::addResult($result);
        }
    }
    return $response;
});

Route::get('testingData', function () {
    $data = Result::getResultValuesByHardwareId(1);
    $data = array_reverse($data->toArray());
    $label = Result::getResultDateTimeByHardwareId(1);
    $label = array_reverse($label->toArray());
    return [$data, $label];
});

Route::get('testingData12', function () {
    $project = \App\Documentation\Projects::getProjectWithDetailByCompanyId(1);
    return $project;
});

Route::get('cronData', function () {
    try {
        $simHardware = Hardware::getHardwareByType('sim');
        dispatch(new HardwaresJob($simHardware));
        $sensorHardware = Hardware::getHardwareByType('sensor');
        dispatch(new HardwaresJob($sensorHardware));
    } catch (Exception $e) {
        var_dump($e);
    }
});
Route::get('NewCronData', function () {

    return 'success';
});

Route::get('testin12', function (Request $request) {
    $responseTemperature = [];
    $responseHumidity = [];
    $responseBattery = [];
    $responseSimConsumption = [];


        //01/30/2019 12:00 AM - 01/31/2020 12:00 AM
        $fromDate = Carbon::make(Carbon::now()->subDays(1));
        $toDate = Carbon::make(Carbon::now());
        $date = Carbon::now()->subMonths(3)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
        $hardwareIds = Hardware::getAllHardware(3);

    //            if (!empty($hardwareIds)) {
    $responseTemperature =  HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
    $responseHumidity = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
    $responseBattery = HardwareData::getAllBatteryHistory($hardwareIds, $fromDate, $toDate);
    $responseSim = HardwareData::getAllSimDatausageHistory($hardwareIds, $fromDate, $toDate);
    $responseSimConsumption=HardwareData::getAllSimDataConsumptionHistory($hardwareIds, $fromDate, $toDate);
    //            }
    return [$responseTemperature,$responseHumidity,$responseBattery,$responseSim,$responseSimConsumption,$date,$hardwareIds
    ,$fromDate,$toDate];
    return view('pages.end_home', ['date' => $date, 'data1' => $responseBattery,
        'data2' => $responseHumidity,'data3'=>$responseSim,'data4'=>$responseSimConsumption, 'data' => $responseTemperature]);
});



Route::get('testing12', function () {

   $hardware=collect([1,2,3,4]);
   $data=array();
   $temp=null;
    $i=0;
   foreach ($hardware as $hard)
   {
        $device=Hardware::getDeviceById($hard);
       $latestData=HardwareData::getLatestHardwareData($hard);
       if(empty($latestData->humditiy))
           $temp[$i]['humidity']['status']='Inactive';
       else $temp[$i]['humidity']['status']='Active';
       if(empty($latestData->temperature))
           $temp[$i]['temperature']['status']='Inactive';
       else $temp[$i]['temperature']['status']='Active';
       $temp[$i]['temperature']['data']=HardwareData::getLastHardwareTypeDataBySku($device->sku,'temperature')['value'];
       $temp[$i]['humidity']['data']=HardwareData::getLastHardwareTypeDataBySku($device->sku,'humidity')['value'];$temp;
       $i++;
   }
  return $temp;
});

Route::get('testing13', function (Request $request) {
    Auth::login(User::getUserById(3));
if ($request->has('date')) {
    $date = explode('-', $request['date']);
    $fromDate = Carbon::make($date[0]);
    $toDate = Carbon::make($date[1]);
    $date = $request->get('date');
} else {
    $fromDate = Carbon::make(Carbon::now()->subDays(1));
    $toDate = Carbon::make(Carbon::now());
    $date = Carbon::now()->subDays(1)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
}

        $sku = $request->get('sku');
        $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $sku, $sku);
        $type=$request->get('type');
    if($type=='temperature'){
        $data =  HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
        $prefix='C';
    }
    else{
        $data = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
        $prefix='%';
    }
    return view('pages.chart_data', ['date' => $date,'data'=>$data, 'type' => $type,'prefix'=>$prefix,'sku'=>$sku]);

});
    Route::get('testing14', function (Request $request) {
    Auth::login(User::getUserById(3));
    if ($request->has('date')) {
        $date = explode('-', $request['date']);
        $fromDate = Carbon::make($date[0]);
        $toDate = Carbon::make($date[1]);
        $date = $request->get('date');
    } else {
        $fromDate = Carbon::make(Carbon::now()->subDays(1));
        $toDate = Carbon::make(Carbon::now());
        $date = Carbon::now()->subDays(1)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
    }

    $sku = $request->get('sku');
    $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $sku, $sku);
    $type=$request->get('type');
    if($type=='temperature'){
        $data =  HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
        $prefix='C';
    }

    else{
        $data = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
        $prefix='%';
    }
    return response()->json(['date'=>$date,'data'=>json_decode($data,true)
    ,'prefix'=>$prefix,'sku'=>$sku],200);

});
Route::get('updatePassword', function () {
    $user = User::find(4);
    $user->password = Hash::make("12345678");
    $user->save();
});

Route::get('/tester',function ()
{
    $fromDate = Carbon::make(Carbon::now()->subDays(1));
    $toDate = Carbon::make(Carbon::now());
    $date = Carbon::now()->subMonths(3)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
    $data=HardwareData::getAllHardwareDataTypeHistory('tempreture',3,$fromDate,$toDate);
    Auth::login(User::getUserById(3));
    return view('pages.chart_data',['date' => $date]);
});


Route::get('/change',function(){

    $user = User::find(3);

    $user->password = Hash::make('xace-demo7853');
    $user->save();

});

