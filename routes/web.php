<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Documentation\Projects;
use App\Vendors\Hardware;
use App\Vendors\HardwareData;
use App\Vendors\VendorCredentials;
use App\Vendors\Vendors;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::get('/zone1',function (){
    $hardware=Hardware::getAllHardware(Auth::id(),'sim');
    $data=array();
    $i=0;
    foreach ($hardware as $hard)
    {
        $device=Hardware::getDeviceBySku($hard);
        $data[$i]['sku']=$hard;
        $data[$i]['imsi']=$device->imsi;
        $data[$i]['sn']=$device->sn;
        $data[$i]['device_name']=$device->device_name;
        $data[$i]['msisdn']=$device->msisdn;
        $data[$i]['iccid']=$device->iccid;
        $data[$i]['pin']=$device->pin;
        $data[$i]['puk']=$device->puk;
        $simBalance=HardwareData::getLastHardwareTypeDataBySku($hard,'data_usage');
        $simUsage=HardwareData::getSimConsumptionHistoryBySku($hard);
        $data[$i]['balance']=$simBalance->value;
        $data[$i]['data_usage']=$simUsage;
        $i++;
    }
    return $data;
});
Route::get('/sim',function (){
    return view('pages.customer.chard_sim');
});
Route::get('/getDevice',function (){
    return view('pages.customer.git_device');
});

Route::get('/test', 'HomeController@showHardware');
//Route::get('/alaa', 'HomeController@master');



Route::get('/',function (){
    return view('pages.master');
});

Auth::routes();

Route::get('/login', function () {
    return view('pages.login');
})->name('login')->middleware('guest');



Route::group(['middleware' => 'auth'], function () {
    Route::get('/homeJson', 'HomeController@indexAjax');
    Route::get('/docs', function () {
        $project = Projects::getProjectWithDetailByCompanyId(1);
        return view('pages.docs', compact('project'));
    });
    Route::get('/home', 'HomeController@master');
    Route::get('/singlePageStatistic', 'HomeController@singlePageStatistic');
    Route::get('/getMultiSimDataJson', 'HomeController@MultiSimDataJson');
    Route::get('/getAllSimDataJson', 'HomeController@AllSimDataJson');
    //edit user
    Route::get('/updateUser', 'HomeController@editUserShow');
    Route::post('/updateUserModel', 'HomeController@updateUserModel')->name('updateUserModel');
    Route::get('/docs', function () {
        $project = Projects::getProjectWithDetailByCompanyId(1);
        return view('pages.docs', compact('project'));
    });

    Route::get('/getDeviceList', 'HomeController@getDeviceList')->name('getDeviceList');

    Route::get('/getDetailSim', 'HomeController@getDetailSim')->name('getDetailSim');
    Route::post('/addClientBalance', 'HomeController@addClientBalance')->name('addClientBalance');
    Route::post('/addClientBalanceZone1', 'HomeController@addClientBalanceZone1')->name('addClientBalanceZone1');
    Route::get('/simStatistic', 'HomeController@SimData')->name('simStatistic');
    Route::get('/simDataJson', 'HomeController@simDataJson')->name('simDataJson');



//Add Vendor
    Route::get('/addVendor', 'AdminController@addVendor')->name('addVendor');
    Route::post('/addVendorModel', 'AdminController@addVendorModel')->name('addVendorModel');
//Show Vendor
    Route::get('/showVendor', 'AdminController@showVendor')->name('showVendor');
    Route::get('/showDetailVendor/{id}', 'AdminController@showDetailVendor')->name('showDetailVendor');
//Edit Vendor
    Route::get('/editVendorShow/{id}', 'AdminController@editVendorShow');
    Route::post('/updateVendorModel', 'AdminController@updateVendorModel')->name('updateVendorModel');
//delete Vendor
    Route::post('/deleteVendor', 'AdminController@deleteVendor');

//Add Hardware
    Route::get('/addHardware', 'AdminController@addHardware')->name('addHardware');
    Route::post('/addHardwareModel', 'AdminController@addHardwareModel')->name('addHardwareModel');
//Show Hardware
    Route::get('/showHardware', 'AdminController@showHardware')->name('showHardware');
    Route::get('/showDetailHardware/{id}', 'AdminController@showDetailHardware')->name('showDetailHardware');
//Edit Hardware
    Route::get('/editHardwareShow/{id}', 'AdminController@editHardwareShow');
    Route::post('/updateHardwareModel', 'AdminController@updateHardwareModel')->name('updateHardwareModel');
//delete Hardware
    Route::post('/deleteHardware', 'AdminController@deleteHardware');


//Add Client
    Route::get('/addClient', 'AdminController@addClient')->name('addClient');
    Route::post('/addClientModel', 'AdminController@addClientModel')->name('addClientModel');
//Show Client
    Route::get('/showClient', 'AdminController@showClient')->name('showClient');
    Route::get('/showDetailClient/{id}', 'AdminController@showDetailClient')->name('showDetailClient');
//Edit Client
    Route::get('/editClientShow/{id}', 'AdminController@editClientShow');
    Route::post('/editClientModel', 'AdminController@editClientModel')->name('editClientModel');
//Delete Client
    Route::post('/deleteClient', 'AdminController@deleteClient');


//Show Rang Device
    Route::get('/showRangDevice', 'AdminController@showRangDevice')->name('showRangDevice');

//Add Rang Device
    Route::get('/addRangeDevice', 'AdminController@addRangeDevice')->name('addRangeDevice');
    Route::post('/addRangeDeviceModel', 'AdminController@addRangeDeviceModel')->name('addRangeDeviceModel');

//Delete Rang Device
    Route::post('/deleteRangeDevice', 'AdminController@deleteRangeDevice');
//view Details Rang Device
    Route::get('/viewDetailsRange/{id}', 'AdminController@viewDetailsRange');


});


Route::get('errorToken', function () {
    return redirect('http://flexiot.404developers.com');
})->name('errorToken');


Route::get('admin/home','AdminController@index');


Route::get('testing13', function (Request $request) {
    if ($request->has('date')) {
        $date = explode('-', $request['date']);
        $fromDate = Carbon::make($date[0]);
        $toDate = Carbon::make($date[1]);
        $date = $request->get('date');
    } else {
        $fromDate = Carbon::make(Carbon::now()->subDays(1));
        $toDate = Carbon::make(Carbon::now());
        $date = Carbon::now()->subDays(1)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
    }

    $sku = $request->get('sku');
    $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $sku, $sku);
    $type=$request->get('type');
    if($type=='temperature'){
        $data =  HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
        $prefix='C';
    }
    else{
        $data = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
        $prefix='%';
    }
    return view('pages.chart_data', ['date' => $date,'data'=>$data, 'type' => $type,'prefix'=>$prefix,'sku'=>$sku]);

})->middleware('auth');
Route::get('testing14', function (Request $request) {
    if ($request->has('date')) {
        $date = explode('-', $request['date']);
        $fromDate = Carbon::make($date[0]);
        $toDate = Carbon::make($date[1]);
        $date = $request->get('date');
    } else {
        $fromDate = Carbon::make(Carbon::now()->subDays(1));
        $toDate = Carbon::make(Carbon::now());
        $date = Carbon::now()->subDays(1)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
    }

    $sku = $request->get('sku');
    $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $sku, $sku);
    $type=$request->get('type');
    if($type=='temperature'){
        $data =  HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
        $prefix='C';
    }

    else{
        $data = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
        $prefix='%';
    }
    return response()->json(['date'=>$date,'data'=>json_decode($data,true)
        ,'prefix'=>$prefix,'sku'=>$sku],200);

})->middleware('auth');
