function deleteUser($id) {
    console.log($id);
    swal({
        title: "are you sure?",
        text: "When deleted, you will not be able to restore the information!",
        icon: "warning",
        dangerMode: true,
        buttons: ["cancel","Yes delete!"],
        showLoaderOnConfirm: true,


    })
        .then((willDelete) =>
        {
            if (willDelete)
            {
                swal({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    buttons: false,
                    onOpen: () => {
                        swal.showLoading()
                    }
                })

                $.ajax
                ({
                    url : "/deleteVendor" ,
                    type : "POST",
                    data : {id:$id},
                    success: function()
                    {
                        console.log( 'id'+$id);
                        swal("You have successfully deleted",
                            {
                                icon: "success",
                            }).then(function ()
                        {
                            window.location.reload();
                        });
                    }
                })
            }
            else
            {

                swal("cancel", "", "error");
            }
        })
}

function deleteClient($id) {
    console.log($id);
    swal({
        title: "are you sure?",
        text: "When deleted, you will not be able to restore the information!",
        icon: "warning",
        dangerMode: true,
        buttons: ["cancel","Yes delete!"],
        showLoaderOnConfirm: true,


    })
        .then((willDelete) =>
        {
            if (willDelete)
            {
                swal({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    buttons: false,
                    onOpen: () => {
                        swal.showLoading()
                    }
                })

                $.ajax
                ({
                    url : "/deleteClient" ,
                    type : "POST",
                    data : {id:$id},
                    success: function()
                    {
                        swal("You have successfully deleted",
                            {
                                icon: "success",
                            }).then(function ()
                        {
                            window.location.reload();
                        });
                    }
                })
            }

            else
            {

                swal("cancel", "", "error");
            }
        })
}

function deleteHardware($id) {
    console.log($id);
    swal({
        title: "are you sure?",
        text: "When deleted, you will not be able to restore the information!",
        icon: "warning",
        dangerMode: true,
        buttons: ["cancel","Yes delete!"],
        showLoaderOnConfirm: true,


    })
        .then((willDelete) =>
        {
            if (willDelete)
            {
                swal({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    buttons: false,
                    onOpen: () => {
                        swal.showLoading()
                    }
                })

                $.ajax
                ({
                    url : "/deleteHardware" ,
                    type : "POST",
                    data : {id:$id},
                    success: function()
                    {
                        swal("You have successfully deleted",
                            {
                                icon: "success",
                            }).then(function ()
                        {
                            window.location.reload();
                        });
                    }
                })
            }

            else
            {

                swal("cancel", "", "error");
            }
        })
}
