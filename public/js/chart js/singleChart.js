$(document).ready(function () {
    var data = $('#data').val();
    var prefix = $('#prefix').val();
    console.log(data)
    initialSingle('chartTemperature', jQuery.parseJSON(data),prefix);
});


function initialSingle(id, data,prefix) {

    $Container = document.getElementById(id);
    createSingleChart($Container, data,prefix);
}


function createSingleChart(container, data , prefix) {

    //var data =  json_encode($data->toArray()) ;
    //
    //console.log(data);

    console.log(container);
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_dataviz);
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create(container, am4charts.XYChart);

        // Add data
        chart.data = [];
        for (let i = 0; i < data.length; i++) {
            chart.data.push({
                "date": new Date(data[i].date_time),
                "value": [data[i].value]
            });

            chart.numberFormatter.numberFormat = "#a";
            chart.numberFormatter.bigNumberPrefixes = [
                { "number": 1e+0, "suffix": prefix }
            ];
        }
        // Set input format for the dates
        // chart.dateFormatter.inputDateFormat = "yyyy-MM-dd hh:mm:ss";

        // Create axes
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.tooltipDateFormat = "yyyy-MM-dd hh:mm:ss";


        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        dateAxis.baseInterval = {
            "timeUnit": "second",
            "count": 1
        };
        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "value";
        series.dataFields.dateX = "date";
        series.tooltipText = "{value}";
        series.strokeWidth = 2;
        series.minBulletDistance = 5;

        // Drop-shaped tooltips
        series.tooltip.background.cornerRadius = 20;
        series.tooltip.background.strokeOpacity = 0;
        series.tooltip.pointerOrientation = "vertical";
        series.tooltip.label.minWidth = 40;
        series.tooltip.label.minHeight = 40;
        series.tooltip.label.textAlign = "middle";
        series.tooltip.label.textValign = "middle";

        // Make bullets grow on hover
        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.circle.strokeWidth = 2;
        bullet.circle.radius = 4;
        bullet.circle.fill = am4core.color("#2900ff");

        var bullethover = bullet.states.create("hover");
        bullethover.properties.scale = 0.3;

        // Make a panning cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.behavior = "panXY";
        chart.cursor.xAxis = dateAxis;
        chart.cursor.snapToSeries = series;

        // Create vertical scrollbar and place it before the value axis
        chart.scrollbarY = new am4core.Scrollbar();
        chart.scrollbarY.parent = chart.leftAxesContainer;
        chart.scrollbarY.toBack();

        // Create a horizontal scrollbar with previe and place it underneath the date axis
        chart.scrollbarX = new am4charts.XYChartScrollbar();
        chart.scrollbarX.series.push(series);
        chart.scrollbarX.parent = chart.bottomAxesContainer;

        dateAxis.start = 0.79;
        dateAxis.keepSelection = true;


    }); // end am4core.ready()
}

function refresh() {

    var date = $('#reservationtime').val();
    var sku = $('#sku').val();
    var prefix = $('#prefix').val();
    var type = $('#type').val();
    console.log(date);
    console.log(sku);
    console.log(prefix);
    console.log(type);
    $.ajax({
        method: 'GET',
        url: '/api/testing14',
        dataType: 'json',
        data: {date: date, sku: sku,prefix:prefix,type:type},
    }).done((json) => {
        console.log('done');
        console.log(json.data);
        console.log(json.prefix);
        createSingleChart('chartTemperature', json.data , json.prefix);
    }).fail((json) => {
        console.log('fail');
    });
}
