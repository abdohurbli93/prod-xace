function generateKeyArray(date, keys, values) {
    var cleanArray = {};
    cleanArray['date'] = date;
    cleanArray[keys] = values;
    return cleanArray;
}



function createSimChart(data,chartName) {
    am4core.ready(function () {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        console.log(chartName)
        var chart = am4core.create(chartName, am4charts.XYChart);

        //

        // Increase contrast by taking evey second color
        chart.colors.step = 2;

        // Add data
        chart.data = generateChartData();
        console.log(chart.data)

        // Create axes
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.tooltipDateFormat = "yyyy-MM-dd hh:mm:ss";
        dateAxis.baseInterval = {
            "timeUnit": "second",
            "count": 1
        };

        // Add prefixes
        chart.numberFormatter.numberFormat = "#a";
        chart.numberFormatter.bigNumberPrefixes = [
            { "number": 1e+0, "suffix": 'MB' }
        ];


        // Create series
        function createAxisAndSeries(field, name, opposite, bullet) {
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            if (chart.yAxes.indexOf(valueAxis) != 0) {
                valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
            }

            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = field;
            series.dataFields.dateX = "date";
            series.strokeWidth = 2;
            series.yAxis = valueAxis;
            series.name = name;
            series.tooltipText = "{name}: [bold]{valueY}[/]";
            series.tensionX = 0.8;
            series.showOnInit = true;

            var interfaceColors = new am4core.InterfaceColorSet();

            switch (bullet) {
                case "triangle":
                    var bullet = series.bullets.push(new am4charts.Bullet());
                    bullet.width = 12;
                    bullet.height = 12;
                    bullet.horizontalCenter = "middle";
                    bullet.verticalCenter = "middle";

                    var triangle = bullet.createChild(am4core.Triangle);
                    triangle.stroke = interfaceColors.getFor("background");
                    triangle.strokeWidth = 2;
                    triangle.direction = "top";
                    triangle.width = 12;
                    triangle.height = 12;
                    break;
                case "rectangle":
                    var bullet = series.bullets.push(new am4charts.Bullet());
                    bullet.width = 10;
                    bullet.height = 10;
                    bullet.horizontalCenter = "middle";
                    bullet.verticalCenter = "middle";

                    var rectangle = bullet.createChild(am4core.Rectangle);
                    rectangle.stroke = interfaceColors.getFor("background");
                    rectangle.strokeWidth = 2;
                    rectangle.width = 10;
                    rectangle.height = 10;
                    break;
                default:
                    var bullet = series.bullets.push(new am4charts.CircleBullet());
                    bullet.circle.stroke = interfaceColors.getFor("background");
                    bullet.circle.strokeWidth = 2;
                    break;
            }

            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
        }
        for (let i = 0; i < data.length; i++) {
            createAxisAndSeries('' + data[i][0].sku, '' + data[i][0].sku, true, "circle");
        }

        // Add legend
        chart.legend = new am4charts.Legend();

        // Add cursor
        chart.cursor = new am4charts.XYCursor();

        // generate some random data, quite different range
        function generateChartData() {
            var chartData = [];
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].length; j++) {
                    // we create date objects here. In your data, you can have date strings
                    // and then set format of your dates using chart.dataDateFormat property,
                    // however when possible, use date objects, as this will speed up chart rendering.
                    var newDate = data[i][j].date_time;
                    var value = data[i][j].value;
                    var name = data[i][0].sku;
                    chartData.push(generateKeyArray(newDate, name, value));

                }
            }
            return chartData;
        }

    });
    // end am4core.ready()

}

function refreshSim() {

    var date = $('#reservationSimTime').val();
    var inputSku=$('#input_sku').val();

    console.log(date);
    console.log(inputSku);
    $.ajax({
        method: 'GET',
        url: '/getMultiSimDataJson',
        dataType: 'json',
        data: { date: date, inputSku: inputSku},
    }).done((json) => {
        console.log('done');
        console.log(json.currentData)
        console.log(json.consumptionData)
        createSimChart(json.currentData,'chartDivMultiSim')
        createSimChart(json.consumptionData,'chartDivmultiSimConsumption')

    }).fail((json) => {
        console.log('fail');
        swal({
            position: "center",
            type: "error",
            title: "You Aren't Owner Of Inserted SKU",
            showConfirmButton: false,
            timer: 2500
        });
    });
}
function refreshAllSim() {

    var date = $('#reservationSimTime').val();
    var fromSku=$('#skuFromSim').val();
    var toSku=$('#skuToSim').val();

    console.log(date);
    $.ajax({
        method: 'GET',
        url: '/getAllSimDataJson',
        dataType: 'json',
        data: { date: date, from_sku: fromSku,to_sku:toSku},
    }).done((json) => {
        console.log('done');
        console.log(json.allSimData);
        console.log(json.allSimConsumption);
        createChart('chartSim',json.allSimData,'MB')
        createChart('chartSimConsumption',json.allSimConsumption,'MB')

    }).fail((json) => {
        console.log('fail');
    });
}

setInterval(function simAutoRefresh() {
    refreshAllSim();
}, 300000);

