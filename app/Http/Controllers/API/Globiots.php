<?php

namespace App\Http\Controllers\API;

use App\Classes\Curl;
use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use App\Vendors\HardwareData;
use App\Vendors\VendorCredentials;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Globiots extends Controller
{
    private $curl = null;
    function __construct($userName=null,$password=null,$url=null)
    {

        if(empty($userName)||empty($password)||empty($url))
        {
            $vendor = VendorCredentials::getVendorCredentialById(1);
            $headerJson = json_decode($vendor['header_json'], true);
            $url = $vendor->base_url;
            $userName = $headerJson['username'];
            $password = $headerJson['password'];
        }
        $this->curl = new Curl($userName . ":" . $password, $url);
    }
    ///////////////////////////////devices Section////////////////////////////////////
    /**
     * get All devices
     * Methods GET
     * @return array of devices.
     */
    public function retrieveAllDevices()
    {
        $response = $this->curl->callCurl(
            "/rest/api/public/v2/devices",
            null,
            true,
            false
        );
        return $response;
    }

    /**
     * @param request contains params
     * @return object device object
     */
    public function retrieveDeviceByHostName(Request $request)
    {
        $hostIp = $request->get('ip');
        if (empty($hostIp))
            return HttpHelper::errorMissingParameter('missing required param');
        $response =  $this->curl->callCurl(
            "/rest/api/public/v2/devices/host-name/" . $hostIp,
            null,
            true,
            false
        );
        return $response;
    }


    /**
     * retrieve data by serial number.
     * @param request contains all required param
     * @return object device object
     */
    public function retrieveDeviceBySn(Request $request)
    {
        $sn = $request->get('sn');
        if (empty($sn))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/devices/serial-number/' . $sn,
            null,
            true,
            false
        );
        return $response;
    }

    /**
     * retrieve data by device status
     * @param Request $request contain all required param
     * @return object devices contain device objects
     */
    public function retrieveDeviceByStatus(Request $request)
    {
        $status = $request->get('status');
        if (empty($status))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/devices/status/' . $status,
            null,
            true,
            false
        );
        return $response;
    }

    /////////////////////////////////parameters Section/////////////////////////
    /**
     * Retrieve parameter information by 'logged' field
     * @param Request $request
     * @return 'parameters object
     */
    public function retrieveAllParameterByHostName(Request $request)
    {
        $hostIp = $request->get('ip');
        if (empty($hostIp))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/parameters/host-name/' . $hostIp,
            null,
            true,
            false
        );
        return $response;
    }

    /**
     * Retrieve parameter information by the 'logged' and 'address' field.
     * @param Request $request contain all required param
     * @return 'parameters object
     */
    public function retrieveParametersByHostNameAndAddress(Request $request)
    {
        $hostIp = $request->get('ip');
        $address = $request->get('address');
        if (empty($address) || empty($hostIp))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/parameters/host-name/' . $hostIp . '/address/' . $address,
            null,
            true,
            false
        );
        return $response;
    }

    /**
     * Retrieve parameter information by 'logged' field
     * @param Request $request
     * @return 'parameters object
     */
    public function retrieveParametersByHostNameAndLoggedStatus(Request $request)
    {
        $hostIp = $request->get('ip');
        $status = $request->get('status');
        if (empty($status) || empty($hostIp))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/parameters/host-name/' . $hostIp . '/logged/' . $status,
            null,
            true,
            false
        );
        return $response;
    }

    /////////////////////////////////////////////alarm section////////////////////////////////////
    /**
     * Retrieve all alarm configuration
     * @param Request $request contain all needed parameter
     * @return objact alarm config
     */
    public function retrieveAllAlarmConfigurationByParameterId(Request $request)
    {
        $parameterId = $request->get('parameter_id');
        if (empty($parameterId))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/alarms/parameter-id/' . $parameterId,
            null,
            true,
            false
        );
        return $response;
    }

    ////////////////////////////////////////////event section//////////////////////////////////////
    /**
     * Retrieve all event configuration
     * @param Request $request contain all needed parameter
     * @return objact event config
     */
    public function retrieveAllEventConfigurationByHostName(Request $request)
    {
        $hostIp = $request->get('ip');
        if (empty($hostIp))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/events/host-name/' . $hostIp,
            null,
            true,
            false
        );
        return $response;
    }

    ////////////////////////////////////////////////memory map section//////////////////////////////////
    /**
     * Retrieve all address on memory map
     * @param Request $request contain all needed parameter
     * @return  objacts of  address objects
     */
    public function retrieveAllAddressOnMemoryMapByHostName(Request $request)
    {
        $hostIp = $request->get('ip');
        if (empty($hostIp))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/memmap/host-name/' . $hostIp,
            null,
            true,
            false
        );
        return $response;
    }


    ///////////////////////////////////////////////////log Data section///////////////////////////////////
    /**
     * Retrieve log data by multiple address
     * @param Request $request contain all needed parameter
     * @return  objact contains objects data
     */
    public function retrieveLogDataByHostNameAndAddress(Request $request)
    {
        $hostIp = $request->get('ip');
        $address = $request->get('address');
        if (empty($hostIp) || empty($address))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/data-log/last-value?hostname=' . $hostIp . '&address=' . $address,
            null,
            true,
            false
        );

        return $response;
    }
    /**
     * Retrieve log data by multiple address (post)
     * @param Request $request contain all needed parameter
     * @return  data objects
     */
    public function retrieveLogDataByMultipleAddress(Request $request)
    {
        $hostIp = $request->get('ip');
        $address = $request->get('address');
        if (empty($address))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/data-log/last-value?hostname=' . $hostIp,
            $address,
            true,
            true
        );

        return $response;
    }


    /**
     * Retrieve log data by date and address and host name
     * @param Request $request contain all needed parameter
     * @return  data objects
     */
    public function retrieveLogDataByHostNameAndAddressAndDate(Request $request)
    {
        $hostIp = $request->get('ip');
        $address = $request->get('address');
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        if (empty($hostIp) || empty($address) || empty($fromDate) || empty($toDate))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/data-log?hostname=' . $hostIp . '&address=' . $address .
                '&fromDate=' . $fromDate . '&toDate=' . $toDate,
            null,
            true,
            false
        );
        $response = json_decode($response, true);
        if ($response['responseCode'] == 200) {
            $data = $response['data'];
            foreach ($data as $res) {
                $result['temperature'] = $res[1];
                $result['date_time'] = Carbon::createFromTimestampMs($res[0])->toDateTimeString();;
                $result['hardware_id'] = 1;
                // HardwareData::addResult($result);
            }
        }
        return $response;
    }
    /**
     * Retrieve log data by date and address and host name
     * @param Request $request contain all needed parameter
     * @return  data objects
     */
    public function getData($hostIp, $address, $fromDate, $toDate)
    {
        if (empty($hostIp) || empty($address) || empty($fromDate) || empty($toDate))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/data-log?hostname=' . $hostIp . '&address=' . $address .
                '&fromDate=' . $fromDate . '&toDate=' . $toDate,
            null,
            true,
            false
        );
        $response = json_decode($response, true);
        if ($response['responseCode'] == 200) {
            $data = $response['data'];
            $cleanResponse = [];
            foreach ($data as $res) {
                $cleanResponse[] = ['value' => $res[1], 'date_time' => Carbon::createFromTimestampMs($res[0])->toDateTimeString()];
            }
        } else {
            return 'Data not found!';
        }
        return $cleanResponse;
    }

    /**
     * Retrieve log data by date and address and host name
     * @param Request $request contain all needed parameter
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrieveLogDataByHostNameAndAddressAndDateJson(Request $request)
    {
        $hostIp = $request->get('ip');
        $address = $request->get('address');
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        $fromDate = Carbon::now()->subSeconds(300)->format('YmdHi00');
        $toDate = Carbon::now()->format('YmdHi00');
        if (empty($hostIp) || empty($address) || empty($fromDate) || empty($toDate))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/data-log?hostname=' . $hostIp . '&address=' . $address .
                '&fromDate=' . $fromDate . '&toDate=' . $toDate,
            null,
            true,
            false
        );
        $success = 0;
        $response = json_decode($response, true);
        if ($response['responseCode'] == 200) {
            // $data = $response['data'];
            // foreach ($data as $res) {
            //     $result['value'] = $res[1];
            //     $result['date_time'] = Carbon::createFromTimestampMs($res[0])->toDateTimeString();
            //     $result['hardware_id'] = 1;
            //     Result::addResult($result);
            // }
            $success = 1;
        }
        if ($success)
            return response()->json($success, 200);
        else  return response()->json($success, 404);
    }


    /**
     * Retrieve log data by date and multiple address and host name (post)
     * @param Request $request contain all needed parameter
     * @return  datas objects
     */
    public function retrieveLogDataByMultipleAddressAndDate(Request $request)
    {
        $hostIp = $request->get('ip');
        $address = $request->get('address');
        $toDate = $request->get('to_date');
        $fromDate = $request->get('from_date');
        //        $fromDate=Carbon::now()->subSeconds(300)->format('YmdHi00');
        //        $toDate=Carbon::now()->format('YmdHi00');
        if (empty($address) || empty($hostIp) || empty($fromDate) || empty($toDate))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/data-log?hostname=' . $hostIp .
                '&fromDate=' . $fromDate . '&toDate=' . $toDate,
            $address,
            true,
            true
        );

        return $response;
    }

    /////////////////////////////////////////alarm log data////////////////////////////////////
    /**
     * Retrieve all data of alarm summery
     * @param Request $request contain all needed parameter
     * @return  json
     */
    public function retrieveAllDataOfAlarmSummery(Request $request)
    {
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/alarm-log/summary',
            null,
            true,
            false
        );
        return $response;
    }
    /**
     * Retrieve all log data of alarm summery by hostname
     * @param Request $request contain all needed parameter
     * @return  arry of alarm log
     */
    public function retrieveAlarmSummeryByHostname(Request $request)
    {
        $hostIp = $request->get('ip');
        if (empty($hostIp))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/alarm-log/summary/host-name/' . $hostIp,
            null,
            true,
            false
        );
        return $response;
    }

    /**
     * Retrieve all log data of alarm detail by hostname
     * @param Request $request contain all needed parameter
     * @return  array of alarm log
     */
    public function retrieveAlarmDetailByHostname(Request $request)
    {
        $hostIp = $request->get('ip');
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        if (empty($hostIp) || empty($fromDate) || empty($toDate))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/alarm-log/detail/host-name/'
                . $hostIp . '?fromDate=' . $fromDate . '&toDate=' . $toDate,
            null,
            true,
            false
        );
        return $response;
    }

    /**
     * Retrieve all data of alarm history
     * @param Request $request contain all needed parameter
     * @return  array of alaram log
     */
    public function retrieveAlarmHistoryByHostName(Request $request)
    {
        $hostIp = $request->get('ip');
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        if (empty($hostIp) || empty($fromDate) || empty($toDate))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/alarm-log/history/host-name/' . $hostIp . '?fromDate='
                . $fromDate . '&toDate=' . $toDate,
            null,
            true,
            false
        );
        return $response;
    }


    /////////////////////////////////////////////////////////////event log data/////////////////////////////////////
    /**
     * Retrieve all event history
     * @param Request $request contain all needed parameter
     * @return  array of event log
     */
    public function retrieveEventHistoryByHostName(Request $request)
    {
        $hostIp = $request->get('ip');
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        if (empty($hostIp) || empty($fromDate) || empty($toDate))
            return HttpHelper::errorMissingParameter('missing required param');
        $response = $this->curl->callCurl(
            '/rest/api/public/v2/event-log/history/host-name/' . $hostIp . '?fromDate='
                . $fromDate . '&toDate=' . $toDate,
            null,
            true,
            false
        );
        return $response;
    }

    //testing


    public function getHeaderJson()
    {
        $getHeaderJson = VendorCredentials::get();
        $header = json_decode($getHeaderJson['header_json']);
        $url = $header->url;
        $appKey = $header->username;
        $secretKey = $header->password;
    }
    public function savingNewData($hardware)
    {
        $fromDate = Carbon::now()->subHours(8)->subSeconds(300)->format('YmdHi00');
        $toDate = Carbon::now()->subHours(8)->format('YmdHi00');

        $response = $this->getData($hardware->hostname,
            $hardware->temperature_address, $fromDate, $toDate);

        $response1 = $this->getData($hardware->hostname,
            $hardware->humidity_address, $fromDate, $toDate);

        $response2 = $this->getData($hardware->hostname,
            $hardware->battery_address, $fromDate, $toDate);
        if(empty($response[0]['value'])&&empty($response1[0]['value'])&&empty($response2[0]['value']))
            return ;
        HardwareData::addResult([
            'date_time' =>Carbon::now(), 'hardware_id' => $hardware->id,
            'temperature' => !empty($response[0]['value']) ? $response[0]['value'] : null,
            'humidity' => !empty($response1[0]['value']) ? $response1[0]['value'] : null,
            'battery' => !empty($response2[0]['value']) ? $response2[0]['value'] : null
        ]);
    }
}
