<?php

namespace App\Http\Controllers\API;

use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use App\User;
use App\Vendors\ClientHardware;
use App\Vendors\Hardware;
use App\Vendors\HardwareData;
use App\Vendors\VendorCredentials;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use SoapClient;

class ClientApiController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth:api', 'client'])->except('login');
    }
    public function getDeviceList()
    {
        $userId = auth()->id();
        $hardware = ClientHardware::getClientHardware($userId);
        return HttpHelper::select($hardware);
    }

    public function getTemperatureByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::now()->subHours(8)->subSeconds(300)->format('YmdHi00');
        $toDate = Carbon::now()->subHours(8)->format('YmdHi00');
        if (!empty($hardware)) {
            $globiots = new Globiots();
            $response = $globiots->getData($hardware->hostname, $hardware->temperature_address, $fromDate, $toDate);
            return HttpHelper::select($response);
        }
        return HttpHelper::errorNotAllowed('access denied');
    }
    public function getHumidityByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::now()->subHours(8)->subSeconds(300)->format('YmdHi00');
        $toDate = Carbon::now()->subHours(8)->format('YmdHi00');
        if (!empty($hardware)) {
            $globiots = new Globiots();
            $response = $globiots->getData($hardware->hostname, $hardware->humidity_address, $fromDate, $toDate);
            return HttpHelper::select($response);
        }
        return HttpHelper::errorNotAllowed('access denied');
    }

    public function getBatteryByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::now()->subHours(8)->subSeconds(300)->format('YmdHi00');
        $toDate = Carbon::now()->subHours(8)->format('YmdHi00');
        if (!empty($hardware)) {
            $globiots = new Globiots();
            $response = $globiots->getData($hardware->hostname, $hardware->battery_address, $fromDate, $toDate);
            return HttpHelper::select($response);
        }
        return HttpHelper::errorNotAllowed('access denied');
    }

    public function getSimDataByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::now()->subHours(8)->subSeconds(300)->format('YmdHi00');
        $toDate = Carbon::now()->subHours(8)->format('YmdHi00');
        if (!empty($hardware)) {
            $flexiRoam = new FlexiRoam($hardware->vendor_id);
            $flexiData = $flexiRoam->getClientBalance($hardware->imsi);
            if (empty($flexiData))
                return HttpHelper::isEmpty('data not found');
            $flexiData = get_object_vars($flexiData);
            $megaBalance = $flexiData['return']->bucketInfos->bytesAvailable / (1024 * 1024);
            $data['value'] = $megaBalance;
            $data['date_time'] = Carbon::now()->timestamp;
            return HttpHelper::select($data);
        }

        return HttpHelper::errorNotAllowed('access denied');
    }



    public function getTemperatureHistoryByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardware)) {
            $response = HardwareData::getTemperatureHistoryByHardwareId($hardware->hardware_id, $fromDate, $toDate);
            return HttpHelper::select($response);
        }
        return HttpHelper::errorNotAllowed('access denised');
    }
    public function getHumidityHistoryByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardware)) {
            $response = HardwareData::getHumidityHistoryByHardwareId($hardware->hardware_id, $fromDate, $toDate);
            return HttpHelper::select($response);
        }
        return HttpHelper::errorNotAllowed('access denised');
    }

    public function getBatteryHistoryByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardware)) {
            $response = HardwareData::getBatteryHistoryByHardwareId($hardware->hardware_id, $fromDate, $toDate);
            return HttpHelper::select($response);
        }
        return HttpHelper::errorNotAllowed('access denised');
    }
    public function getSimHistoryByHardwareId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardware = Hardware::getHardwareById($request->get('hardware_id'), Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardware)) {
            $response = HardwareData::getSimHistoryByHardwareId($hardware->hardware_id, $fromDate, $toDate);
            return HttpHelper::select($response);
        }
        return HttpHelper::errorNotAllowed('access denised');
    }
    public function getAllDevicesTemperatureHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        $hardwareIds = Hardware::getAllHardware(Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denised');
    }
    public function getAllHumidityDevicesHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        $hardwareIds = Hardware::getAllHardware(Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denised');
    }
    public function getAllBatteryDevicesHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        $hardwareIds = Hardware::getAllHardware(Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllBatteryHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denised');
    }
    public function getAllSimDevicesHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        $hardwareIds = Hardware::getAllHardware(Auth::id());
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllSimDataUsageHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denised');
    }
    public function getTemperatureFromSkuRang(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_sku' => 'required|integer',
            'to_sku' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $request->get('from_sku'), $request->get('to_sku'));
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denised');
    }

    public function getHumidityFromSkuRang(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_sku' => 'required|integer',
            'to_sku' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $request->get('from_sku'), $request->get('to_sku'));
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denised');
    }

    public function getBatteryFromSkuRang(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_sku' => 'required|integer',
            'to_sku' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $request->get('from_sku'), $request->get('to_sku'));
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllBatteryHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denied');
    }
    public function getSimDataFromSkuRang(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_sku' => 'required|integer',
            'to_sku' => 'required|integer',
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $request->get('from_sku'), $request->get('to_sku'));
        $fromDate = Carbon::make($request->get('from_date'));
        $toDate = Carbon::make($request->get('to_date'));
        if (!empty($hardwareIds)) {
            $response = HardwareData::getAllSimDataUsageHistory($hardwareIds, $fromDate, $toDate);
            return HttpHelper::select(json_decode($response, true));
        }
        return HttpHelper::errorNotAllowed('access denied');
    }

    public function getClientBalance(Request $request)
    {
        return HttpHelper::select(HardwareData::getAllClientSimBalance(Auth::id()));
    }

    public function getClientBalanceHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d H:i:s',
            'to_date' => 'required|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        $response = HardwareData::getAllClientSimBalanceHistory(
            Auth::id(),
            $request->get('from_date'),
            $request->get('to_date')
        );
        return HttpHelper::select($response);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('Missing required params..');
        $user = User::login($request->get('username'), $request->get('password'));
        if (empty($user))
            return HttpHelper::notAuthorized('Invalid Login or password');
        return HttpHelper::select($user);
    }

    public function test()
    {
        $options = array(
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'login' => "frxtest@JTM2M_GB",
            'password' => "FL3x1."
            //'login' => "LiC@JTM2M_GB",
            //'password'=> "Cr@b55"
        );

        $client = new SoapClient('https://api.jtm2m.com/nomad-ws/m2mservice?wsdl', $options);

        var_dump($client->__getFunctions());
        exit;
    }
}
