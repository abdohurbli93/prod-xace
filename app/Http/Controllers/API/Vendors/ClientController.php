<?php

namespace App\Http\Controllers\API\Vendors;

use App\Vendors\ClientBalance;
use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use App\User;
use App\Vendors\ClientHardware;
use App\Vendors\Clients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{

    public function __construct()
    {
        return $this->middleware(['auth:api','admin']);
    }
    public function addClient(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_name' => 'required',
            'email' => 'required|unique:users',
            'phone_number' => 'required',
            'address' => 'required',
            'username' => 'required',
            'password' => 'required',
            'client_type' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        if (!empty(User::getUserByUserName($request->get('username'))))
            return HttpHelper::errorAlreadyExists('username already exist');
            if($request->get('client_type')=='test')
            {
                $validator = Validator::make($request->all(), [
                    'client_balance' => 'required|numeric',
                ]);
                if ($validator->fails())
                    return HttpHelper::errorMissingParameter('missing param');
            }
        $client = $request->except('client_name');
        $user = User::addUser($client);
        $client = Clients::addClient(['user_id' => $user->id, 'client_name' => $request->get('client_name')]);
        if(!empty($client->client_balance))
        {
            $newClientBalance['current_balance']=$client->client_balance;
            $newClientBalance['user_id']=$user->id;
            ClientBalance::addNewBalance($newClientBalance);
        }
        $client_token_api = $user->createToken('flexi')->accessToken;
        if (!empty($client))
            return HttpHelper::insert(['token_api' => $client_token_api]);
        return HttpHelper::generalError('insert fail');
    }

    public function UpdateClient(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        if (!empty($request->get('username')))
            if (!empty(User::getUserByUserName($request->get('username'))))
                return HttpHelper::errorAlreadyExists('username already exist');
        $client = $request->except('user_id', 'client_name');
        if (!empty($request->get('password')))
            $client['password'] = Hash::make($client['password']);
        $clientId = $request->get('user_id');
        if (empty(User::getUserById($clientId)))
            return HttpHelper::isEmpty('client not found');
        $updatedClientStatus = User::updateUser($client, $clientId);
        if (!empty($request->get('client_name')))
            $updatedClientStatus = Clients::updateClient(['client_name' => $request->get('client_name')], $clientId);
        $client = User::getUserById($clientId);
        $client_token_api = $client->createToken('flexi')->accessToken;
        if (!empty($updatedClientStatus))
            return HttpHelper::update(['token_api' => $client_token_api]);
        return HttpHelper::generalError('update fail');
    }

    public function deleteClient(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if (empty(User::getUserById($request->get('user_id'))))
            return HttpHelper::isEmpty('client not found');
        Clients::deleteClientByUserId($request->get('user_id'));
        return HttpHelper::delete(User::deleteUser($request->get('user_id')));
    }
    public function addClientHardware(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|numeric',
            'user_id' => 'required|numeric'
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        $addHardware  =  ClientHardware::addClientHardware(['hardware_id' => $request->get('hardware_id'), 'user_id' => $request->get('user_id')]);
        if ($addHardware)
            return HttpHelper::insert('Added Successfully!');
        else
            return HttpHelper::generalError('Internal server Error ...');
    }
}
