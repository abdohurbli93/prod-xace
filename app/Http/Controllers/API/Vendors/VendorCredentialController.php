<?php

namespace App\Http\Controllers\API\Vendors;

use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use App\Vendors\VendorCredentials;
use App\Vendors\Vendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VendorCredentialController extends Controller
{

    public function __construct()
    {
        return $this->middleware(['auth:api','admin']);
    }
    public function addVendorCredential(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required|numeric',
            'header_json' => 'required',
            'base_url' => 'required',
            'type' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');
        if (empty(Vendors::getVendorByVendorId($request->get('vendor_id'))))
            return HttpHelper::isEmpty('vendor not found');

        $vendorCredential=$request->all();
        $vendorCredential['header_json']=json_encode($vendorCredential['header_json'],true);
        $vendorCredential=VendorCredentials::addVendorCredential($vendorCredential);
        if(!empty($vendorCredential))
        return HttpHelper::insert(['vendor_credential_id'=>$vendorCredential->id]);
        return HttpHelper::generalError('insert fail');
    }

    public function updateVendorCredential(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_credential_id' => 'required|numeric',
            'vendor_id' => 'required|numeric',
            'header_json' => 'required',
            'base_url' => 'required',
            'type' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if (empty(VendorCredentials::getVendorCredentialById($request->get('vendor_credential_id'))))
            return HttpHelper::isEmpty('vendor credential not found');

        if (empty(Vendors::getVendorByVendorId($request->get('vendor_id'))))
            return HttpHelper::isEmpty('vendor not found');

        $vendorCredential = $request->only('vendor_id', 'header_json', 'base_url', 'type');
        $vendorCredentialId = $request->get('vendor_credential_id');
        $vendorCredential['header_json'] = json_encode($vendorCredential['header_json'], true);

        if (!empty(VendorCredentials::updateVendorCredential($vendorCredential, $vendorCredentialId)))
            return HttpHelper::update(['vendor_credential_id' => $vendorCredentialId]);
        return HttpHelper::generalError('update fail');
    }
    public function deleteVendorCredential(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_credential_id' => 'required|numeric',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if (empty(VendorCredentials::getVendorCredentialById($request->get('vendor_credential_id'))))
            return HttpHelper::isEmpty('vendor credential not found');

        return HttpHelper::delete(VendorCredentials::deleteVendorCredential($request->get('vendor_credential_id')));
    }
}
