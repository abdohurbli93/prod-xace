<?php

namespace App\Http\Controllers\API\Vendors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HardwareDataController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth:api','admin']);
    }
}
