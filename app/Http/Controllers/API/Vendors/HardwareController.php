<?php

namespace App\Http\Controllers\API\Vendors;

use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use App\Vendors\Hardware;
use App\Vendors\VendorCredentials;
use App\Vendors\Vendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HardwareController extends Controller
{

    public function __construct()
    {
        return $this->middleware(['auth:api','admin']);
    }
    public function addHardware(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required|numeric',
            'device_name' => 'required',
            'hardware_type' => 'required',
            'sku' => 'required|unique:hardware',
            'status' => 'required',
            'sn' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if ($request->get('hardware_type') == 'sensor') {
            $validator = Validator::make($request->all(), [
                'hostname' => 'required',
                'temperature_address' => 'required',
                'humidity_address' => 'required',
                'battery_address' => 'required',
            ]);
        }

            if ($request->get('hardware_type') == 'sim') {
                $validator = Validator::make($request->all(), [
                    'imsi' => 'required',
                    'msisdn' => 'required',
                    'iccid' => 'required',
                    'pin' => 'required',
                    'puk' => 'required',
                ]);
            }


            if ($validator->fails())
                return HttpHelper::errorMissingParameter('missing param');

        if (empty(Vendors::getVendorByVendorId($request->get('vendor_id'))))
            return HttpHelper::isEmpty('vendor not found');

        $hardware = $request->all();
        $hardware = Hardware::addHardware($hardware);
        if (!empty($hardware))
            return HttpHelper::insert(['devices_id' => $hardware->id]);
        return HttpHelper::generalError('insert fail');
    }

    public function updateHardware(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|numeric',
            'vendor_id' => 'required|numeric',
            'device_name' => 'required',
            'hardware_type' => 'required',
            'sku' => 'unique:hardware',
            'status' => 'required',
            'sn' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if ($request->get('hardware_type') == 'sensor') {
            $validator = Validator::make($request->all(), [
                'hostname' => 'required',
                'temperature_address' => 'required',
                'humidity_address' => 'required',
                'battery_address' => 'required',
            ]);
        }

            if ($request->get('hardware_type') == 'sim') {
                $validator = Validator::make($request->all(), [
                    'imsi' => 'required',
                    'msisdn' => 'required',
                    'iccid' => 'required',
                    'pin' => 'required',
                    'puk' => 'required',
                ]);
            }

            if ($validator->fails())
                return HttpHelper::errorMissingParameter('missing param');


        if (empty(Hardware::getHardwareById($request->get('hardware_id'))))
            return HttpHelper::isEmpty('device not found');

        if (empty(Vendors::getVendorByVendorId($request->get('vendor_id'))))
            return HttpHelper::isEmpty('vendor not found');

        $hardware = $request->except('hardware_id');
        $hardwareId = $request->get('hardware_id');
        if (!empty(Hardware::updateHardware($hardware, $hardwareId)))
            return HttpHelper::update(['device_id' => $hardwareId]);
        return HttpHelper::generalError('update fail');
    }
    public function deleteHardware(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_id' => 'required|numeric',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if (empty(Hardware::getHardwareById($request->get('hardware_id'))))
            return HttpHelper::isEmpty('device not found');

        return HttpHelper::delete(Hardware::deleteHardware($request->get('hardware_id')));
    }
}
