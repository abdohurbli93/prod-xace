<?php

namespace App\Http\Controllers\API\Vendors;

use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use App\Vendors\Vendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth:api','admin']);
    }
    public function addVendor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $vendor = $request->all();
        $vendor = Vendors::addVendor($vendor);
        if (!empty($vendor))
            return HttpHelper::insert(['vendor_id' => $vendor->id]);
        return HttpHelper::generalError('insert fail');
    }

    public function updateVendor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required|numeric',
            'vendor_name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if (empty(Vendors::getVendorByVendorId($request->get('vendor_id'))))
            return HttpHelper::isEmpty('vendor not found');

        $vendor = $request->only('vendor_name', 'email', 'phone_number', 'address');
        $vendorId = $request->get('vendor_id');
        if (!empty(Vendors::updateVendor($vendor, $vendorId)))
            return HttpHelper::update(['vendor_id' => $vendorId]);
        return HttpHelper::generalError('update fail');
    }
    public function deleteVendor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required|numeric',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if (empty(Vendors::getVendorByVendorId($request->get('vendor_id'))))
            return HttpHelper::isEmpty('vendor not found');

        return HttpHelper::delete(Vendors::deleteVendor($request->get('vendor_id')));
    }
}
