<?php

namespace App\Http\Controllers\API\Vendors;

use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use App\Vendors\Hardware;
use App\Vendors\HardwareType;
use App\Vendors\Vendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HardwareTypeController extends Controller
{

    public function __construct()
    {
        return $this->middleware(['auth:api','admin']);
    }

    public function addHardwareType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        $hardwareType=$request->all();
        $hardwareType=HardwareType::addHardwareType($hardwareType);
        if(!empty($hardwareType))
        return HttpHelper::insert(['device_type_id'=>$hardwareType->id]);
        return HttpHelper::generalError('insert fail');

    }

    public function updateHardwareType(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'hardware_type_id'=>'required|numeric',
            'name'=>'required',
        ]);
        if($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if(empty(HardwareType::getHardwareTypeById($request->get('hardware_type_id'))))
            return HttpHelper::isEmpty('device type not found');

        $hardwareType=$request->get('name');
        $hardwareTypeId=$request->get('hardware_type_id');
        if(!empty(HardwareType::updateHardwareType($hardwareType,$hardwareTypeId)))
        return HttpHelper::update(['hardware_type_id'=>$hardwareTypeId]);
        return HttpHelper::generalError('update fail');

    }
    public function deleteHardwareType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hardware_type_id' => 'required|numeric',
        ]);
        if ($validator->fails())
            return HttpHelper::errorMissingParameter('missing param');

        if(empty(HardwareType::getHardwareTypeById($request->get('hardware_type_id'))))
            return HttpHelper::isEmpty('device type not found');

        return HttpHelper::delete(HardwareType::deleteHardwareType($request->get('hardware_type_id')));
    }
}
