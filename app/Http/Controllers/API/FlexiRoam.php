<?php


namespace App\Http\Controllers\API;


use App\Helper\HttpHelper;
use App\Vendors\HardwareData;
use App\Vendors\VendorCredentials;
use Carbon\Carbon;
use mysql_xdevapi\Exception;
use SoapClient;

class FlexiRoam
{
    public $basUrl, $login, $password,$client;
    function __construct($vendorId)
    {
        $vendorCredential = VendorCredentials::getVendorCredentialByVendorId($vendorId);
        if (!empty($vendorCredential)) {
            $credentialHeader = json_decode($vendorCredential['header_json']);
            $this->basUrl = $vendorCredential->base_url;
            $this->login = $credentialHeader->username;
            $this->password = $credentialHeader->password;
            $options = [

                'soap_version' => SOAP_1_1,

                'exceptions' => true,

                'trace' => 1,

                'cache_wsdl' => WSDL_CACHE_NONE,

                'login' => $this->login,

                "password" => $this->password

            ];
            try {
                $this->client = new SoapClient($this->basUrl,  $options);
            } catch (\SoapFault $e) {
                $client=null;
            }
        }
    }

    public  function getClientBalance($imsi)
    {
        if (empty($this->basUrl) || empty($this->login) || empty($this->password)||empty($this->client))
            return  null;

        try {
            $result = $this->client->getPrepaidSimInfo(array('imsi' => $imsi));
        } catch (\SoapFault $e) {
            return null;
        }

        return $result;
    }

    public  function fillClientSim($imsi,$amount)
    {
        if (empty($this->basUrl) || empty($this->login) || empty($this->password) || empty($this->client))
            return  null;

        try {
            $result = $this->client->adjustSimsWithStatus(array('imsiList'=>$imsi,'refillAmount'=>0,
                'refillZones'=>'1-9','refillInZones'=>$amount));
        } catch (\SoapFault $e)
        {
            return null;
        }
        return 1;
    }
    public function savingNewData($hardware)
    {
        $flexiData = $this->getClientBalance($hardware->imsi);
        if (empty($flexiData))
            return;
        $flexiData = get_object_vars($flexiData);
        $megaBalance = $flexiData['return']->bucketInfos->bytesAvailable / (1024 * 1024);
        $latestDataUsage=HardwareData::getLatestHardwareData($hardware->id);
        if(!empty($megaBalance)&& !empty($latestDataUsage) && $latestDataUsage->data_usage  >=  $megaBalance )
        {
            $dataUsage=$latestDataUsage->data_usage;
            $dataConsumption= (double)$dataUsage -  (double)$megaBalance;
        }
        else $dataConsumption=0;
        HardwareData::addResult(['hardware_id' => $hardware->id,
            'date_time' => Carbon::now(), 'data_usage' => $megaBalance,'data_consumption'=>$dataConsumption]);
    }
}
