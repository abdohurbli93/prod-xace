<?php


namespace App\Http\Controllers\API;


use App\Curl;
use App\Helper\HttpHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SoapClient;

class JT extends Controller
{
    // use SoapClient;


    private $curl = null;
    function __construct()
    {
        $userName = '';
        $password = '';
        $this->curl = new Curl('frxtest@JTM2M_GB' . ":"
            . 'FL3x1.', "https://api.jtm2m.com/nomad-ws/m2mservice?wsdl");
    }
    public function getAccountStatements(Request $request)
    {
        $portfolioId = $request->get('portfolio_id');
        $startRow = $request->get('start_row');
        $endRow = $request->get('end_row');
        if (empty($portfolioId) || empty($startRow) || empty($endRow))
            return HttpHelper::errorMissingParameter('missing parameter');
        return  $this->curl->callCurl(
            'account/customerInfo?portfolioId=' . $portfolioId,
            [$startRow, $endRow],
            true,
            true
        );
    }
    public function getSimInfo(Request $request)
    {
        $imsi = $request->get('imsi');
        if (empty($imsi))
            return HttpHelper::errorMissingParameter('missing parameter');
        return  $this->curl->callCurl(
            '/sim?imsi=' . $imsi,
            null,
            true,
            false
        );
    }
    public function getPrepaidAccountInfo(Request $request)
    {
        $accountNumber = $request->get('account_number');
        if (empty($accountNumber))
            return HttpHelper::errorMissingParameter('missing parameter');
        return  $this->curl->callCurl(
            'account/accountInfo?portfolioId=' . $accountNumber,
            null,
            true,
            false
        );
    }


}
