<?php

namespace App\Http\Controllers;

use App\User;
use App\Vendors\ClientBalance;
use App\Vendors\ClientHardware;
use App\Vendors\Clients;
use App\Vendors\Hardware;
use App\Vendors\VendorCredentials;
use App\Vendors\Vendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use function GuzzleHttp\Promise\all;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    public function index()
    {
        return view('pages.admin_view');
    }


    public function addVendor(){
        return view('pages.vendor.addVendor');
    }

    public function addVendorModel(Request $request){
        if ($request['header_json'] == 'tokenShow'){
            $validate = Validator::make($request->all(), [
                'token' => 'required',
            ]);
            if ($validate->fails())
                return back()->withErrors($validate)->withInput();
        }elseif($request['header_json'] == 'authShow'){
            $validate = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required',
            ]);
            if ($validate->fails())
                return back()->withErrors($validate)->withInput();
        }


        $vendor=$request->except('token','username','password','header_json','base_url');
        $vendorCredential=$request->only('token','username','password','header_json','base_url');
        if($vendorCredential['header_json'] == 'authShow'){
            $vendorCredential['header_json']=json_encode(['username'=>$vendorCredential['username'],'password'=>$vendorCredential['password']]);
            $vendorCredential['type']= 'basic_auth';
        }elseif ($vendorCredential['header_json'] == 'tokenShow'){
            $vendorCredential['header_json'] =json_encode(['token'=>$vendorCredential['token']]);
            $vendorCredential['type']= 'token';
        }
        $vendor=Vendors::addVendor($vendor);
        if(empty($vendor))
            return back()->with('error', 'Sorry! there is an error ');
        $vendorCredential['vendor_id']=$vendor->id;
        $vendorCredential= \App\Vendors\VendorCredentials::addVendorCredential($vendorCredential);
        if(!empty($vendorCredential))
            return redirect('/addVendor')->with('success', 'Done successfully');
        if(empty($vendorCredential))
            return back()->with('error', 'Sorry! there is an error ')->withInput();
    }


    public function showVendor(){
        $vendors = Vendors::join('vendor_credentials','vendor_credentials.vendor_id','vendors.id')->get();
        return view('pages.vendor.showVendor', compact('vendors'));
    }

    public function editVendorShow($id){
        $editVendorShow = Vendors::where('vendors.id', $id)->join('vendor_credentials','vendor_credentials.vendor_id','vendors.id')->first();
       if($editVendorShow['type'] == 'basic_auth'){
         $vendorType = json_decode($editVendorShow['header_json'], true);
         $vendorType['token']='';
       }elseif($editVendorShow['type'] == 'token'){
           $vendorType = json_decode($editVendorShow['header_json'], true);
           $vendorType['username']= '';
           $vendorType['password']= '';
       }
        return view('pages.vendor.editVendor', compact(['editVendorShow' , 'vendorType']));
    }

    public function updateVendorModel(Request $request){
        $vendor=$request->except('_token','vendor_id','token','username','password','header_json','base_url','vendorCredential_id');
        $vendorCredential=$request->only('header_json','base_url');
        if($vendorCredential['header_json'] == 'authShow'){
            $vendorCredential['header_json']=json_encode(['username'=>$request['username'],'password'=>$request['password']]);
            $vendorCredential['type']= 'basic_auth';
        }elseif ($vendorCredential['header_json'] == 'tokenShow'){
            $vendorCredential['header_json'] =json_encode(['token'=>$request['token']]);
            $vendorCredential['type']= 'token';
        }
        $vendor=Vendors::updateVendor($vendor, $request['vendor_id']);
        if(empty($vendor))
            return back()->with('error', 'Sorry! there is an error ')->withInput();
        $vendorCredential= \App\Vendors\VendorCredentials::updateVendorCredential($vendorCredential ,$request['vendorCredential_id']);
        if(!empty($vendorCredential))
            return redirect('/showVendor')->with('success', 'Done successfully');
        if(empty($vendorCredential))
            return back()->with('error', 'Sorry! there is an error ')->withInput();
    }

    public function deleteVendor(Request $request){
        $id = $request->get('id');
        $deleteVendor = Vendors::deleteVendor($id);
        $deletedVendorCredential=VendorCredentials::where('user_id',$id)->delete();
        if ($deletedVendorCredential){
            echo "done";
        } else{
            echo "fail";
        }
    }

    public function showDetailVendor($id){
        $showDetailVendor = Vendors::join('vendor_credentials','vendor_credentials.vendor_id','vendors.id')
            ->where('vendors.id', $id)->first();
        $vendorType = json_decode($showDetailVendor['header_json'], true);
        return view('pages.vendor.showDetailVendor', compact(['showDetailVendor' ,'vendorType']));
    }

    public function addHardware(){
        $vendors = Vendors::get();
        return view('pages.hardware.addHardware' , compact('vendors'));
    }

    public function addHardwareModel(Request $request){
        if ($request['hardware_type'] == 'sim'){
            $validate = Validator::make($request->all(), [
                'sku' => 'required|unique:hardware',
                'sn' => 'required|unique:hardware',
                'imsi' => 'required|max:30',
                'msisdn' => 'required|max:30',
                'iccid' => 'required|max:30',
                'pin' => 'required|max:30',
                'puk' => 'required|max:30',
            ]);
            if ($validate->fails())
                return back()->withErrors($validate)->withInput();
        }elseif( $request['hardware_type'] == 'sensor'){
            $validate = Validator::make($request->all(), [
                'sku' => 'required|unique:hardware',
                'sn' => 'required|unique:hardware',
                'hostname' => 'required|max:30',
                'temperature_address' => 'required|max:30',
                'humidity_address' => 'required|max:30',
                'battery_address' => 'required|max:30',
            ]);
            if ($validate->fails())
                return back()->withErrors($validate)->withInput();
        }



        $hardware = $request->all();
        $hardware = Hardware::addHardware($hardware);
        if (!empty($hardware))
            return redirect('/showHardware')->with('success', 'Done successfully');
        return back()->with('error', 'Sorry! there is an error ')->withInput();
    }

    public function showHardware(){
        $hardwares = Hardware::get();
        $countSIMSold = Hardware::join('client_hardware', 'client_hardware.hardware_id', '=', 'hardware.id')->where('hardware.hardware_type', 'sim')->count();
        $countSensorSold =Hardware::join('client_hardware', 'client_hardware.hardware_id', '=', 'hardware.id')->where('hardware.hardware_type', 'sensor')->count();
        $countTotalSold = $countSIMSold +  $countSensorSold;

        $countSIM = Hardware::where('hardware.hardware_type', 'sim')->count();
        $countSensor = Hardware::where('hardware.hardware_type', 'sensor')->count();
        $countTotal = $countSIM +  $countSensor;

        return view('pages.hardware.showHardware', compact(['hardwares', 'countSIMSold', 'countSensorSold', 'countTotalSold' , 'countSIM' ,'countSensor' ,'countTotal']));
    }

    public function showDetailHardware($id){
        $showDetailHardware = Hardware::where('id', $id)->first();
        return view('pages.hardware.showDetailHardware', compact('showDetailHardware'));
    }

    public function editHardwareShow($id){
        $editHardwareShow = Hardware::where('hardware.id', $id)->join('vendors' , 'vendors.id' , 'hardware.vendor_id')
          ->select(['hardware.*' , 'vendors.vendor_name'])->first();
        $vendors = Vendors::get();
        return view('pages.hardware.editHardware', compact(['editHardwareShow' , 'vendors']));
    }

    public function updateHardwareModel(Request $request){
        $editHardware = $request->except('id','_token');
        $updateHardware =  Hardware::updateHardware($editHardware , $request['id'] );
        if(!empty($updateHardware))
            return redirect('/showHardware')->with('success', 'Done successfully');
        return back()->with('error', 'Sorry! there is an error ')->withInput();
    }

    public function deleteHardware(Request $request){
        $id = $request->get('id');
        $deleteHardware = Hardware::deleteHardware($id);
        if($deleteHardware) {
            echo "done";
        }else{
            echo "fail";
        }
    }


    public function addClient(){
        return view('pages.client.addClient');
    }

    public function addClientModel(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
         ]);
        if ($validator->fails())
            return back()->withErrors($validator)->withInput();

        $user=$request->except('client_name','client_balance','client_type');
        $user['password'] = Hash::make($user['password']);
        $client=$request->only('client_name','client_balance','client_type');
        $user = User::addUser($user);
        if (empty($user))
            return back()->with('error', 'Sorry! there is an error ')->withInput();
        $client['user_id']=$user->id;
        $client = Clients::addClient($client);
        if (!empty($client)) {
            if(!empty($client->client_balance))
            {
                $newClientBalance['current_balance']=$client->client_balance;
                $newClientBalance['user_id']=$user->id;
                ClientBalance::addNewBalance($newClientBalance);
            }
            return redirect('/addClient')->with('success', 'Done successfully');
        }
        return back()->with('error', 'Sorry! there is an error ')->withInput();
    }

    public function showClient(){
        $clients = Clients::join('users','users.id','clients.user_id')
            ->where('users.user_type' , 'client')
            ->select(['users.*','clients.id as client_id','clients.client_name as client_name'])
            ->get();
        foreach ($clients as $countSIM){
            $countSIM['countAllSim'] = ClientHardware::join('hardware', 'hardware.id', 'client_hardware.hardware_id')
                  ->where('client_hardware.user_id', $countSIM['id'])->where('hardware.hardware_type', 'sim')->count();
        }
        foreach ($clients as $countSensor){
            $countSensor['countAllSensor']= Hardware::join('client_hardware', 'client_hardware.hardware_id', '=', 'hardware.id')
                 ->where('client_hardware.user_id',  $countSensor->id)->where('hardware.hardware_type', 'sensor')->count();
        }

        return view('pages.client.showClient', compact('clients'));
    }

    public function editClientShow($id){
        $editClientShow = Clients::where('clients.user_id', $id)
                ->join('users','users.id','clients.user_id')
                ->select(['users.*','clients.id as client_id','clients.client_name as client_name'])
                ->first();
        return view('pages.client.editClient', compact('editClientShow'));
    }

    public function editClientModel(Request $request){
        $user=$request->except('_token','client_name','user_id', 'client_id');
        $client=$request->only('client_name');
        $user = User::updateUser($user , $request['user_id']);
        if(empty($user))
            return back()->with('error', 'Sorry! there is an error ')->withInput();
        $client = Clients::updateClient($client, $request['client_id']);
        if (!empty($client))
            return redirect('/showClient')->with('success', 'Done successfully');
        return back()->with('error', 'Sorry! there is an error ')->withInput();
    }

    public function deleteClient(Request $request){
        $id = $request->get('id');
        $deleteClient = Clients::deleteClientByUserId($id);
        if ($deleteClient){
            echo "done";
        } else{
            echo "fail";
        }
//        return response()->json('true',200);
    }

    public function showRangDevice(){
       $showRangDevice = Clients::getAllClientSkuRange();
        return view('pages.devices.rangDevice' , compact('showRangDevice'));
    }

    public function addRangeDevice(){
        $clients = Clients::getAllClientSkuRange();
        $dataRang = Hardware::getSkuAvailableRange();
        return view('pages.devices.addRangeDevice', ['dataRang' => $dataRang], compact('clients'));
    }

    public function addRangeDeviceModel(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id' => 'required:client_hardware',
        ]);
        if ($validate->fails())
            return back()->with('error', 'Sku and Sn is not unique')->withInput();

        $addRangeDeviceModel = ClientHardware::reserveUserSkuRange($request['user_id'] , $request['from_sku'] , $request['to_sku']);
        if ($addRangeDeviceModel['status'] == 'success') {
            return redirect('/addRangeDevice')->with('success', 'Done successfully');
        } elseif ($addRangeDeviceModel['status'] == 'fail') {
            return back()->with('error', 'Sorry! there is an error ')->withInput();
        }
    }

    public function viewDetailsRange($id){
        $client = Clients::where('user_id', $id)->first();
        $getUserSkuRange=ClientHardware::getUserSkuRange($id);
        return view('pages.devices.viewDetailsRange' , compact(['getUserSkuRange' , 'client']));
    }

    public function deleteRangeDevice(Request $request){
        $deleteRangeDevice = ClientHardware::deleteClientHardwareBySkuRange($request->skuFrom,$request->skuTo ,$request->userId );
        return $deleteRangeDevice;
    }

}
