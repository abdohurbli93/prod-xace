<?php
//
namespace App\Http\Controllers\Admin;

use App\Vendors\ClientsCredentials;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;


class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     *
     * @return View login
     *
     */
    public function loginView()
    {
        return View('pages.login');
    }

    /**
     * @param \Illuminate\Http\Request $request contains:
     *  1- username
     *  2- password
     * @return Response done (Sign in successfully) || failed (Login failed)
     */
    public function login(Request $request)
    {
        $admin =ClientsCredentials::where('username',$request->get('username'))
            ->where('password',$request->get('password'))
            ->first();
        if( !empty($admin)){
            auth()->login($admin);
            return redirect('/admin');
        }else {
            return redirect('/admin/login');
        }
    }

    /**
     *  Admin logout
     * @return login view
     */
    public function logout()
    {
        auth()->logout();
        return redirect('/admin/login');
    }
}
