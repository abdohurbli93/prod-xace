<?php

namespace App\Http\Controllers;

use App\Classes\XaceHelper;
use App\Helper\HttpHelper;
use App\User;
use App\Vendors\ClientBalance;
use App\Vendors\ClientHardware;
use App\Vendors\Clients;
use App\Vendors\Hardware;
use App\Vendors\HardwareData;
use App\Vendors\Vendors;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'client']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $dataRang = ClientHardware::getUserSkuRangeByHardwareType(Auth::id(), 'sim');

        $responseTemperature = [];
        $responseHumidity = [];
        $responseBattery = [];
        $responseSimConsumption = [];

        if ($request->has('date')) {
            $date = explode('-', $request['date']);
            $fromDate = Carbon::make($date[0]);
            $toDate = Carbon::make($date[1]);
            $date = $request->get('date');
        } else {
            //01/30/2019 12:00 AM - 01/31/2020 12:00 AM
            $fromDate = Carbon::make(Carbon::now()->subDays(1));
            $toDate = Carbon::make(Carbon::now());
            $date = Carbon::now()->subMonths(3)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
        }
        if ($request->has('from_sku') && $request->has('to_sku')) {
            $fromSku = $request->get('from_sku');
            $toSku = $request->get('to_sku');
            $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $fromSku, $toSku);
        } else {
            $hardwareIds = Hardware::getAllHardware(Auth::id());
        }
        //            if (!empty($hardwareIds)) {
        $responseTemperature =  HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
        $responseHumidity = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
        $responseBattery = HardwareData::getAllBatteryHistory($hardwareIds, $fromDate, $toDate);
        $responseSim = HardwareData::getAllSimDataUsageHistory($hardwareIds, $fromDate, $toDate);
        $responseSimConsumption = HardwareData::getAllSimDataConsumptionHistory($hardwareIds, $fromDate, $toDate);
        //            }
        return view('pages.end_home', [
            'dataRang' => $dataRang, 'date' => $date, 'data1' => $responseBattery,
            'data2' => $responseHumidity, 'data3' => $responseSim, 'data4' => $responseSimConsumption, 'data' => $responseTemperature
        ]);
    }

    public function indexAjax(Request $request)
    {
        $responseTemperature = [];
        $responseHumidity = [];
        $responseBattery = [];
        $responseSim = [];
        $responseSimConsumption = [];

        if ($request->has('date')) {
            $date = explode('-', $request['date']);
            $fromDate = Carbon::make($date[0]);
            $toDate = Carbon::make($date[1]);
            $date = $request->get('date');
        } else {
            $fromDate = Carbon::make(Carbon::now()->subDays(1));
            $toDate = Carbon::make(Carbon::now());
            $date = Carbon::now()->subDays(1)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
        }
        if ($request->has('from_sku') && $request->has('to_sku')) {
            $fromSku = $request->get('from_sku');
            $toSku = $request->get('to_sku');
            $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $fromSku, $toSku);
        } else {
            $hardwareIds = Hardware::getAllHardware(Auth::id());
        }
        //            if (!empty($hardwareIds)) {
        $responseTemperature =  HardwareData::getAllTemperatureHistory($hardwareIds, $fromDate, $toDate);
        $responseHumidity = HardwareData::getAllHumidityHistory($hardwareIds, $fromDate, $toDate);
        $responseBattery = HardwareData::getAllBatteryHistory($hardwareIds, $fromDate, $toDate);
        $responseSim = HardwareData::getAllSimDatausageHistory($hardwareIds, $fromDate, $toDate);
        $responseSimConsumption = HardwareData::getAllSimDataConsumptionHistory($hardwareIds, $fromDate, $toDate);


        //            }
        return response()->json(['date' => $date, 'data1' => json_decode($responseBattery, true),
            'data2' => json_decode($responseHumidity, true), 'data3' => json_decode($responseSim,true),
               'data4' => json_decode($responseHumidity, true) ,'data' => json_decode($responseTemperature, true)], '200');
        //return response()->json( ['date'=>$date,'data1'=>count($responseTemperature),'data2'=>count($responseHumidity),'data'=>count($responseTemperature)],'200');

    }

    public function editUserShow()
    {
        $editUserShow = User::where("users.id", \auth()->user()->id)->join('clients','clients.user_id','users.id')
          ->select(['users.*' , 'clients.client_name' , 'clients.id as client_id'])->first();
        return view('pages.updateUser', compact('editUserShow'));
    }

    public function updateUserModel(Request $request)
    {
        $user = $request->except('_token', 'client_name', 'client_id');
        $client = $request->only('client_name');
        $user = User::updateUser($user, $request['id']);
        if (empty($user))
            return back()->with('error', 'Sorry! there is an error ')->withInput();
        $client = Clients::updateClient($client, $request['client_id']);
        if (!empty($client))
            return back()->with('success', 'Done successfully');
        return back()->with('error', 'Sorry! there is an error ')->withInput();
    }


    public function getDeviceList()
    {
        $userId = auth()->id();
        $hardwares = ClientHardware::getClientHardware($userId);
        foreach ($hardwares as $hardwareSimBalance){
        $hardwareSimBalance['balance'] = HardwareData::join('hardware', 'hardware.id', 'hardware_data.hardware_id')
            ->where('hardware.hardware_type','sim')
            ->where('hardware_data.hardware_id',$hardwareSimBalance['hardware_id'])
            ->orderBy('hardware_data.date_time', 'DESC')
            ->select(['hardware_data.data_usage'])
            ->first();
        if (!empty($hardwareSimBalance['balance']))
            $hardwareSimBalance['balance'] =$hardwareSimBalance['balance']->data_usage;
        else
            $hardwareSimBalance['balance']='';
        }
        $countSIM = ClientHardware::join('hardware', 'hardware.id', '=', 'client_hardware.hardware_id')
            ->where('client_hardware.user_id', $userId)->where('hardware.hardware_type', 'sim')->count();
        $countSensor = ClientHardware::join('hardware', 'hardware.id', '=', 'client_hardware.hardware_id')
            ->where('client_hardware.user_id', $userId)->where('hardware.hardware_type', 'sensor')->count();
        $countTotal = $countSIM +  $countSensor;
        return view('pages.get_device_list', compact(['hardwares', 'countSIM', 'countSensor', 'countTotal']));
    }

    public function getDetailSim()
    {
        $client = Clients::where('user_id', Auth::id())->first();
        if ($client->client_type == 'test') {
            $clientBalance = Clients::where('user_id', Auth::id())->first();
            $sims = ClientHardware::getUserHardwareType('sim');
            $currentBalance = ClientBalance::getLastBalance(Auth::id());
            $currentBalance = $currentBalance->current_balance;
            return view('pages.detail_sim', compact(['clientBalance', 'sims', 'currentBalance']));
        } else
            return redirect('/home');
    }

    public function addClientBalance(Request $request)
    {
        $addClientBalance = Clients::refillUserSimByHardwareId($request['hardware_id'], $request['client_balance']);
        if (!empty($addClientBalance)) {
            return back()->with('success', 'Done successfully');
        } else {
            return back()->with('error', 'Sorry! there is an error ')->withInput();
        }
    }

    public function MultiSimDataJson(Request $request)
    {
        if ($request->has('date')) {
            $date = explode('-', $request['date']);
            $fromDate = Carbon::make($date[0]);
            $toDate = Carbon::make($date[1]);
            $date = $request->get('date');
        } else {
            $fromDate = Carbon::make(Carbon::now()->subDays(1));
            $toDate = Carbon::make(Carbon::now());
            $date = Carbon::now()->subDays(1)->format('m/d/Y h:i A') . ' - ' .
                Carbon::now()->format('m/d/Y h:i A');
        }
        $skuInput=$request->get('inputSku');
        $skuInput=XaceHelper::getNumberFromString($skuInput);
           $currentData=array();
           $consumptionData=array();
           for($i=0,$j=0;($i<count($skuInput)&&$i<5);$i++,$j++)
           {
               if(empty($check=ClientHardware::checkUserSku($skuInput[$i],Auth::id())))
                   return  response()->json(['data'=>'not allowd'],404);
               $currentData[$j]=HardwareData::getSimHistoryBySku($skuInput[$i],$fromDate,$toDate);
               $consumptionData[$j]=HardwareData::getSimHistoryBySku($skuInput[$i],$fromDate,$toDate,'data_consumption');
           }

        if ($request->has('from_sku') && $request->has('to_sku')) {
            $fromSku = $request->get('from_sku');
            $toSku = $request->get('to_sku');
            $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $fromSku, $toSku);
        } else {
            $hardwareIds = Hardware::getAllHardware(Auth::id());
        }

        $responseSim = HardwareData::getAllSimDatausageHistory($hardwareIds, $fromDate, $toDate);
        $responseSimConsumption=HardwareData::getAllSimDataConsumptionHistory($hardwareIds, $fromDate, $toDate);

           return  response()->json(['currentData'=> $currentData,
               'consumptionData'=>$consumptionData],200);

        return  response()->json([
            'currentData' => $currentData,
            'consumptionData' => $consumptionData
        ], 200);
    }
    public function AllSimDataJson(Request $request)
    {
        $responseSim = [];
        $responseSimConsumption = [];
        if ($request->has('date')) {
            $date = explode('-', $request['date']);
            $fromDate = Carbon::make($date[0]);
            $toDate = Carbon::make($date[1]);
            $date = $request->get('date');
        } else {
            $fromDate = Carbon::make(Carbon::now()->subDays(1));
            $toDate = Carbon::make(Carbon::now());
            $date = Carbon::now()->subDays(1)->format('m/d/Y h:i A') . ' - ' . Carbon::now()->format('m/d/Y h:i A');
        }
        if ($request->has('from_sku') && $request->has('to_sku')) {
            $fromSku = $request->get('from_sku');
            $toSku = $request->get('to_sku');
            $hardwareIds = Hardware::getHardwareBySku(Auth::id(), $fromSku, $toSku);
        } else {
            $hardwareIds = Hardware::getAllHardware(Auth::id());
        }
        $responseSim = HardwareData::getAllSimDatausageHistory($hardwareIds, $fromDate, $toDate);
        $responseSimConsumption = HardwareData::getAllSimDataConsumptionHistory($hardwareIds, $fromDate, $toDate);

        return  response()->json([
            'allSimData' => json_decode($responseSim, true),
            'allSimConsumption' => json_decode($responseSimConsumption, true)
        ], 200);
    }

    public function showHardware(){
     $showDetail = Hardware::where('id', Auth::id())->first();
    return view('pages.test', compact('showDetail'));
    }

    public function master(){
        $hardware=collect([30007,30008,30009,30010]);
        $data=array();
        $data=null;
        $i=0;
        foreach ($hardware as $hard)
        {
            $device=Hardware::getDeviceBySku($hard);
            $latestData=HardwareData::getLatestHardwareData($device->id);
            $data[$i]['sku']=$hard;
            if((XaceHelper::CheckDataTime($latestData->date_time)>8))
            {
                $data[$i]['humidity']['status'] = 'Inactive';
                $data[$i]['temperature']['status']='Inactive';
            }
            else {
                $data[$i]['humidity']['status']='Active';
                $data[$i]['temperature']['status']='Active';
            }
            $temperatureData=HardwareData::getLastHardwareTypeDataBySku($device->sku,'temperature');
            $humidityData=HardwareData::getLastHardwareTypeDataBySku($device->sku,'humidity');
            $data[$i]['temperature']['data']=$temperatureData['value'];
            $data[$i]['humidity']['data']=$humidityData['value'];
            $data[$i]['temperature']['date_time']=$temperatureData['date_time'];
            $data[$i]['humidity']['date_time']=$humidityData['date_time'];
            $i++;
        }
        return view('pages.dashboard', compact('data'));
    }
}


