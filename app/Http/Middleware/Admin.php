<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (optional(auth()->user())->user_type == 'admin')
            return $next($request);
        else if(optional(auth()->user())->user_type == 'client'){

            if(\Request::is('api/*'))
                return redirect('api/ApiErrorToken');
            else
                return redirect('home');
        }
        else{
            if(\Request::is('api/*'))
                return redirect('api/ApiErrorToken');
            else
                return redirect('home');
        }

    }
}
