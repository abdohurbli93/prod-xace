<?php

namespace App\Http\Middleware;

use Closure;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (optional(auth()->user())->user_type == 'client')
            return $next($request);
        else if(optional(auth()->user())->user_type == 'admin'){
            if(\Request::is('api/*'))
                return redirect('api/ApiErrorToken');
            else
                return redirect('admin/home');
        }
        else{
            if(\Request::is('api/*'))
                return redirect('api/ApiErrorToken');
            else
                return redirect('home');
        }
//
    }
}
