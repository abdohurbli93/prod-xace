<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable ,HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'phone_number', 'username', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public function setPasswordAttribute($pass){
//
//        $this->attributes['password'] = Hash::make($pass);
//
//    }

    public static function getUserByEmail($email)
    {
        return User::where('email', $email)
            ->first();
    }
    public static function addUser($user)
    {
        return User::create($user);
    }

    public static function updateUser($user, $userId)
    {
        return User::where('id', $userId)
            ->update($user);
    }

    public static function deleteUser($userId)
    {
        $deletedClient = User::where('id', $userId)
            ->delete();
        if (!empty($deletedClient))
            return true;
        return false;
    }
    public static function getUserById($userId)
    {
        return User::where('id', $userId)
            ->first();
    }
    public static function getUserByUserName($username)
    {
        return User::where('username', $username)
            ->first();
    }

    public static function login($email, $password)
    {
        if (Auth::attempt(['username' => $email, 'password' => $password])) {
            $user = Auth::user();

            $user = User::join('clients', 'clients.user_id', '=', 'users.id')
                ->where('users.id', $user->id)->
                select(['clients.client_name', 'clients.id','clients.client_type','clients.client_balance',
                    'users.username', 'users.email', 'users.address', 'users.image_url', 'users.phone_number'])->first();
            $user['token'] = Auth::user()->createToken('flexiot')->accessToken;
            return $user;
        }
        return null;
    }
}
