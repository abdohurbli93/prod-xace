<?php


namespace App\Classes;


use Carbon\Carbon;

class XaceHelper
{

    public static function getNumberFromString($string)
    {
        $StringNumbers=array();
        preg_match_all('!\d+!', $string, $StringNumbers);//get numbers from string put it in array
        $count=0;
        $numbers= Array();
        for ($j=0; $j<count($StringNumbers[0]);$j++,$count++)
            $numbers[$count]=intval($StringNumbers[0][$j]);

        return $numbers;
    }
    public static function CheckDataTime($dataTime)
    {
      $dataTime=Carbon::parse($dataTime);
      $currentDateTime=Carbon::now();
     return $currentDateTime->diffInMinutes($dataTime);
    }
    public static function getFlexiBalanceByMega($flexiData)
    {
        if(!empty($flexiData))
        {
            $flexiData = get_object_vars($flexiData);
            $flexiData = $flexiData['return']->bucketInfos->bytesAvailable / (1024 * 1024);
        }
        return $flexiData;
    }
}

