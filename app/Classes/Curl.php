<?php


namespace App\Classes;


class Curl
{
    private  $credential;
    private  $url;

    function __construct($credential, $url)
    {
        $this->credential = $credential;
        $this->url = $url;
    }

    function getCredentials()
    {
        return $this->credential;
    }
    function getUrl()
    {
        return $this->url;
    }

    function setCredentials($credential)
    {
        $this->credential = $credential;
    }
    function setUrl($url)
    {
        $this->url = $url;
    }



    /**
     * curl <T> method helps to call any third party APIs
     * @param string $url
     * @param array $params (post/ query param)
     * @param boolean $withHeader
     * @param boolean $isPost false = GET
     * @return T depends on APIs response
     */

    function callCurl($urlPrefix, $params, $withHeader = false, $isPost = false)
    {
        $headers = [];
        $ch = curl_init($this->getUrl() . $urlPrefix);
        curl_setopt($ch, CURLOPT_POST, $isPost);
        if ($isPost)
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params, true));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->getCredentials());
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!$withHeader)
            $headers = array('Content-Type: application/json');
        else $headers = array(
            'Content-Type: application/json',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return curl_exec($ch);
    }
}
