<?php

namespace App\Policies;

use App\User;
use App\Vendors\Clients;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isAdmin()
    {
        return (optional(auth()->user())->user_type == 'admin');
    }

    public function isClient()
    {
        return (optional(auth()->user())->user_type == 'client');
    }


    public function hasClientBalance()
    {
        $client = Clients::where('user_id', Auth::id())->first();
        return (optional($client)->client_type == 'test');
    }
}
