<?php

namespace App\Helper;

class HttpHelper
{
    /**
     * @param mixed $data
     * @return JSON obj
     */
    public static function select($data = null)
    {
        return response(['status' => 'OK', 'data' => $data], 220);
    }

    /**
     * @param $data Mixed
     * @return json
     */
    public static function insert($data = null)
    {
        return response(['status' => 'OK', 'data' => $data], 230);
    }
    /**
     * @param $data Mixed
     * @return json
     */
    public static function update($data = null)
    {
        return response(['status' => 'OK', 'data' => $data], 235);
    }
    /**
     * @param $data Mixed
     * deleted Successful
     */
    public static function delete($data = null)
    {
        return response(['status' => 'OK', 'data' => $data], 240);
    }
    /**
     * @param $param Mixed
     * Something Already Exist
     * @return json
     */
    public static function errorAlreadyExists($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 330);
    }
    /**
     * @param $data Mixed
     * Parameter Missing
     * @return json
     */

    public static function errorMissingParameter($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 400);
    }
    /**
     * @param $data Mixed
     * User not Auth.
     * @return json
     */
    public static function errorNotAllowed($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 460);
    }

    /**
     * @param $data Mixed
     * Internal Server Error
     * @return json
     */
    public static function generalError($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 500);
    }
    /**
     * @param mixed
     * not authorized user
     * @return json
     */
    public static function notAuthorized($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 550);
    }

    /**
     * @param mixed
     * token miss match exception
     * @return json
     */
    public static function tokenMissMatch($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 400);
    }
    /**
     * @param mixed
     * isEmpty Array exception
     * @return json
     */
    public static function isEmpty($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 400);
    }

    /**
     * @param mixed
     * success Array exception
     * @return json
     */
    public static function success($msg = null)
    {
        return response(['status' => 'ERROR', 'msg' => $msg], 200);
    }
}
