<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class ProjectAuthentication extends Model
{
    protected $table='project_authentication';

    protected $fillable = [
        'example','description','project_id'
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];
    protected $appends = ['class_name'];

    public function getClassNameAttribute()
    {
        $class_name= 'Authentication';
        return $class_name;
    }
    public function project()
    {
        return $this->belongsTo('App\Documentation\Projects','project_id');
    }
}
