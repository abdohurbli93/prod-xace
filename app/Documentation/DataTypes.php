<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class DataTypes extends Model
{
    protected $table='data_types';

    protected $fillable = [
        'type_name','is_object','project_id',
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];

    public function project()
    {
        return $this->belongsTo('App\Documentation\Projects');
    }

    public function dataTypeDetail()
    {
        return $this->hasMany('App\Documentation\DataTypesDetails');
    }

    public function requestResponsesData()
    {
        return $this->hasMany('App\Documentation\RequestResponsesData','data_type_id');
    }
    public function requestData()
    {
        return $this->hasMany('App\Documentation\RequestData','data_type_id');
    }
    public function requestParams()
    {
        return $this->hasMany('App\Documentation\RequestParam','data_type_id');
    }

    public static function getDataTypeObjectDetail()
    {
      $dataType=DataTypes::all();
      foreach ($dataType as $dt)
      {
          if($dt->is_object)
          {
              $dt['childs']=DataTypesDetails::getDataTypeDetailByDataTypeId($dt->id);
          }
      }
      return $dataType;
    }

}
