<?php

namespace App\Documentation;
use App\PartRequests;
use Illuminate\Database\Eloquent\Model;

class ProjectParts extends Model
{
    protected $table='project_parts';

    protected $fillable = [
        'name','description','url','image','project_id'
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];
    public function project()
    {
        return $this->belongsTo('App\Documentation\Projects');
    }
    public function partRequest()
    {
        return $this->hasMany('App\Documentation\PartRequests','project_part_id');
    }
}
