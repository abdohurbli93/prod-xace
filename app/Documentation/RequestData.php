<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class RequestData extends Model
{
    protected $table='request_data';

    protected $fillable = [
        'name','description','temp_value','required','data_type_id','is_array','part_requests_id',
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];

    public function dataType()
    {
        return $this->belongsTo('App\Documentation\DataTypes');
    }
    public function partRequest()
    {
        return $this->belongsTo('App\Documentation\PartRequests','part_requests_id');
    }
}
