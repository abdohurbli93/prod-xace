<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class RequestResponsesData extends Model
{
    protected $table='request_responses_data';

    protected $fillable = [
        'name','is_array','description','temp_value','data_type_id','request_responses_id'
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];
    public function dataType()
    {
        return $this->belongsTo('App\Documentation\DataTypes');
    }
    public function requestResponse()
    {
        return $this->belongsTo('App\Documentation\RequestResponses','request_responses_id');
    }
}
