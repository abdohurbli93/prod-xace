<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class Responses extends Model
{
    protected $table='responses';

    protected $fillable = [
        'msg','status_code',
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];

    public function requestResponses()
    {
        return $this->hasMany('App\Documentation\RequestResponses','response_id');
    }

}
