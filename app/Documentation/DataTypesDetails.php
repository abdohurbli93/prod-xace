<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class DataTypesDetails extends Model
{
    protected $table='data_types_details';

    protected $fillable = [
        'name','is_object','data_types_id','object_id','description',
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];

    public static function getDataTypeDetailByDataTypeId($dataTypeId)
    {
       $dataTypeDetail=DataTypesDetails::where('object_id',$dataTypeId)
           ->join('data_types','data_types.id','=', 'data_types_details.data_types_id')
               ->select('data_types.type_name as data_types_name',
                   'data_types.is_object as data_types_is_object',
                   'data_types_details.*')
            ->get();
        foreach($dataTypeDetail as $dtd)
        {
            if($dtd->is_object)
                $dtd['childs']=self::getDataTypeDetailByDataTypeId($dtd->id);
        }
        return $dataTypeDetail;
    }

}
