<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class ProjectCommonTerms extends Model
{
    protected $table='project_common_terms';

    protected $fillable = [
        'status','msg','project_id'
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];
    protected $appends = ['class_name'];

    public function getClassNameAttribute()
    {
        $class_name= 'Common Terms';
        return $class_name;
    }
    public function project()
    {
        return $this->belongsTo('App\Documentation\Projects','project_id');
    }
}
