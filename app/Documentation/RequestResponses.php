<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class RequestResponses extends Model
{
    protected $table='request_responses';

    protected $fillable = [
        'request_msg','response_id','part_requests_id',
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];
    public function response()
    {
        return $this->belongsTo('App\Documentation\Responses');
    }
    public function partRequest()
    {
        return $this->belongsTo('App\Documentation\PartRequests','part_requests_id');
    }
    public function RequestResponsesData()
    {
        return $this->hasMany('App\Documentation\RequestResponsesData');
    }
}
