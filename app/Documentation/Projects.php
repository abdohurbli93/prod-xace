<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class Projects extends Model
{
    protected $table = 'projects';


    protected $fillable = [
        'name', 'detail', 'finish_date', 'start_date', 'url', 'company_id'
    ];


    protected $hidden = [
        'created_at', 'updated_at'
    ];


    public function company()
    {
        return $this->belongsTo('App\Documentation\Company');
    }

    public function projectParts()
    {
        return $this->hasMany('App\Documentation\ProjectParts', 'project_id');
    }

    public function dataTypes()
    {
        return $this->hasMany('App\Documentation\DataTypes', 'project_id');
    }
    public function authentication()
    {
        return $this->hasMany('App\Documentation\ProjectAuthentication', 'project_id');
    }
    public function general()
    {
        return $this->hasMany('App\Documentation\ProjectGeneral', 'project_id');
    }

    public function commonTerms()
    {
        return $this->hasMany('App\Documentation\ProjectCommonTerms', 'project_id');
    }

    public static function getProjectWithDetailByCompanyId($companyId)
    {
        $result = Projects::with(['general', 'authentication'])
            ->with(['projectParts' => function ($query) {
                $query->with(['partRequest'=>function($partRequest){
                    $partRequest->orderBy('part_request_order','ASC')->get();
                }])->get();
            }])
            ->get();

        foreach ($result[0]->projectParts as $projectPar) {
            foreach ($projectPar->partRequest as $partReq) {
                $partReq['data'] = PartRequests::getPartRequestDetail($partReq['id']);
            }
        }
        return $result[0];
    }

}
