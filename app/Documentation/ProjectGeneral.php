<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class ProjectGeneral extends Model
{
    protected $table='project_general';

    protected $fillable = [
        'example_url','protocol','url_path','response_format','date_format','project_id'
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];
    protected $appends = ['class_name'];

    public function getClassNameAttribute()
    {
        $class_name='General';
        return $class_name;
    }
    public function project()
    {
        return $this->belongsTo('App\Documentation\Projects','project_id');
    }
}
