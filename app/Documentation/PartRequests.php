<?php

namespace App\Documentation;
use App\ProjectParts;
use Illuminate\Database\Eloquent\Model;

class PartRequests extends Model
{
    protected $table='part_requests';

    protected $fillable = [
        'name','description','url','request_limit','request_condition','type','project_part_id'
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];

    public function projectPart()
    {
       return $this->belongsTo('App\Documentation\ProjectParts');
    }
    public function requestResponses()
    {
        return $this->hasMany('App\Documentation\RequestResponses');
    }
    public function requestParams()
    {
        return $this->hasMany('App\Documentation\RequestParam');
    }
    public function requestData()
    {
        return $this->hasMany('App\Documentation\RequestData');
    }
        //'requestData'
    public static function getPartRequestDetail($partRequestId)
    {
       $partRequests=PartRequests::where('id',$partRequestId)
           ->with(['requestData'=>function($requestData){
          $requestData->join('data_types','data_types.id','=', 'request_data.data_type_id')
              ->select('data_types.type_name as data_types_name',
                  'data_types.is_object as data_types_is_object',
                  'request_data.*');
      }])->with(['requestParams'=>function($requestParams){
          $requestParams->join('data_types','data_types.id','=', 'request_param.data_type_id')
              ->select('data_types.type_name as data_types_name',
                  'data_types.is_object as data_types_is_object',
                  'request_param.*');
      }])->with(['requestResponses'=>function($requestResponses) {
        $requestResponses->join('responses','request_responses.response_id','=','responses.id')
            ->select(['responses.id as response_id','responses.msg as responses_msg',
                'responses.status_code as responses_status_code',
                'request_responses.*'])
            ->with(['RequestResponsesData'=>function($RequestResponsesData){
          $RequestResponsesData->join('data_types','data_types.id','=',
              'request_responses_data.data_type_id')
              ->select('data_types.type_name as data_types_name',
                  'data_types.is_object as data_types_is_object',
                  'request_responses_data.*');
      }]);
    }])->get();
       foreach ($partRequests as $partRequest)
       {
          foreach ($partRequest->requestResponses as $requestResponses)
          {
              foreach ($requestResponses->RequestResponsesData as $RequestResponsesData) {
                  if (!empty($RequestResponsesData)) {
                      if ($RequestResponsesData->data_types_is_object == 1) {
                          $RequestResponsesData['child'] = DataTypesDetails
                              ::getDataTypeDetailByDataTypeId($RequestResponsesData['data_type_id']);
                      } else {
                          $RequestResponsesData['child'] = null;
                      }

                  }
              }
          }
           foreach ($partRequest->requestData as $requestData)
           {
               if(!empty($requestData))
               {
                   if($requestData->data_types_is_object==1)
                   {
                       $requestData['child']=DataTypesDetails
                           ::getDataTypeDetailByDataTypeId($requestData['data_type_id']);
                   }
                   else  {
                       $requestData['child']=null;
                   }

               }
           }

       }
       return $partRequests[0];
    }
}
