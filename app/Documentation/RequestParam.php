<?php

namespace App\Documentation;

use Illuminate\Database\Eloquent\Model;

class RequestParam extends Model
{
    protected $table='request_param';

    protected $fillable = [
        'name','required','description','temp_value','data_type_id','part_requests_id'
    ];


    protected $hidden = [
        'created_at','updated_at'
    ];

    public function dataType()
    {
        return $this->belongsTo('App\Documentation\DataTypes');
    }
    public function partRequest()
    {
        return $this->belongsTo('App\Documentation\PartRequests','part_requests_id');
    }
}
