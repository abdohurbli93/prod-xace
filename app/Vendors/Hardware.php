<?php

namespace App\Vendors;

use ArrayObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Object_;

class Hardware extends Model
{

    use SoftDeletes;
    protected $table = 'hardware';
    protected $fillable = ['vendor_id', 'device_name', 'hardware_type', 'status', 'sku', 'sn', 'hostname','imsi',
        'temperature_address', 'humidity_address', 'battery_address', 'msisdn', 'iccid', 'pin', 'puk'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public static function addHardware($hardware)
    {
        return Hardware::create($hardware);
    }

    public static function updateHardware($hardware, $hardwareId)
    {
        return Hardware::where('id', $hardwareId)
            ->update($hardware);
    }

    public static function deleteHardware($hardwareId)
    {
        $deletedHardware = Hardware::where('id', $hardwareId)
            ->delete();
        if (!empty($deletedHardware))
            return true;
        return false;
    }

    public static function getHardwareById($hardwareId, $userId=null)
    {
        if(!empty($userId)) {
            return ClientHardware::where('client_hardware.id', $hardwareId)
                ->where('client_hardware.user_id', $userId)
                ->join('hardware', 'hardware.id', 'client_hardware.hardware_id')->first();
        }

        return ClientHardware::where('client_hardware.id', $hardwareId)
            ->join('hardware', 'hardware.id', 'client_hardware.hardware_id')
            ->first();
    }


    public static function getAllHardware($userId)
    {
        return ClientHardware::where('client_hardware.user_id', $userId)->join('hardware', 'hardware.id', 'client_hardware.hardware_id')->pluck('hardware_id')->toArray();
    }

    public static function getHardwareBySku($userId, $fromSku, $toSku)
    {
        return ClientHardware::where('client_hardware.user_id', $userId)
            ->whereBetween('sku', [$fromSku, $toSku])
            ->join('hardware', 'hardware.id', 'client_hardware.hardware_id')
            ->pluck('hardware_id')->toArray();
    }
    public static function getSkuAvailableRange()
    {
        /*SELECT * FROM hardware LEFT JOIN client_hardware
        ON hardware.id`=`client_hardware.hardware_id WHERE
        client_hardware.hardware_id is null*/
         $skuRange=Hardware::leftJoin('client_hardware',function ($join){
            $join->on('hardware.id','=','client_hardware.hardware_id');
        })->whereNull('client_hardware.hardware_id')
             ->orWhereNotNull('client_hardware.deleted_at')
            ->WhereNotIn('client_hardware.hardware_id',function ($query)
             {
                 $query->from('client_hardware')
                     ->select('client_hardware.hardware_id')
                        ->whereNull('client_hardware.deleted_at');
             })
             ->orderBy('hardware.sku','asc')
            ->select('hardware.sku','hardware.hardware_type','hardware.vendor_id','hardware.id')
            ->get()->toArray();
            if(empty($skuRange))
                return null;
           return self::separateSkuToRange($skuRange);

    }
    public static function separateSkuToRange($skuRange)
    {
        $data['sim']= collect();
        $data['sensor']= collect();
        $number['sim']=0;
        $number['sensor']=0;

        $currentType=$skuRange[0]['hardware_type'];
        $currentRangeStart=$skuRange[0]['sku'];
        $number[$currentType]++;
        for($i=1;$i<count($skuRange);$i++)
        {
            if($currentType!=$skuRange[$i]['hardware_type']||$skuRange[$i]['sku']-1>$skuRange[$i-1]['sku'])
            {
                $data[$currentType]->add([
                    'from'=>$currentRangeStart,
                    'to'=>$skuRange[$i-1]['sku'],
                    'range_number'=>$currentType.'_'.$number[$currentType]
                ]);
                $currentType=$skuRange[$i]['hardware_type'];
                $currentRangeStart=$skuRange[$i]['sku'];
                $number[$currentType]++;
            }
        }
        $data[$currentType]->add([
            'from'=>$currentRangeStart,
            'to'=>$skuRange[$i-1]['sku'],
            'range_number'=>$currentType.'_'.$number[$currentType]
        ]);
        $data['max']=$skuRange[count($skuRange)-1]['sku'];
        return $data;
    }

    public static function getHardwareByRange($skuFrom, $skuTo)
    {
      return  Hardware::whereBetween('sku',[$skuFrom,$skuTo])
            ->orderBy('sku','asc')
            ->get()->toArray();
    }
    public static function getHardwareByType($hardwareType)
    {
        return Hardware::where('hardware_type',$hardwareType)
            ->get();
    }
    public static function getUserHardwareByHardwareId($userId,$hardwareId,$hardware_type)
    {
        return Hardware::where('hardware.id',$hardwareId)
            ->where('hardware.hardware_type',$hardware_type)
            ->join('client_hardware','client_hardware.hardware_id','hardware.id')
            ->where('client_hardware.user_id',$userId)
            ->first();
    }
    public static function checkHardwareSku($sku)
    {
      return  Hardware::where('sku',$sku)
            ->first();
    }
    public static function getDeviceBySku($sku)
    {
        return Hardware::where('sku',$sku)
            ->first();
    }
    public static function getUserHardwareByHardwareSku($userId,$sku,$hardware_type)
    {
        return Hardware::where('hardware.sku',$sku)
            ->where('hardware.hardware_type',$hardware_type)
            ->join('client_hardware','client_hardware.hardware_id','hardware.id')
            ->where('client_hardware.user_id',$userId)
            ->first();
    }

}
