<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class HardwareData extends Model
{
    use SoftDeletes;
    protected $table = 'hardware_data';
    protected $fillable = ['hardware_id', 'date_time', 'temperature', 'humidity', 'data_usage', 'battery','data_consumption'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];



    public static function addResult($result)
    {
        return HardwareData::create($result);
    }

    public static function updateResult($result, $resultId)
    {
        return HardwareData::where('id', $resultId)
            ->update($result);
    }

    public static function deleteHardware($resultId)
    {
        $result = HardwareData::where('id', $resultId)
            ->delete();
        if (!empty($result))
            return true;
        return false;
    }

    public static function getResultById($resultId)
    {
        return  HardwareData::where('id', $resultId)
            ->first();
    }
    public static function getResultValuesByHardwareId($hardwareId)
    {
        return HardwareData::where('hardware_id', $hardwareId)
            ->select('value')->latest('date_time')->take(15)
            ->get();
    }
    public static function getResultDateTimeByHardwareId($hardwareId)
    {
        return HardwareData::where('hardware_id', $hardwareId)
            ->select('date_time')->latest('date_time')->take(15)
            ->get();
    }
    public static function getTemperatureHistoryByHardwareId($hardwareId, $fromDate, $toDate)
    {
        return HardwareData::where('hardware_id', $hardwareId)
            ->whereNotNull('temperature')
            ->select('temperature as value', 'date_time')->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->get();
    }
    public static function getHumidityHistoryByHardwareId($hardwareId, $fromDate, $toDate)
    {
        return HardwareData::where('hardware_id', $hardwareId)
            ->whereNotNull('humidity')
            ->select('humidity as value', 'date_time')->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->get();
    }
    public static function getBatteryHistoryByHardwareId($hardwareId, $fromDate, $toDate)
    {
        return HardwareData::where('hardware_id', $hardwareId)
            ->whereNotNull('battery')
            ->select('battery as value', 'date_time')->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->get();
    }
    public static function getSimHistoryByHardwareId($hardwareId, $fromDate, $toDate)
    {
        return HardwareData::where('hardware_id', $hardwareId)
            ->whereNotNull('data_usage')
            ->select('data_usage as value', 'date_time')
            ->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->get();
    }
    public static function getSimHistoryBySku($sku, $fromDate, $toDate,$type='data_usage')
    {
        return HardwareData::join('hardware','hardware.id','hardware_data.hardware_id')
            ->where('hardware.sku', $sku)
            ->whereNotNull('hardware_data.'.$type)
            ->whereBetween('hardware_data.date_time', [$fromDate, $toDate])
            ->select('hardware_data.'.$type.' as value', 'hardware_data.date_time','hardware.sku as sku')
            ->latest('hardware_data.date_time')
            ->get();
    }

    public static function getAllTemperatureHistory($hardwareIds, $fromDate, $toDate)
    {
        $data = HardwareData::whereNotNull('temperature')->whereIn('hardware_id', $hardwareIds)
            ->select(
                DB::raw('AVG(temperature) as value'),
                'date_time'
            )->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->groupBy('date_time')
            ->get();
        return json_encode(array_reverse($data->toArray()));
    }
    public static function getAllHumidityHistory($hardwareIds, $fromDate, $toDate)
    {

        $data = HardwareData::whereNotNull('humidity')->whereIn('hardware_id', $hardwareIds)
            ->select(
                DB::raw('AVG(humidity) as value'),
                'date_time'
            )->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->groupBy('date_time')
            ->get();
        return json_encode(array_reverse($data->toArray()));
    }
    public static function getAllBatteryHistory($hardwareIds, $fromDate, $toDate)
    {
        $data = HardwareData::whereNotNull('battery')->whereIn('hardware_id', $hardwareIds)
            ->select(
                DB::raw('AVG(battery) as value'),
                'date_time'
            )->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->groupBy('date_time')
            ->get();
        return json_encode(array_reverse($data->toArray()));
    }
    public static function getAllSimDataUsageHistory($hardwareIds, $fromDate, $toDate)
    {
        $data = HardwareData::whereNotNull('data_usage')->whereIn('hardware_id', $hardwareIds)
            ->select(
                DB::raw('AVG(data_usage) as value'),
                'date_time'
            )->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->groupBy('date_time')
            ->get();
        return json_encode(array_reverse($data->toArray()));
    }

    public static function getAllClientSimBalance($userId)
    {
        return ClientHardware::where('user_id', $userId)
            ->join('hardware', 'hardware.id', 'client_hardware.hardware_id')
            ->where('hardware.hardware_type','sim')
            ->select([
                'hardware.device_name',
                'hardware.sku',
                'hardware.sn',
                'hardware.status',
            ])
            ->addSelect(['current_balance' =>
                HardwareData::select('data_usage')
                    ->whereColumn('hardware_id','client_hardware.hardware_id')
                    ->orderBy('created_at','DESC')
                    ->limit(1)
            ])
            ->get();
    }
    public static function getAllClientSimBalanceHistory($userId,$fromDate,$toDate,$skuFrom=0,$skuTo=0,$type='data_consumption')
    {
        return ClientHardware::where('user_id', $userId)
            ->join('hardware', 'hardware.id', 'client_hardware.hardware_id')
            ->when($skuTo>0,function ($query) use ($skuFrom,$skuTo)
            {
                return  $query->whereBetween('hardware.sku',[$skuFrom,$skuTo]);
            })
            ->where('hardware.hardware_type','sim')
            ->select([
                'hardware.device_name',
                'hardware.sku',
                'hardware.sn',
                'hardware.status',
            ])
            ->addSelect(['balance_avg' => function($query) use($fromDate,$toDate,$type) {
                $query->from('hardware_data')
                    ->whereColumn('hardware_data.hardware_id', 'client_hardware.hardware_id')
                    ->whereBetween('hardware_data.date_time', [$fromDate, $toDate])
                    ->select(DB::raw('AVG(hardware_data.'.$type.') as value'))
                    ->orderBy('created_at', 'DESC')
                    ->limit(1);
            }])
            ->get();
    }
    public static function getLatestHardwareData($hardwareId)
    {
        return HardwareData::where('hardware_id',$hardwareId)
                ->latest()->first();
    }

    public static function getAllSimDataConsumptionHistory($hardwareIds,  $fromDate,  $toDate)
    {
        $data = HardwareData::whereNotNull('data_consumption')->whereIn('hardware_id', $hardwareIds)
            ->select(
                DB::raw('AVG(data_consumption) as value'),
                'date_time'
            )->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->groupBy('date_time')
            ->get();
        return json_encode(array_reverse($data->toArray()));
    }

    public static function getAllHardwareDataTypeHistory($hardwareDataType,$hardwareIds,  $fromDate,  $toDate)
    {
        $data = HardwareData::whereNotNull($hardwareDataType)->whereIn('hardware_id', $hardwareIds)
            ->select(
                DB::raw('AVG('.$hardwareDataType.') as value'),
                'date_time'
            )->latest('date_time')->whereBetween('date_time', [$fromDate, $toDate])
            ->groupBy('date_time')
            ->get();
        return array_reverse($data->toArray());
    }
    public static function getLastHardwareTypeDataBySku($sku,$hardwareType)
    {
        return HardwareData::join('hardware','hardware.id','=','hardware_data.hardware_id')
            ->where('hardware.sku',$sku)
            ->whereNotNull('hardware_data.'.$hardwareType)
            ->select('hardware_data.'.$hardwareType.' as value','hardware_data.date_time')
            ->latest('hardware_data.created_at')->first();
    }
    public static function getSimConsumptionHistoryBySku($sku)
    {
        return  HardwareData::join('hardware','hardware.id','=','hardware_data.hardware_id')
            ->where('hardware.sku',$sku)
            ->whereNotNull('hardware_data.data_consumption')
            ->sum('hardware_data.data_consumption');
    }

}

