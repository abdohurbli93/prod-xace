<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorCredentials extends Model
{
    use SoftDeletes;
    protected $table='vendor_credentials';
    protected $fillable=['vendor_id','header_json','base_url','type'];
    protected $hidden=['created_at','updated_at','deleted_at'];


    public static function addVendorCredential($vendorCredential)
    {
        return VendorCredentials::create($vendorCredential);
    }

    public static function updateVendorCredential($vendorCredential,$vendorCredentialId)
    {
        return VendorCredentials::where('id',$vendorCredentialId)
            ->update($vendorCredential);
    }

    public static function deleteVendorCredential($vendorCredentialId)
    {
        $deletedVendorCredential=VendorCredentials::where('id',$vendorCredentialId)
            ->delete();
        if(!empty($deletedVendorCredential))
            return true;
        return false;
    }
    public static function getVendorCredentialById($vendorCredentialId)
    {
      return  VendorCredentials::where('id',$vendorCredentialId)
            ->first();
    }

    public static function getVendorCredentialByVendorId($vendorId)
    {
        return VendorCredentials::where('vendor_id',$vendorId)
            ->first();
    }

    public static function getUserVendorCredentialByVendorType($userId,$vendorType)
    {
        return VendorCredentials::join('hardware','hardware.vendor_id','=','vendor_credentials.vendor_id')
            ->where('hardware.hardware_type',$vendorType)
            ->join('client_hardware','client_hardware.id','=','hardware.id')
            ->where('client_hardware.user_id',$userId)
            ->select('vendor_credentials.*','hardware.sn as imsi')
            ->first();
    }


}
