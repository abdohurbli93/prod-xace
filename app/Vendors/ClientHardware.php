<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClientHardware extends Model
{

    use SoftDeletes;
    protected $table = 'client_hardware';
    protected $fillable = ['hardware_id', 'user_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public static function addClientHardware($clientHardware)
    {
        return ClientHardware::create($clientHardware);
    }


    public static function deleteHardwareData($clientHardwareId)
    {
        $deletedHardwareData = ClientHardware::where('id', $clientHardwareId)
            ->delete();
        if (!empty($deletedHardwareData))
            return true;
        return false;
    }

    public static function getClientHardwareById($clientHardwareId)
    {
        return ClientHardware::where('id', $clientHardwareId)
            ->first();
    }

    public static function getClientHardware($userId)
    {
        return ClientHardware::where('user_id', $userId)
            ->join('hardware', 'hardware.id', 'client_hardware.hardware_id')
            ->select([
                'client_hardware.id as hardware_id',
                'hardware.device_name',
                'hardware.sku',
                'hardware.sn',
                'hardware.hardware_type',
                'hardware.status',
                'hardware.id as hardware_id',
            ])->get();
    }

    public static function deleteClientHardwareBySkuRange($skuFrom,$skuTo,$userId)
    {
        return ClientHardware::whereIn('hardware_id',function ($query) use($userId,$skuFrom,$skuTo)
        {
            $query->select('id')
                ->from('hardware')
                ->where('user_id',$userId)
                ->whereBetween('sku',[$skuFrom,$skuTo]);
        })->delete();
    }

    public static function getUserSkuRange($userId)
    {
        $skuRange=ClientHardware::where('user_id',$userId)
            ->join('hardware','hardware.id','=','client_hardware.hardware_id')
            ->select('hardware.sku','hardware.hardware_type','hardware.vendor_id','hardware.id')
            ->orderBy('sku','asc')
            ->get()->toArray();
        if(empty($skuRange))
            return null;
        return Hardware::separateSkuToRange($skuRange);
    }
    public static function getUserSkuRangeByHardwareType($userId,$hardwareType)
    {
        $skuRange=ClientHardware::where('user_id',$userId)
            ->join('hardware','hardware.id','=','client_hardware.hardware_id')
            ->where('hardware.hardware_type',$hardwareType)
            ->select('hardware.sku','hardware.hardware_type','hardware.vendor_id','hardware.id')
            ->orderBy('sku','asc')
            ->get()->toArray();
        if(empty($skuRange))
            return null;
        return Hardware::separateSkuToRange($skuRange);
    }
    public static function checkSkuRangeAvailability($skuFrom,$skuTo)
    {
        return ClientHardware::join('hardware','hardware.id','=','client_hardware.hardware_id')
            ->whereBetween('sku',[$skuFrom,$skuTo])
            ->select('hardware.sku','hardware.hardware_type','hardware.vendor_id','hardware.id')
            ->orderBy('sku','asc')
            ->get()->toArray();
    }
    public static function reserveUserSkuRange($userId,$skuFrom,$skuTo)
    {
        $skuRange=self::checkSkuRangeAvailability($skuFrom,$skuTo);
        if(count($skuRange)>0||empty(Hardware::checkHardwareSku($skuFrom)||empty(Hardware::checkHardwareSku($skuTo))))
        {
            $data['data']=Hardware::separateSkuToRange($skuRange);
            $data['status']='fail';
            return $data;
        }

        $clientsHardware=array();
        $hardware=Hardware::getHardwareByRange($skuFrom,$skuTo);
        for($i=$skuFrom,$j=0;$i<=$skuTo;$i++,$j++)
        {
            $clientHardware = array('user_id' => $userId, 'hardware_id' => $hardware[$j]['id']);
            array_push($clientsHardware,$clientHardware);
        }
        DB::beginTransaction();
        try {
            $insertedClient=ClientHardware::insert($clientsHardware);
            $data['status']='success';
            DB::commit();
            $data['data']=$insertedClient;
        } catch (\Exception $e) {
            $data['status']='fail';
            DB::rollBack();
        }
        return $data;
    }
    public static function getUserHardwareByType($userId,$hardwareType)
    {
        return ClientHardware::where('user_id',$userId)
            ->join('hardware','hardware.id','=','client_hardware.hardware_id')
            ->where('hardware.hardware_type',$hardwareType)
            ->pluck('client_hardware.hardware_id')->toArray();
    }
    public static function getUserFirstSkuByOrder($userId,$hardwareType,$orderType)
    {
        return ClientHardware::where('user_id',$userId)
            ->join('hardware','hardware.id','=','client_hardware.hardware_id')
            ->where('hardware.hardware_type',$hardwareType)
            ->orderBy('hardware.sku',$orderType)
            ->pluck('client_hardware.hardware_id')
            ->first();
    }
    public static function getUserHardwareType($hardwareType)
    {
        return ClientHardware::where('user_id',Auth::id())
            ->join('hardware','hardware.id','=','client_hardware.hardware_id')
            ->where('hardware.hardware_type',$hardwareType)
            ->get();
    }
    public static function checkUserSku($sku,$userId)
    {
       return ClientHardware::join('hardware','hardware.id','=','client_hardware.hardware_id')
           ->where('client_hardware.user_id',$userId)
           ->where('hardware.sku',$sku)
           ->first();
    }
}
