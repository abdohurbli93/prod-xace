<?php

namespace App\Vendors;

use App\Http\Controllers\API\FlexiRoam;
use App\Http\Middleware\Client;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;

class Clients extends Model
{
    use SoftDeletes;
    protected $table = 'clients';
    protected $fillable = ['user_id', 'client_name','client_type','client_balance'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public static function addClient($client)
    {
        return Clients::create($client);
    }

    public static function updateClient($client, $clientId)
    {
        return Clients::where('id', $clientId)
            ->update($client);
    }

    public static function deleteClientByUserId($userId)
    {
        $deletedClient = Clients::where('user_id', $userId)
            ->delete();
        if (!empty($deletedClient))
        {
            $deletedUser=User::deleteUser($userId);
            if(!empty($deletedUser))
            return true;

            return false;
        }
        return false;
    }
    public static function getClientById($clientId)
    {
        return Clients::where('id', $clientId)
            ->first();
    }
    public static function getClientByUserId($userId)
    {
        return Clients::where('user_id', $userId)
            ->first();
    }
    public static function getAllClientSkuRange()
    {
       $clients=Clients::join('users','users.id','=','clients.user_id')
       ->where('user_type','client')
       ->get();
        foreach ($clients as $client)
        {
         $client['data']=ClientHardware::getUserSkuRange($client->user_id);
       }
        return $clients;
    }
    public static function refillUserSimByHardwareId($hardwareId,$amount)
    {
        $hardware=Hardware::getUserHardwareByHardwareId(Auth::id(),$hardwareId,'sim');
        $client=Clients::getClientByUserId(Auth::id());
        $clientBalance=ClientBalance::getLastBalance(Auth::id());
        if(empty($hardware)||$clientBalance->current_balance<$amount)
            return null;
        $flexiRoam = new FlexiRoam($hardware->vendor_id);
        $bytes=$amount*pow(1024.0,2);
        $flexiData = $flexiRoam->fillClientSim($hardware->imsi,$bytes);
        if(!empty($flexiData))
        {
            $newClientBalance['current_balance']=$clientBalance->current_balance-$amount;
            $newClientBalance['user_id']=Auth::id();
            ClientBalance::addNewBalance($newClientBalance);
            $flexiRoam->savingNewData($hardware);
          return 1;
        }
        return null;

    }
    public static function refillUserSimByHardwareSku($sku,$amount)
    {
        $hardware=Hardware::getUserHardwareByHardwareSku(Auth::id(),$sku,'sim');
        $client=Clients::getClientByUserId(Auth::id());
        $clientBalance=ClientBalance::getLastBalance(Auth::id());
        if(empty($hardware)||$clientBalance->current_balance<$amount)
            return null;
        $flexiRoam = new FlexiRoam($hardware->vendor_id);
        $bytes=$amount*pow(1024.0,2);
        $flexiData = $flexiRoam->fillClientSim($hardware->imsi,$bytes);
        if(!empty($flexiData))
        {
            $newClientBalance['current_balance']=$clientBalance->current_balance-$amount;
            $newClientBalance['user_id']=Auth::id();
            ClientBalance::addNewBalance($newClientBalance);
            $flexiRoam->savingNewData($hardware);
            return 1;
        }
        return null;

    }

}
