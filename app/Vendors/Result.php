<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Result extends Model
{
    use SoftDeletes;
    protected $table='results';
    protected $fillable=['hardware_id','value','date_time'];
    protected $hidden=['created_at','updated_at','deleted_at'];

    public static function addResult($result)
    {
        return Result::create($result);
    }

    public static function updateResult($result,$resultId)
    {
        return Result::where('id',$resultId)
            ->update($result);
    }

    public static function deleteHardware($resultId)
    {
        $result=Result::where('id',$resultId)
            ->delete();
        if(!empty($result))
            return true;
        return false;
    }

    public static function getResultById($resultId)
    {
        return  Result::where('id',$resultId)
            ->first();
    }
    public static function getResultValuesByHardwareId($hardwareId)
    {
        return Result::where('hardware_id',$hardwareId)
            ->select('value')->latest('date_time')->take(15)
            ->get();
    }
    public static function getResultDateTimeByHardwareId($hardwareId)
    {
        return Result::where('hardware_id',$hardwareId)
            ->select('date_time')->latest('date_time')->take(15)
            ->get();
    }
}
