<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientBalance extends Model
{
    use SoftDeletes;
    protected $table='client_balance';
    protected $fillable=['user_id','current_balance','date_time'];

    public static function addNewBalance($newBalance)
    {
        return ClientBalance::create($newBalance);
    }
    public static function getLastBalance($userId)
    {
        return ClientBalance::where('user_id',$userId)
            ->first();
    }
}
