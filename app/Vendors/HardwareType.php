<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HardwareType extends Model
{
    use SoftDeletes;
    protected $table='hardware_type';
    protected $fillable=['name'];
    protected $hidden=['created_at','updated_at','deleted_at'];

    public static function addHardwareType($hardwareType)
    {
        return HardwareType::create($hardwareType);
    }

    public static function updateHardwareType($hardwareType,$hardwareTypeId)
    {
        return HardwareType::where('id',$hardwareTypeId)
            ->update(['name'=>$hardwareType]);
    }

    public static function deleteHardwareType($hardwareTypeId)
    {
        $deletedHardwareType=HardwareType::where('id',$hardwareTypeId)
            ->delete();
        if(!empty($deletedHardwareType))
            return true;
        return false;
    }
    public static function getHardwareTypeById($hardwareTypeId)
    {
      return HardwareType::where('id',$hardwareTypeId)
          ->first();
    }


}
