<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendors extends Model
{
    use SoftDeletes;

    protected $table='vendors';
    protected $fillable=['vendor_name','email','phone_number','address'];
    protected $hidden=['created_at','updated_at','deleted_at'];

    public static function addVendor($vendor)
    {
        return Vendors::create($vendor);
    }

    public static function updateVendor($vendor,$vendorId)
    {
        return Vendors::where('id',$vendorId)
        ->update($vendor);
    }

    public static function deleteVendor($vendorId)
    {
       $deletedVendor=Vendors::where('id',$vendorId)
            ->delete();
        if(!empty($deletedVendor))
            return true;
        return false;
    }

    public static function getVendorByVendorId($vendorId)
    {
      return  Vendors::where('id',$vendorId)
            ->first();
    }

}
