<?php

namespace App\Vendors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientsCredentials extends Model
{
    use SoftDeletes;
    protected $table='client_credentials';
    protected $fillable=['client_id','username','password'];
    protected $hidden=['created_at','updated_at','deleted_at'];

    public static function addClientCredential($clientCredential)
    {
        return ClientsCredentials::create($clientCredential);
    }

    public static function updateClientCredential($clientCredential,$clientCredentialId)
    {
        return ClientsCredentials::where('id',$clientCredentialId)
            ->update($clientCredential);
    }

    public static function deleteClientCredential($clientCredentialId)
    {
        $deletedClientCredential=ClientsCredentials::where('id',$clientCredentialId)
            ->delete();
        if(!empty($deletedClientCredential))
            return true;
        return false;
    }
    public static function getClientCredentialById($clientCredentialId)
    {
        return ClientsCredentials::where('id',$clientCredentialId)
            ->first();
    }
}
