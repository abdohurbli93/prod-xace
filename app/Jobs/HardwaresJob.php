<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HardwaresJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $hardwares;

    /**
     * Create a new job instance.
     *
     * @param $hardwares
     */
    public function __construct($hardwares)
    {
        $this->hardwares=$hardwares;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->hardwares as $hardware)
        {
            dispatch(new HardwaresDataJob($hardware));
        }
    }
}
