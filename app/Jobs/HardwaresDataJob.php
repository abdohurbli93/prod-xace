<?php

namespace App\Jobs;

use App\Http\Controllers\API\FlexiRoam;
use App\Http\Controllers\API\Globiots;
use App\Vendors\HardwareData;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HardwaresDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $hardware;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($hardware)
    {
        $this->hardware=$hardware;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        if ($this->hardware->hardware_type == 'sim') {
            $flexiRoam = new FlexiRoam($this->hardware->vendor_id);
           $flexiRoam->savingNewData($this->hardware);
        } else {
            $globiots = new Globiots();
            $globiots->savingNewData($this->hardware);
        }
    }
}
